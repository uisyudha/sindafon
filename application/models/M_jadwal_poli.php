<?php

class M_Jadwal_Poli extends CI_Model{
  public function __construct()
  {
    parent::__construct();
  }

  public function alljadwal_count()
  {
    $query = $this
      ->db
      ->join('poli', 'poli.id=dokter.poli')
      ->get('dokter');

    return $query->num_rows();

  }

  public function alljadwal($limit, $start, $col, $dir, $c=null)
  {
    if($c != null){
      $this->db->where($c);
    }
    $query = $this
      ->db
      ->limit($limit, $start)
      ->order_by($col, $dir)
      ->join('poli', 'poli.id=dokter.poli')
      ->get('dokter');

    if ($query->num_rows() > 0) {
      return $query->result();
    } else {
      return null;
    }

  }

  public function jadwal_count($c){
    if($c != null){
      $this->db->where($c);
    }
    $query = $this
      ->db
      ->join('poli', 'poli.id=dokter.poli')
      ->get('dokter');

    return $query->num_rows();
  }

  public function jadwal_search($limit, $start, $search, $col, $dir, $c = null)
  {
    if($c != null){
      $this->db->where($c);
    }
    $query = $this
      ->db
      ->like('nama', $search)
      ->or_like('nama_dokter', $search)
      ->or_like('keterangan_hari', $search)
      ->or_like('keterangan_jam', $search)
      ->limit($limit, $start)
      ->order_by($col, $dir)
      ->join('poli', 'poli.id=dokter.poli')
      ->get('dokter');

    if ($query->num_rows() > 0) {
      return $query->result();
    } else {
      return null;
    }
  }

  public function jadwal_search_count($search, $c = null)
  {
    if($c != null){
      $this->db->where($c);
    }

    $query = $this
      ->db
      ->like('nama', $search)
      ->or_like('nama_dokter', $search)
      ->or_like('keterangan_hari', $search)
      ->or_like('keterangan_jam', $search)
      ->join('poli', 'poli.id=dokter.poli')
      ->get('dokter');

    return $query->num_rows();
  }
}