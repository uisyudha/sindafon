<?php

class M_Jadwal_Operasi extends CI_Model
{
  public function __construct()
  {
    parent::__construct();
  }

  public function insert($data)
  {

    $query = $this->db->insert('jadwal_operasi', $data);

    return $this->db->insert_id();
  }

  public function update($condition, $data)
  {
    $this->db->where($condition);

    $query = $this->db->update('jadwal_operasi', $data);

    return ($this->db->affected_rows() > 0);
  }

  public function delete($data)
  {
    $this->db->where($data);

    $query = $this->db->delete('jadwal_operasi');

    return ($this->db->affected_rows() > 0);
  }

  public function get($condition = null)
  {

    if ($condition != null) {
      $this->db->where($condition);
    }

    $query = $this->db->get('jadwal_operasi');

    return $query->result();
  }

}
