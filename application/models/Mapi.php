<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Mapi extends CI_Model {
	public function __construct()
        {
                // Call the CI_Model constructor
                parent::__construct();
        }

         public function get_kunjungan($data=null)
        {       

                if($data!=null){
                    $this->db->where($data);
                }     
                $query = $this->db->get('t_kunjungan');
                return $query->result();
        }
        public function get_pasien($data=null)
        {       

                if($data!=null){
                    $this->db->where($data);
                }     
                $query = $this->db->get('t_pasien');
                return $query->result();
        }

        public function update_pasien($condition, $data)
        {
                $this->db->where($condition);

                $query = $this->db->update('t_pasien', $data);

                return ($this->db->affected_rows() > 0);
        }

        public function insert_pasien($data)
        {

                $query = $this->db->insert('t_pasien', $data);

                return $this->db->insert_id();
        }

        public function delete_pasien($data)
        {
                $this->db->where($data);

                //  $this->db->join('t_kunjungan_poli','t_kunjungan_poli.id_kunjungan=t_tindakan.id_kunjungan');
                $query = $this->db->delete('t_pasien');

                return ($this->db->affected_rows() > 0);
        }

        public function get_total_pasien($data=null)
        {       

                if($data!=null){
                    $this->db->where($data);
                }     
                $query = $this->db->get('t_pasien');
                return $query->num_rows();
        }

}
