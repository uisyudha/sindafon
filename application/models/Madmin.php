<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Madmin extends CI_Model {
	public function __construct()
        {
                // Call the CI_Model constructor
                parent::__construct();
        }

         public function getadmin($data=null)
        {       

                if($data!=null){
                    $this->db->where($data);
                }     
                $query = $this->db->get('admin');
                return $query->result();
        } 
          public function getlayanan($data=null)
        {       

                if($data!=null){
                    $this->db->where($data);
                }     
                $query = $this->db->get('t_layanan');
                return $query->result();
        } 
        public function get_jadwal($data=null)
        {       

                if($data!=null){
                    $this->db->where($data);
                }     
                $this->db->join('t_layanan','t_layanan.id=jadwal_poli.layanan_id');
                $this->db->order_by('day,id_jadwal','asc');
                $query = $this->db->get('jadwal_poli');
                return $query->result();
        }
         public function insert_jadwal($data){

                 
                 $query = $this->db->insert('jadwal_poli',$data);

                return $this->db->insert_id();
            } 
         public function update_jadwal($con,$value){
                
                $this->db->where($con);
                $query = $this->db->update('jadwal_poli',$value);

                return ($this->db->affected_rows() > 0);
            }   
        public function delete_jadwal($con){
                
                $this->db->where($con);
                $query = $this->db->delete('jadwal_poli');

                return ($this->db->affected_rows() > 0);
        }   

         public function get_aturan($data=null)
        {       

                if($data!=null){
                    $this->db->where($data);
                }     
                 
                $query = $this->db->get('aturan');
                return $query->result();
        }
         public function insert_aturan($data){

                 
                 $query = $this->db->insert('aturan',$data);

                return $this->db->insert_id();
            } 
         public function update_aturan($con,$value){
                
                $this->db->where($con);
                $query = $this->db->update('aturan',$value);

                return ($this->db->affected_rows() > 0);
            }   
        public function delete_aturan($con){
                
                $this->db->where($con);
                $query = $this->db->delete('aturan');

                return ($this->db->affected_rows() > 0);
        }    
       
        public function get_apikey($data=null){
                
                  if($data!=null){
                    $this->db->where($data);
                      }     
                $query = $this->db->get('keys');

                 return $query->result();
        }   

         public function deletekey($con){
                
                $this->db->where($con);
                $query = $this->db->delete('keys');

                return ($this->db->affected_rows() > 0);
        }   

}
