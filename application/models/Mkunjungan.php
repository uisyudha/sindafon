<?php
defined('BASEPATH') or exit('No direct script access allowed');
/* 
Model untuk datatable list kunjungan pasien pada layanan pendaftarn dengan
custom filter
 */
class Mkunjungan extends CI_Model
{
  public $table = 't_kunjungan';
  public $column_order = array(null, 'kode', 't_kunjungan.no_rm', 'nama', 'tgl_kunjungan', 'nama_layanan', 'cara_bayar', 'nomor_anggota', 'no_rujukan', 'no_surat'); //set column field database for datatable orderable
  public $column_search = array('t_kunjungan.no_rm', 't_kunjungan.kode'); //set column field database for datatable searchable
  public $order = array('t_kunjungan.id' => 'desc'); // default order

  public function __construct()
  {
    parent::__construct();
    $this->load->database();
  }

  private function _get_datatables_query($condition = null)
  {
    if($condition != null){
      $this->db->where($condition);
    }

    //add custom filter here
    if ($this->input->post('no_rm')) {
      $this->db->where('no_rm', $this->input->post('no_rm'));
    }
    // if ($this->input->post('nama')) {
    //   $this->db->like('nama', $this->input->post('nama'));
    // }

    $this->db->select('t_kunjungan.*, t_pasien.nama, t_layanan.nama_layanan');
    $this->db->from($this->table);
    $this->db->join('t_pasien', 't_kunjungan.no_rm=t_pasien.no_rm');
    $this->db->join('t_layanan', 't_layanan.id=t_kunjungan.layanan');
    $i = 0;

    foreach ($this->column_search as $item) // loop column
    {
      if ($_POST['search']['value']) // if datatable send POST for search
      {

        if ($i === 0) // first loop
        {
          $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
          $this->db->like($item, $_POST['search']['value']);
        } else {
          $this->db->or_like($item, $_POST['search']['value']);
        }

        if (count($this->column_search) - 1 == $i) //last loop
        {
          $this->db->group_end();
        }
        //close bracket
      }
      $i++;
    }

    if (isset($_POST['order'])) // here order processing
    {
      $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    } else if (isset($this->order)) {
      $order = $this->order;
      $this->db->order_by(key($order), $order[key($order)]);
    }
  }

  public function get_datatables($condition = null)
  {
    $this->_get_datatables_query($condition);
    if ($_POST['length'] != -1) {
      $this->db->limit($_POST['length'], $_POST['start']);
    }
    // $this->db->join('t_pasien', 't_pasien.no_rm = t_kunjungan.no_rm');

    $query = $this->db->get();
    // print_r($this->db->last_query());
    return $query->result();
  }

  public function count_filtered($condition = null)
  {
    $this->_get_datatables_query($condition);
    $query = $this->db->get();
    return $query->num_rows();
  }

  public function count_all()
  {
    $this->db->from($this->table);
    return $this->db->count_all_results();
  }
}