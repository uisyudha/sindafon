<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Mpendaftaran extends CI_Model {



	public function __construct()
        {
                // Call the CI_Model constructor
                parent::__construct();
        }

         
         public function get_provinsi($data=null)
        {       

                if($data!=null){
                    $this->db->where($data);
                }     
                $query = $this->db->get('provinces');
                return $query->result();
        }
        public function get_city($data=null)
        {       

                if($data!=null){
                    $this->db->where($data);
                }     
                $query = $this->db->get('regencies');
                return $query->result();
        }
        public function get_district($data=null)
        {       

                if($data!=null){
                    $this->db->where($data);
                }     
                $query = $this->db->get('districts');
                return $query->result();
        }
        public function get_village($data=null)
        {       

                if($data!=null){
                    $this->db->where($data);
                }     
                $query = $this->db->get('villages');
                return $query->result();
        }
        public function get_layanan($data=NULL)
        {   
                if($data!=NULL)
                    $this->db->where($data);
                $query = $this->db->get('t_layanan');
                return $query->result();
        }

        public function insert_pasien($data){
                $query = $this->db->insert('t_pasien',$data);

                return $this->db->insert_id();
        }
        public function insert_pendaftaran($data){
                $query = $this->db->insert('t_kunjungan',$data);

                return $this->db->insert_id();
        }

        public function update_pendaftaran($condition, $data)
        {
                $this->db->where($condition);

                $query = $this->db->update('t_kunjungan', $data);

                return ($this->db->affected_rows() > 0);
        }
        
         public function get_jadwal($data=null)
        {       

                if($data!=null){
                    $this->db->where($data);
                }     
                $this->db->join('t_layanan','t_layanan.id=jadwal_poli.layanan_id');
                $this->db->order_by('day,id_jadwal','asc');
                $query = $this->db->get('jadwal_poli');
                return $query->result();
        }
         public function get_pendaftaran($data)
        {       

                if($data!=null){
                    $this->db->where($data);
                }     
                
                
                $query = $this->db->get('t_kunjungan');
                return $query->result();
        }

        public function get_pendaftaran_full($data)
        {       

                if($data!=null){
                    $this->db->where($data);
                }     
                
                $this->db->join('t_layanan', 't_layanan.id = t_kunjungan.layanan');
                $query = $this->db->get('t_kunjungan');
                return $query->result();
        }
          public function get_aturan($data=null)
        {       

                if($data!=null){
                    $this->db->where($data);
                }     
               
                $this->db->order_by('id_aturan','asc');
                $query = $this->db->get('aturan');
                return $query->result();
        }

        public function get_pasien($condition=null)
        {       

                if($condition!=null){
                    $this->db->where($condition);
                }     
                $this->db->limit(1);
                $query = $this->db->get('t_pasien');
                return $query->result();
        }

        public function hapus_kunjungan($data){

                
                $this->db->where($data);
                
              //  $this->db->join('t_kunjungan_poli','t_kunjungan_poli.id_kunjungan=t_tindakan.id_kunjungan');
                $query = $this->db->delete('t_kunjungan');

               return ($this->db->affected_rows() > 0);
        }

        public function update_kunjungan($condition,$data){

                $this->db->where($condition);
                
                 $query = $this->db->update('t_kunjungan',$data);

               return ($this->db->affected_rows() > 0);
            }
       
}
