 
<div class="form-pendaftaran">
	<section class="form-box" >
            <div class="container form-reg">
                
                <div class="row">
                    <div class="col-sm-10  offset-sm-1 col-md-10 offset-md-1 col-lg-10 offset-lg-1 form-wizard">
					
						<!-- Form Wizard -->
                    	<form role="form" action="" method="post">

                    		<h3>Pendaftaran Pasien</h3>
                    		<p>Isi Semua Data untuk melanjutkan kelangkah selanjutnya</p>
							
							<!-- Form progress -->
                    		<div class="form-wizard-steps form-wizard-tolal-steps-4">
                    			<div class="form-wizard-progress">
                    			    <div class="form-wizard-progress-line" data-now-value="12.25" data-number-of-steps="4" style="width: 12.25%;"></div>
                    			</div>
								<!-- Step 1 -->
                    			<div class="form-wizard-step active">
                    				<div class="form-wizard-step-icon"><span class="fa fa-user icon-title" aria-hidden="true"></span></div>
                    				<p>Data pasien</p>
                    			</div>
								<!-- Step 1 -->
								
								<!-- Step 2 -->
                    			<div class="form-wizard-step">
                    				<div class="form-wizard-step-icon"><span class="fa fa-location-arrow icon-title" aria-hidden="true"></span></div>
                    				<p>Data Kunjungan</p>
                    			</div>
								<!-- Step 2 -->
								
								<!-- Step 3 -->
								<div class="form-wizard-step">
                    				<div class="form-wizard-step-icon"><span class="fa fa-briefcase icon-title" aria-hidden="true"></span></div>
                    				<p>Jenis pasien</p>
                    			</div>
								<!-- Step 3 -->
								
								<!-- Step 4 -->
								<div class="form-wizard-step">
                    				<div class="form-wizard-step-icon"><span class="fa fa-list-alt icon-title" aria-hidden="true"></span></div>
                    				<p>Konfirmasi Data</p>
                    			</div>
								<!-- Step 4 -->
                    		</div>
							<!-- Form progress -->
                    		
							
							<!-- Form Step 1 -->
                    		<fieldset>

                    		    <h4>informasi Pasien: <span>Step 1 - 4</span></h4>
								
								<div class="form-group">
                    			    <label class="control-label">Title  <span>*</span></label>
                                    <select class="form-control" id="title" name="title">
							          <option value="Tn">Tn</option>
							          <option value="Ny">Ny</option>
							          <option value="Nn">Nn</option>
							          <option value="An">An</option>
							          <option value="Sdr">Sdr</option>
							          <option value="By">By</option>
							    
							      </select>
                                </div>
                                <div class="form-group">
                    			    <label class="control-label">Nama Pasien: <span>*</span></label>
                                    <input type="text" name="nama" placeholder="Nama Pasien" class="form-control required">
                                </div>
								
								  <div class="form-group">
                    			    <label class="control-label">Nama Ayah: <span>*</span></label>
                                    <input type="text" name="nama_ayah" placeholder="Nama Ayah" class="form-control required">
                                </div>

                                <div class="form-group">

                    			    <label class="control-label">jenis Kelamin :  </label>
                    			      <div class="form-check">
                    			      	  <label for="radio-1">Laki-Laki</label>
	                                      <input id="radio-1" class="radio form-control " type="radio" name="kelamin" value="L" >  
										   <label for="radio-2">Perempuan</label>
	                                      <input id="radio-2" class="radio form-control " type="radio" name="kelamin" value="P" >  
									  </div>
									 
								 
                                </div>


								<div class="form-group">
                    			    <label class="control-label">Status Perkawinan  <span>*</span></label>
                                    <select class="form-control" id="status_kawin" name="status_kawin">
									    <option value="1">Belum Kawin</option>
									    <option value="2">Kawin</option>
									    <option value="3">Janda / Duda</option>
								     </select>
                                </div>

                                <div class="form-group">
								  <label class=" control-label" for="pendidikan">Pendidikan Terakhir  </label>
								    <select class="form-control" id="pendidikan" name="pendidikan">
									    <option value="1">SD</option>
									    <option value="2">SMP</option>
									    <option value="3">SMA</option>
									    <option value="4">D3</option>
									    <option value="5">S1</option>
									    <option value="6">S2</option>
									    <option value="7">S3</option>
									    <option value="8">Tidak Tahu</option>
								    </select>
								</div> 
								 <div class="form-group">
								  <label class=" control-label" for="pendidikan">Agama </label>
								   <select class="form-control" id="agama" name="agama">
								    <option value="islam">Islam</option>
								    <option value="kristen">Kristen</option>
								    <option value="katholik">Katholik</option>
								    <option value="hindu">Hindu</option>
								    <option value="budha">Budha</option>
								    <option value="lain">Lain- lain</option>
								    </select>
								</div>
                          
								<!-- Text input-->
									<div class="form-group">
									  <label class="  control-label" for="textinput">Tempat Lahir</label>  
									 
									  <input id="tempat_lahir" name="tempat_lahir" placeholder="Kota Kelahiran" class="form-control input-md required" type="text" required>
									    
									  
									</div>

									<!-- Text input-->
									<div class="form-group">
									  <label class="  control-label" for="textinput">Tanggal Lahir</label>  
									 
									  <input id="tgl_lahir" name="tgl_lahir" placeholder="20-12-2000" class="form-control required" type="text" value="" required>
									   
									</div>

									<!-- Text input-->
									<div class="form-group">
									  <label class="control-label" for="textinput">Alamat </label>  
									  
									  <input id="alamat_sekarang" name="alamat" placeholder="Alamat" class="form-control input-md required" type="text" required>  
									</div>
 
								<div class="form-group ui-widget">
								  <label class="control-label" for="provinsi">Provinsi : </label>
								  
								     <select class="form-control cbb" id="provinsi" name="provinsi"   >
								     	<option selected="true" disabled="true"></option>
									    <?php 
									    if($provinsi!==''){
									    	foreach($provinsi as $d){
									    	 
									         ?>
									          <option value="<?php echo $d->id; ?>"><?php echo $d->name; ?></option>
									        
									      <?php 
									        
									    	}
									    }else{
									    	?>
											 	<option value="0"></option>
									    	<?php
									    }
									    ?>
									</select> 
								</div>

								<div class="form-group">
								  <label class=" control-label" for="kota">Kota</label>
								     <select class="form-control required" id="kota" name="kota">
								   		 
								     </select>								  
								</div>

								<div class="form-group">
								  <label class="col-md-4 control-label" for="kec">Kecamatan</label>
								  	<select class="form-control required" id="kec" name="kec" >
								   		 
								     </select>								 
								</div>
								<div class="form-group">
								  <label class="control-label" for="kel">Kelurahan</label>
								    <select class="form-control required" id="kel" name="kel" >
									     
									  </select>
								</div>

								<!-- Text input-->
								<div class="form-group">
								  <label class="control-label" for="no_ktp">No KTP</label>  
								 <input id="no_ktp" name="no_ktp" placeholder="370111111111111111111" class="form-control required" type="text" required>
								</div>

								<!-- Text input-->
								<div class="form-group">
								  <label class="control-label" for="no_telp">No Telp</label>  
								  <input id="no_telp" name="no_telp" placeholder="082100000000000" class="form-control required" type="text" required>
								 </div>
								 <!-- Text input-->
								<div class="form-group">
								  <label class="control-label" for="pekerjaan">Pekerjaan</label>  
								  <input id="pekerjaan" name="pekerjaan" placeholder="pekerjaan" class="form-control required" type="text" required>
								 </div>
								 <!-- Text input-->
									<div class="form-group">
									  <label class="control-label" for="penanggung_jawab">Nama Penanggung Jawab</label>  
									  <input id="penanggung_jawab" name="penanggung_jawab" placeholder="Penanggung Jawab" class="form-control required" type="text" >
									 </div>
								<!-- Textarea -->
								<div class="form-group">
								  <label class="control-label" for="telp_penanggung">No Telp / Hp Penanggung Jawab</label>
								  <input id="telp_penanggung" name="telp_penanggung" placeholder="telp penanggung jawab" class="form-control required" type="text" >    
								 </div>
                                <div class="form-wizard-buttons">
                                    <button type="button" class="btn btn-next">Next</button>
                                </div>
                            </fieldset>
							<!-- Form Step 1 -->

							<!-- Form Step 2 -->
                            <fieldset>

                                <h4>Informasi Pendaftaran : <span>Step 2 - 4</span></h4>
								<div class="form-group">
								  <label class="control-label" for="poli">Poli Tujuan</label>  
								 
								  <select class="form-control" id="poli" name="poli" required>
								  <option disabled selected required></option>
								     <?php
								    if($layanan!==''){
								      foreach($layanan as $d){
								        ?>
								       <option value="<?php echo $d->id; ?>"><?php echo $d->nama_layanan; ?></option>
								      <?php
								      }
								    }else{
								      ?>
								    <option value="0"> - </option>
								      <?php
								    }
								    ?>
								    
								  </select>
								  
								</div>

								<div class="form-group">
								  <label class="control-label" for="textinput">Tanggal Kunjungan</label>  
								  <input id="tgl_daftar" name="tgl_daftar" placeholder="20-01-2019" class="form-control required" type="text"  >
								 <small class="form-text text-info"><b>*kosongi jika tidak ada</b> </small>
								</div>
								<br/>
								 <div class="form-wizard-buttons">
                                    <button type="button" class="btn btn-previous">Previous</button>
                                    <button type="button" class="btn btn-next">Next</button>
                                </div>
                            </fieldset>
							<!-- Form Step 2 -->

							<!-- Form Step 3 -->
                            <fieldset>

                                <h4>Jenis Pasien: <span>Step 3 - 4</span></h4>
								<div class="form-group">
								  <label class="col-md-3 control-label" for="textinput">Kelompok Pasien</label>  
								
								  <select class="form-control" id="jenispasien" name="jenispasien" required>
								  	<option value="99">Umum</option>
								  	<option value="1">BPJS</option>
								  	<option value="3">BPJS TK</option>
								  	
								  </select>
								 
								</div>
								<div class="form-group">
								  <label class="control-label" for="textinput">Nomor Anggota</label>  
								  <input  name="no_anggota" placeholder="0" class="form-control" type="text" >
								 	<small class="form-text text-info"><strong>*kosongi jika tidak ada</strong> </small>
								</div>
								 
								<br/>
                                <div class="form-wizard-buttons">
                                    <button type="button" class="btn btn-previous">Previous</button>
                                    <button type="button" class="btn btn-next btn-confirmation">Next</button>
                                </div>
                            </fieldset>
							<!-- Form Step 3 -->
							
							<!-- Form Step 4 -->
							<fieldset>

                                <h4>Pastikan Data Anda Sesuai Sebelum Di Proses : <span>Step 4 - 4</span></h4>
								<div style="clear:both;"></div>
								<div class="form-group">
								  <label class="control-label" for="textinput">Masukkan Email</label>  
								  <input  name="email" placeholder="example@mail.com" class="form-control required" type="email" required>
								 	<small class="form-text text-muted">*digunakan untuk mengirim konfirmasi pendaftaran </small>
								</div>
								<div class="data-konfirmasi">
									 <p> <u> Informasi Pasien </u></p>
									 <p>Nama : <strong> <span id="ktitle"></span> <span id="knama"></span> </strong> </p>
									 <p>Nama Ayah : <strong> <span id="kayah"></span></strong> </p>
									 <p>Jenis Kelamin : <strong> <span id="kkelamin"></span></strong> </p>
									 <p>status : <strong> <span id="kstatus"></span></strong> </p>
									 <p>Pendidikan : <strong> <span id="kpendidikan"></span> </strong></p>
									 <p>Agama : <strong> <span id="kagama"></span> </strong></p>
									 <p>Tempatlahir : <strong> <span id="ktempat_lahir"></span> </strong></p>
									 <p>Tanggal Lahir : <strong> <span id="ktgl_lahir"></span> </strong></p>
									 <p>Alamat : <strong> <span id="kalamat"></span> </strong></p>
									 <p>Provinsi : <strong> <span id="kprovinsi"></span></strong> </p>
									 <p>Kota :<strong>  <span id="kkota"></span> </strong></p>
									 <p>kecamatan :<strong> <span id="kkec"></span></strong> </p>
									 <p>Kelurahan :<strong> <span id="kkel"></span></strong> </p>
									 <p>No Ktp :<strong> <span id="kktp"></span></strong> </p>
									 <p>No Telp : <strong><span id="ktelp"></span> </strong></p>
									 <p>Pekerjaan : <strong><span id="kpekerjaan"></span></strong> </p>
									 <p>Penanggungjawab : <strong><span id="kpenanggungjawab"></span> </strong></p>
									 <p>Telp Penanggungjawab : <strong><span id="ktelppenanggung"></span></strong> </p>
									 <br>
									 <p> <u> Data Kunjungan </u></p>
									 <p>Poli Tujuan : <strong><span id="kpoli"></span></strong> </p>
									 <p>Tanggal Kunjungan :<strong> <span id="ktgl_kunjungan"></span> </strong></p>
									 <br>
									 <p> <u> Kelompok Pasien </u></p>
									 <p>Jenis pasien :<strong> <span id="kjenis_pasien"></span> </strong></p>
									 <p>No Anggota : <strong><span id="kno_anggota"></span></strong> </p>
								</div>
								 <br>
								 <p class="text-muted">* Pastikan Data sesuai</p>

                    			 
								 
                                <div class="form-wizard-buttons">
                                    <button type="button" class="btn btn-previous">Previous</button>
                                    <button type="button" id="btn-nonreg-submit" class="btn btn-submit">Submit</button>
                                </div>
                            </fieldset>
							<!-- Form Step 4 -->
                    	
                    	</form>
						<!-- Form Wizard -->
                    </div>
                </div>
                    
            </div>
        </section>
    </div>