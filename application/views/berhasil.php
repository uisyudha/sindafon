<section class="content mt-2">
 	<div class="container-fluid">
 		<!-- Card -->
		<div class="card card-warning card-outline">
			<div class="card-body">
				<div class="row">
					<div class="col-12">
						<h3>Pendaftaran Berhasil </h3>
						<p>Nomor Pendaftaran anda<br>
						<p class="text-center"><span style="font-size:30px"><b><?php echo $kode;?></b></span></p>
						<p><b>Simpan No Pendaftaran</b> anda dan tunjukkan kepada petugas pendaftaran saat berkunjung</p>
						<p><b>No Pendaftaran jangan sampai lupa!</b></p>
						<p><b>Pasien wajib melakukan konfirmasi kedatangan ke loket pendaftaran pada:</b></p>
						<ul>
							<li>Pukul 07.00-11.00 untuk hari senin s/d kamis</li>
							<li>Pukul 07.00-10.00 untuk hari jumat & sabtu</li>
						</ul>
						Ceklist dan Persyaratan yang harus dilengkapi ketika berkunjung :</p>
						<ul>
							<?php foreach($aturan as $a){
								echo $a->keterangan;
							}
							?>
						</ul>						
					</div>
				</div>
			</div>
			<!-- /.card-body -->
		</div>
		<!-- ./card -->
 	</div>
</section>