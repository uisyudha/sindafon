
<div class="container">
	
    <div class="row">
    	<div class="col-md-6 offset-md-3">
    		<div class="card form-login border border-dark rounded">
		<div class="card-header">
			<h3 class="card-title text-center"> Form Login </h3>
		</div>
		<div class="card-body border-top border-dark">
       <form role="form" class="form" action="<?php echo base_url('adminrs/login');?>" method="post">
            <center><p class="text-warning"><?php echo $this->session->flashdata('pesan');?></p></center>
       		<div class="form-group">
			    <label for="username">Username</label>
			    <input type="text" name="username" class="form-control" id="username" aria-describedby="usernameHelp" placeholder="Enter username">
			    <small id="usernameHelp" class="form-text text-muted"></small>
			  </div>
			  <div class="form-group">
			    <label for="password">Password</label>
			    <input type="password" name="password" class="form-control" id="password" placeholder="Password">
			  </div>
            
          <button type="submit" class="btn btn-success" name="submit" value="submit">Submit</button>
        </form>
    </div>
    </div>
</div>
</div>
</div>