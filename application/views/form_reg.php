 
<section class="content mt-2">
 	<div class="container-fluid">
 		<!-- Card -->
		<div class="card card-warning card-outline">
			<div class="card-body">
				<div class="row">
					<div class="col-12">
						<div class="form-wizard p-1">
							<!-- Form Wizard -->
							<form role="form" action="" method="post">
								<h3>Pendaftaran Pasien</h3>
								<p>Isi Semua Data untuk melanjutkan kelangkah selanjutnya</p>
								<!-- Form progress -->
								<div class="form-wizard-steps form-wizard-tolal-steps-4">
									<div class="form-wizard-progress">
										<div class="form-wizard-progress-line" data-now-value="12.25" data-number-of-steps="4" style="width: 12.25%;"></div>
									</div>
									<!-- Step 1 -->
									<div class="form-wizard-step active p-1">
										<div class="form-wizard-step-icon"><span class="fa fa-user icon-title" aria-hidden="true"></span></div>
									</div>
									<!-- Step 1 -->
									
									<!-- Step 2 -->
									<div class="form-wizard-step p-1">
										<div class="form-wizard-step-icon"><span class="fa fa-location-arrow icon-title" aria-hidden="true"></span></div>
									</div>
									<!-- Step 2 -->
										
									<!-- Step 3 -->
									<div class="form-wizard-step p-1">
										<div class="form-wizard-step-icon"><span class="fa fa-briefcase icon-title" aria-hidden="true"></span></div>
									</div>
									<!-- Step 3 -->
									
									<!-- Step 4 -->
									<div class="form-wizard-step p-1">
										<div class="form-wizard-step-icon"><span class="fa fa-list-alt icon-title" aria-hidden="true"></span></div>
									</div>
									<!-- Step 4 -->
								</div>
								<!-- Form progress -->

								<!-- Form Step 1 -->
								<fieldset>
									<h4>Data Pasien: <span>Step 1 - 4</span></h4>
									<!-- Text input-->
									<div class="form-group">
										<label class="control-label" for="nomr">No RM</label>
										<input id="nomr" name="nomr" autocomplete="off" placeholder="012345" class="form-control required" type="text" >   
										<small class="form-text text-muted">*nomor yang ada di kartu pasien</small> 
									</div>
									<div class="form-group">
										<label class="  control-label" for="textinput">Tanggal Lahir</label>  
										
										<input id="tgl_lahir" autocomplete="off" name="tgl_lahir" placeholder="20-12-2000" class="form-control required" type="text" value="" required>
									</div>
									<div class="form-wizard-buttons">
										<button type="button" class="btn btn-warning btn-nomr">Next</button>
									</div>
								</fieldset>
								<!-- Form Step 1 -->

								<!-- Form Step 2 -->
								<fieldset>
									<h4>Informasi Pendaftaran : <span>Step 2 - 4</span></h4>
									<div class="form-group">
										<label class="control-label" for="stextinput">No RM</label>  
										<input id="no_rm_confirm" class="form-control required" type="text" disabled>
									</div>
									<div class="form-group">
										<label class="control-label" for="stextinput">Nama</label>  
										<input id="nama_confirm" class="form-control required" type="text" disabled>
									</div>
									<div class="form-group">
										<label class="control-label" for="stextinput">Tangal Lahir</label>  
										<input id="tgl_lahir_confirm" class="form-control required" type="text" disabled>
									</div>
									<div class="form-group">
										<label class="control-label" for="stextinput">Jenis Kelamin</label>  
										<input id="jenis_kelamin_confirm" class="form-control required" type="text" disabled>
									</div>
									<div class="form-group">
										<label class="control-label" for="stextinput">Alamat</label>  
										<input id="alamat_confirm" class="form-control required" type="text" disabled>
									</div>
									<div class="form-group">
										<label class="control-label" for="poli">Poli Tujuan</label>  
										<select class="form-control" id="poli" name="poli" required>
											<option disabled selected required></option>
											<?php
											if($layanan!==''){
											foreach($layanan as $d){
											?>
											<option value="<?php echo $d->id; ?>"><?php echo $d->nama_layanan; ?></option>
											<?php
											}
											}else{
											?>
											<option value="0"> - </option>
											<?php
											}
											?>
										</select>
									</div>
									<div class="form-group">
										<label class="control-label" for="textinput">Tanggal Pemeriksaan (H-1)</label>  
										<input disabled class="form-control required" type="text" value="<?php echo date("d-m-Y", strtotime("+1 day"));?>" >
										<input type="hidden" id="tgl_daftar" name="tgl_daftar" value="<?php echo date("d-m-Y", strtotime("+1 day"));?>" >
									</div>
									<br/>
									<div class="form-wizard-buttons">
										<button type="button" class="btn btn-previous">Previous</button>
										<button type="button" class="btn btn-next-poli btn-warning">Next</button>
									</div>
								</fieldset>
								<!-- Form Step 2 -->

								<!-- Form Step 3 -->
								<fieldset>
									<h4>Jenis Pasien: <span>Step 3 - 4</span></h4>
									<div class="form-group">
										<label class="col-md-3 control-label" for="textinput">Cara Bayar</label>  
										<select class="form-control" id="jenispasien" name="jenispasien" required>
											<option value="99">Umum</option>
											<option value="1">BPJS</option>
											<option value="3">BPJS TK</option>
											<option value="5">Jasa Raharja</option>
											<option value="6">Jampersal</option>
										</select>
									</div>
									<div id="form-no-anggota" style="display:none">
										<div class="form-group">
											<label class="control-label" id="lbnomoranggota" for="textinput">Nomor BPJS</label>  
											<input  name="no_anggota" placeholder="0" class="form-control" type="text" >
											<small class="form-text text-muted">*kosongi jika tidak ada </small>
										</div>
									</div>
									<div id="form-jenis-bpjs" style="display:none">
										<div class="form-group">
											<label class="control-label" for="textinput">Nomor Rujukan</label>  
											<input  name="no_rujukan" placeholder="0" class="form-control" type="text" >
											<small class="form-text text-muted">*kosongi jika tidak ada </small>
										</div>
										<div class="form-group">
											<label class="control-label" for="textinput">Nomor Surat Kontrol</label>  
											<input  name="no_surat" placeholder="0" class="form-control" type="text" >
											<small class="form-text text-muted">*kosongi jika tidak ada </small>
										</div>
									</div>
									<br/>
									<div class="form-wizard-buttons">
										<button type="button" class="btn btn-previous">Previous</button>
										<button type="button" class="btn btn-next btn-warning btn-confirmation-reg">Next</button>
									</div>
								</fieldset>
								<!-- Form Step 3 -->

								<!-- Form Step 4 -->
								<fieldset>
									<h4>Pastikan Data Anda Sesuai Sebelum Di Proses : <span>Step 4 - 4</span></h4>
									<div style="clear:both;"></div>
									<div class="data-konfirmasi">
										<p> <u> Informasi Pasien </u></p>
										<p>Nama : <strong> <span id="knama"></span> </strong> </p>
										<p>Tanggal Lahir : <strong> <span id="ktgl_lahir"></span> </strong></p>
										<p>No RM : <strong> <span id="knomr"></span> </strong></p>
										<br>
										<p> <u> Data Kunjungan </u></p>
										<p>Poli Tujuan : <strong><span id="kpoli"></span></strong> </p>
										<p>Tanggal Kunjungan :<strong> <span id="ktgl_kunjungan"></span> </strong></p>
										<br>
										<p> <u> Kelompok Pasien </u></p>
										<p>Jenis pasien :<strong> <span id="kjenis_pasien"></span> </strong></p>
										<p>No Anggota : <strong><span id="kno_anggota"></span></strong> </p>
										<p>No Rujukan : <strong><span id="kno_rujukan"></span></strong> </p>
									</div>
									<br>
									<p class="text-muted">* Pastikan Data sesuai</p>
									<div class="form-wizard-buttons">
										<button type="button" class="btn btn-previous">Previous</button>
										<button type="button" id="btn-reg-submit" class="btn btn-submit">Submit</button>
									</div>
								</fieldset>
								<!-- Form Step 4 -->
							</form>
							<!-- Form Wizard -->
						</div>
					</div>
				</div>
			</div>
			<!-- /.card-body -->
		</div>
		<!-- ./card -->
 	</div>
</section>
<script>
$(document).ready(function(){
	$( ".btn-nomr" ).on('click',function() {
		
		var mr=$('input[name=nomr]').val();

		var parent_fieldset = $(this).parents('fieldset');
		var current_active_step = $(this).parents('.form-wizard').find('.form-wizard-step.active');
		var progress_line = $(this).parents('.form-wizard').find('.form-wizard-progress-line');

		if(mr.length!=6){
			toastr.error('No RM harus 6 digit');
			// throw new Error("No RM harus 6 digit");
		}
		else if(isNaN(mr)){
			toastr.error('No RM harus angka');
			// throw new Error("No RM harus angka");
		}
		else{
			NProgress.start();
			var nomr = $('#nomr').val();
			var tgl_lahir = $('#tgl_lahir').val().split("-");

			$.ajax({
				type: "POST",
				url: "<?php echo base_url(); ?>" + "Pendaftaran/get_data_pasien",
				dataType: "JSON",
				data: {
					nomr: nomr,
					tgl_lahir: tgl_lahir[2]+'-'+tgl_lahir[1]+'-'+tgl_lahir[0]
				},
				success: function(data) {
					//Reset form
					$('#no_rm_confirm').val("");
					$('#nama_confirm').val("");
					$('#tgl_lahir_confirm').val("");
					$('#jenis_kelamin_confirm').val("");
					$('#alamat_confirm').val("");


					$('#no_rm_confirm').val(data.response.no_rm);
					$('#nama_confirm').val(data.response.title +". " +data.response.nama);
					$('#tgl_lahir_confirm').val(data.response.tanggal_lahir);
					
					if(data.response.jenis_kelamin == 'L'){
						$('#jenis_kelamin_confirm').val("Laki-laki");
					}
					else if(data.response.jenis_kelamin == 'P'){
						$('#jenis_kelamin_confirm').val("Perempuan");
					}
					else{
						$('#jenis_kelamin_confirm').val("Tidak Diketahui");
					}

					$('#alamat_confirm').val(data.response.alamat);

					if(data.success){
						$.ajax({
							type: "POST",
							url: "<?php echo base_url(); ?>" + "Pendaftaran/check_kunjungan_pasien",
							dataType: "JSON",
							data: {
								nomr: nomr
							},
							success: function(data) {
								NProgress.done();
								if(data.success){
									Swal.fire({
										title: '<h1 class="text-danger"><b>Anda Sudah Melakukan Pendaftaran Hari Ini</b></h1>',
										html: '<table class="table table-sm table-borderless text-left"><tr><td>No Pendaftaran</td><td>'+data.response.kode+'</td></tr><tr><td>No RM</td><td>'+data.response.no_rm+'</td></tr><tr><td>Poli Tujuan</td><td>'+data.response.nama_layanan+'</td></tr></table>',
										type: 'warning',
										showCancelButton: true,
										confirmButtonColor: '#3085d6',
										cancelButtonColor: '#d33',
										confirmButtonText: 'Edit Pendaftaran',
										cancelButtonText: 'Batal'
									}).then((result) => {
										if(result.value){
											var next_step = true;
											parent_fieldset.find('.required').each(function() {
												if( $(this).val() == "" ) {

													$(this).addClass('input-error');

													next_step = false;

												}
												else {

													$(this).removeClass('input-error');

												}
											});

											// fields validation

											if( next_step ) {

												parent_fieldset.fadeOut(400, function() {

													// change icons

													current_active_step.removeClass('active').addClass('activated').next().addClass('active');

													// progress bar

													bar_progress(progress_line, 'right');

													// show next step

													$(this).next().fadeIn();

													// scroll window to beginning of the form

													scroll_to_class( $('.form-wizard'), 20 );

												});

											}
										}
										else{

										}
									})


								}else{
									var next_step = true;
									//CEK SUDAH DAFTAR
									parent_fieldset.find('.required').each(function() {
										if( $(this).val() == "" ) {

											$(this).addClass('input-error');

											next_step = false;

										}
										else {

											$(this).removeClass('input-error');

										}
									});

									// fields validation

									if( next_step ) {

										parent_fieldset.fadeOut(400, function() {

											// change icons

											current_active_step.removeClass('active').addClass('activated').next().addClass('active');

											// progress bar

											bar_progress(progress_line, 'right');

											// show next step

											$(this).next().fadeIn();

											// scroll window to beginning of the form

											scroll_to_class( $('.form-wizard'), 20 );

										});

									}
								}
								// toastr.success('Data Ditambahkan')
							}
						});
					}else{
						NProgress.done();
						toastr.error(data.response);
					}
					// toastr.success('Data Ditambahkan')
				}
			});
		}
	});

	$( ".btn-next-poli" ).on('click',function() {
		
		var poli=$('#poli').val();

		var parent_fieldset = $(this).parents('fieldset');
		var current_active_step = $(this).parents('.form-wizard').find('.form-wizard-step.active');
		var progress_line = $(this).parents('.form-wizard').find('.form-wizard-progress-line');

		if(poli){
			var next_step = true;
			parent_fieldset.find('.required').each(function() {

				if( $(this).val() == "" ) {

					$(this).addClass('input-error');

					next_step = false;

				}

				else {

					$(this).removeClass('input-error');

				}

			});

			// fields validation

			

			if( next_step ) {

				parent_fieldset.fadeOut(400, function() {

					// change icons

					current_active_step.removeClass('active').addClass('activated').next().addClass('active');

					// progress bar

					bar_progress(progress_line, 'right');

					// show next step

					$(this).next().fadeIn();

					// scroll window to beginning of the form

					scroll_to_class( $('.form-wizard'), 20 );

				});

			}
		}
		else{
			toastr.error("Poli Tujuan Harus Diisi");
		}	
	});

	$('#jenispasien').on('change', function(){
		var jenispasien = $("#jenispasien").val();
		if(jenispasien == "1"){
			$("#form-jenis-bpjs").show();
			$('#form-no-anggota').show();
			document.getElementById('lbnomoranggota').innerHTML = 'Nomor BPJS'
		}
		else if(jenispasien == "99"){
			$("#form-jenis-bpjs").hide();
			$('#form-no-anggota').hide();
		}
		else{
			$("#form-jenis-bpjs").hide();
			$('#form-no-anggota').show();
			document.getElementById('lbnomoranggota').innerHTML = 'Nomor Anggota'
		}
	})

	$( ".btn-confirmation-reg" ).click(function() {
		var nama = $('#nama_confirm').val();
		var alamat = $('#alamat_confirm').val();
		var tgl_lahir = $('#tgl_lahir').val();
		var norm = $('#nomr').val();
		var poli=$('select[name=poli] option:selected').text();
		var tgl_kunjungan= $('input[name=tgl_daftar]').val();
		var jenispasien= $('select[name=jenispasien] option:selected').text();
		var no_anggota= $('input[name=no_anggota]').val();
		var no_rujukan= $('input[name=no_rujukan]').val();
		

		$('#knama').html(nama);

		$('#knomr').html(norm);

		$('#ktgl_lahir').html(tgl_lahir);

		$('#kalamat').html(alamat);

		$('#kpoli').html(poli);

		$('#ktgl_kunjungan').html(tgl_kunjungan);

		$('#kjenis_pasien').html(jenispasien);

		$('#kno_anggota').html(no_anggota);

		$("#kno_rujukan").html(no_rujukan);

	});

	$( "#btn-reg-submit" ).click(function() {
		var tgl_lahir=$('input[name=tgl_lahir]').val();
		var nomr=$('input[name=nomr]').val();

		var poli=$('select[name=poli]').val();
		var tgl_kunjungan=$('input[name=tgl_daftar]').val();
		var jenispasien=$('select[name=jenispasien]').val();
		var no_anggota=$('input[name=no_anggota]').val();
		var no_rujukan=$('input[name=no_rujukan]').val();
		var no_surat=$('input[name=no_surat]').val();
		


		$(".se-pre-con").fadeIn("slow");
		$.ajax({
			type: "POST",
			dataType: "JSON",
			url: "<?php echo base_url();?>Pendaftaran/proses_reg",
			data: {
				tgl_lahir: tgl_lahir,
				nomr:nomr,
				poli:poli,
				tgl_kunjungan:tgl_kunjungan,
				jenispasien:jenispasien,
				no_anggota:no_anggota,
				no_rujukan:no_rujukan,
				no_surat: no_surat
			},
			success: function(data){
				setTimeout(function() {
					$(".se-pre-con").fadeOut("slow");
					if(data.status==false){
							alert('Maaf Sedang Ada Masalah Pada Sistem, Pendaftaran Anda Gagal!');
					}else{
						window.location.href = "<?php echo base_url();?>"+"Pendaftaran/berhasil/"+data.kode;
					}
				}, 1000);
			}
		});
	});
})
</script>
                
                
                    
					
						
                    		
							
							
                    		
                    		
							
							
								  
									

									 
								

							
							
							
							
                    	
                    	
                    </div>
                </div>
                    
            </div>
        </section>
    </div>