<section class="content mt-2">
  <div class="container-fluid">
    <div class="row">
      <div class="col-12">
        <div class="card card-warning card-outline">
          <div class="card-header">
            <h3 class="text-center"><span class="text-center">Ketentuan Pendaftaran Online</span></h3>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <div class="row">
              <div class="col-12">
                <p><b>Jadwal Pelayanan Loket Pendaftaran :	Senin - Kamis (07.30 - 11.00 WIB), Jum'at & Sabtu (07.30-10.00 WIB).</b></p>
                <p>1.	Pendaftaran Online hanya berlaku bagi pasien yang telah memiliki Nomor Rekam Medis (No RM) di RSUD R.A. Basoeni Kabupaten Mojokerto.</p>
                <p>2.	Pendaftaran Online berlaku untuk pasien BPJS Kesehatan dan pasien Umum.</p>
                <p>3.	Pendaftaran Online dapat dilakukan untuk kontrol ke Poliklinik dengan Jadwal H-1 dari tanggal kunjungan.</p>
                <p>4.	Bagi pasien yang telah melakukan pendaftaran Online, akan mendapatkan Bukti pendaftaran yang dapat dicetak (atau menunjukan kode booking pada HP) dan dibawa pada Hari Berobat.</p>
                <p>5.	Bukti Pendaftaran Online ditunjukkan kepada petugas loket untuk konfirmasi kedatangan pasien.</p>
                <p>6.	Pasien BPJS yang datang berobat wajib membawa kelengkapan berkas. Seperti kartu BPJS, KTP, surat rujukan dan surat kontrol.</p>
                <p>7.	Pasien yang telah melakukan registrasi online diharapkan datang tepat waktu.</p>
              </div>
            </div>
            <div class="row">
              <div class="col-12 d-flex justify-content-center">
                <a href="<?php echo base_url()?>/Pendaftaran/form_reg" class="btn btn-md btn-warning text-center">DAFTAR</a>
              </div>
            </div>
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </div>
</section>
<!-- /.content -->

<script>
$(document).ready(function(){
  

})
</script>