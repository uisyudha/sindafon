<div class="container-fluid main"> 

<div class="card" >
  <div class="card-heading">
  	<div class="text-center"> <h3>Checklist</h3></div>

    <div><button class="btn btn-sm btn-success" data-toggle="modal" data-target="#ModalaAdd"><span class="fa fa-plus"></span> Tambah Checklist</button>



    </div>
  </div>
  <div class="card-body">
 
 


            <div class="table-responsive">
              <table id="table-jadwal" >
              	<thead>
					<tr><th>No</th>
                    <th>Tipe</th>
                    <th>Keterangan</th>
                    <th>Tindakan</th>
					</tr>
					</thead>
					<tbody id="showdata">
					</tbody>
              </table>
          </div>
     
  </div>


</div>
 

</div>


<!-- MODAL ADD -->
        <div class="modal fade" id="ModalaAdd"  tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
            <div class="modal-dialog">
            <div class="modal-content">
            <div class="modal-header">
               
                <h4 class="modal-title" id="myModalLabel">Tambah Aturan</h4>
                 <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <form class="form-horizontal">
                <div class="modal-body">
 
                     <div class="form-group">
                        <label class="control-label col-xs-3" >Tipe</label>
                        <div class="col-xs-9">
                        	 <input name="id" id="id_aturan" class="form-control" type="hidden" value="">
                            <select class="form-control" id="tipe" name="tipe" style="width:335px;" >
                               
                              <?php for($a=1;$a<=count($tipe);$a++){
                                echo '<option value="'.$a.'">'.$tipe[$a].'</option>';
                              }
                              ?>
                            
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-xs-3" >Keterangan</label>
                        <div class="col-xs-9">
                            <textarea class="form-control" name="keterangan" id="keterangan"></textarea> 
                        </div>
                    </div>                  
                      
 
                </div>
 
                <div class="modal-footer">
                    <button class="btn" data-dismiss="modal" aria-hidden="true">Tutup</button>
                    <button class="btn btn-info" id="btn_simpan">Simpan</button>
                </div>
            </form>
            </div>
            </div>
        </div>
<!--END MODAL ADD-->

<script type="text/javascript">
 

$(document).ready(function() {

    var tabel=$('#table-jadwal').DataTable({
	    	dom: 'Bfrtip',
		    buttons: [
		        'copy', 'excel', 'csv'
		    ],
        "ajax": {
            url : "<?php echo site_url("adminrs/view_aturan") ?>",
            type : 'GET'
        },

    });



    $('#ModalaAdd').on('shown.bs.modal',function(e){
     
      
    })

    //SHOW DATA
    $('#showdata').on('click','.item_edit',function(){
            var id=$(this).attr('data');
            $.ajax({
                type : "GET",
                url  : "<?php echo base_url('adminrs/get_aturan')?>",
                dataType : "JSON",
                data : {id:id},
                success: function(data){
                   
                    $('#ModalaAdd').modal('show');
                     $('#id_aturan').val(data.id_aturan);
                    if(data.tipe!=null)
                        $('#tipe option[value='+data.tipe+']').prop('selected', true);  // To select via value
                    else
                        $('#tipe option')[0].selected = true;

                  
                       $('#keterangan').val(data.keterangan); 

               
                }
            });
            return false;
        });


    //Simpan Barang
    $('#btn_simpan').on('click',function(){
            
            
            var id_aturan=$('#id_aturan').val();
            var tipe=$('#tipe').val();
            var keterangan=$('#keterangan').val();
            
             
            $.ajax({
                type : "POST",
                url  : "<?php echo base_url('adminrs/simpan_aturan')?>",
                dataType : "JSON",
                data : {id:id_aturan , tipe:tipe, keterangan:keterangan },
                success: function(data){
                  	console.log('aaa');
                  	console.log(data);
                    $('#id_aturan').val('');
                    
                    $('#tipe option')[0].selected = true;
                    $('#keterangan').val('');

                    $('#ModalaAdd').modal('hide');
                    tabel.ajax.reload();
                    alert(data);
                }
            });
            return false;
        });


      //GET HAPUS
    $('#showdata').on('click','.item_hapus',function(){

      var id=$(this).attr('data');
           if(confirm('Data Akan dihapus, anda yakin?')){
                                 
                $.ajax({
                    type : "POST",
                    url  : "<?php echo base_url('adminrs/hapus_aturan')?>",
                    dataType : "JSON",
                    data : {id:id},
                    success: function(data){
                      	
                        tabel.ajax.reload();

                    }
                });
               
           }

            return false;
        });
 


});

</script>