<?php  
  date_default_timezone_set('Asia/Jakarta');
  $department=$this->session->userdata('department');
?>

<!-- Navbar -->
<nav class="main-header navbar navbar-expand border-bottom navbar-warning">
  <!-- <audio id="notif-sound" controls muted autoplay loop> 
    <source src="<?php echo base_url();?>/assets/sound/notif.mp3" type="audio/mpeg">
  </audio> -->
  <!-- Left navbar links -->
  <ul class="navbar-nav">
    <li class="nav-item">
      <a class="nav-link text-dark" data-widget="pushmenu" href="#" id="btn-menu-sidebar">
        <i class="fa fa-bars mt-1"></i>
      </a>
    </li>
    <li class="nav-item d-none d-sm-inline-block">
      <a href="" class="nav-link text-dark"><b>Sistem Informasi Pendaftaran Online RSUD RA Basoeni</b></a>
    </li>
  </ul>

  <div class="form-inline ml-3">
    <div class="input-group input-group-sm">
      <span class="text-bold text-white" id="tanggal"></span>
    </div>
  </div>

  <!-- SEARCH FORM 
  <form class="form-inline ml-3">
    <div class="input-group input-group-sm">
      <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
      <div class="input-group-append">
        <button class="btn btn-navbar" type="submit">
          <i class="fa fa-search"></i>
        </button>
      </div>
    </div>
  </form>
  -->

  <!-- Right navbar links -->
  <ul class="navbar-nav ml-auto">
    <!-- Messages Dropdown Menu -->
    <!-- <li class="nav-item dropdown">
      <a class="nav-link" data-toggle="dropdown" href="#">
        <i class="fa fa-comments-o"></i>
        <span class="badge badge-danger navbar-badge">3</span>
      </a>
      <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
        <a href="#" class="dropdown-item"> -->
          <!-- Message Start -->
          <!-- <div class="media">
            <img src="dist/img/user1-128x128.jpg" alt="User Avatar" class="img-size-50 mr-3 img-circle">
            <div class="media-body">
              <h3 class="dropdown-item-title">
                Brad Diesel
                <span class="float-right text-sm text-danger"><i class="fa fa-star"></i></span>
              </h3>
              <p class="text-sm">Call me whenever you can...</p>
              <p class="text-sm text-muted"><i class="fa fa-clock-o mr-1"></i> 4 Hours Ago</p>
            </div>
          </div> -->
          <!-- Message End -->
        <!-- </a>
        <div class="dropdown-divider"></div>
        <a href="#" class="dropdown-item"> -->
          <!-- Message Start -->
          <!-- <div class="media">
            <img src="dist/img/user8-128x128.jpg" alt="User Avatar" class="img-size-50 img-circle mr-3">
            <div class="media-body">
              <h3 class="dropdown-item-title">
                John Pierce
                <span class="float-right text-sm text-muted"><i class="fa fa-star"></i></span>
              </h3>
              <p class="text-sm">I got your message bro</p>
              <p class="text-sm text-muted"><i class="fa fa-clock-o mr-1"></i> 4 Hours Ago</p>
            </div>
          </div> -->
          <!-- Message End -->
        <!-- </a>
        <div class="dropdown-divider"></div>
        <a href="#" class="dropdown-item"> -->
          <!-- Message Start -->
          <!-- <div class="media">
            <img src="dist/img/user3-128x128.jpg" alt="User Avatar" class="img-size-50 img-circle mr-3">
            <div class="media-body">
              <h3 class="dropdown-item-title">
                Nora Silvester
                <span class="float-right text-sm text-warning"><i class="fa fa-star"></i></span>
              </h3>
              <p class="text-sm">The subject goes here</p>
              <p class="text-sm text-muted"><i class="fa fa-clock-o mr-1"></i> 4 Hours Ago</p>
            </div>
          </div> -->
          <!-- Message End -->
        <!-- </a>
        <div class="dropdown-divider"></div>
        <a href="#" class="dropdown-item dropdown-footer">See All Messages</a>
      </div>
    </li> -->
    <!-- User Dropdown Menu -->
  </ul>
</nav>
<!-- /.navbar -->