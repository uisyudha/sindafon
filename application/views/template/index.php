<?php
 date_default_timezone_set('Asia/Jakarta');

 ?>
 <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>	
	<meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>SINDAFON</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="shortcut icon" href="<?php echo base_url(); ?>/assets/img/basoeni_logo.ico" type="image/x-icon" />

	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/form.css" >
	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/slick.css" >
	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/slick-theme.css" >

  <!-- Font Awesome -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>/plugins/fontawesome-free/css/all.min.css">
	<!-- Ionicons -->
  <!-- iCheck -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>/plugins/iCheck/all.css">
  <!-- Morris chart -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>/plugins/morris/morris.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>/plugins/datepicker/datepicker3.css">
	<!-- DataTables -->
	<!-- <link rel="stylesheet" href="<?php echo base_url(); ?>/plugins/datatables/jquery.dataTables.min.css"> -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>/plugins/datatables/rowReorder.dataTables.min.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>/plugins/datatables/responsive.dataTables.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>/plugins/datatables/dataTables.bootstrap4.min.css">
	<!-- <link rel="stylesheet" href="<?php echo base_url(); ?>/plugins/bootstrap/css/bootstrap.css"> -->
  <!-- Daterange picker -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>/plugins/daterangepicker/daterangepicker.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
	<!-- Select2 -->
	<link rel="stylesheet" href="<?php echo base_url();?>/plugins/select2/css/select2.min.css">
	<link rel="stylesheet" href="<?php echo base_url();?>/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
	<!-- Bootstrap4 Duallistbox -->
  <link rel="stylesheet" href="<?php echo base_url();?>/plugins/bootstrap4-duallistbox/bootstrap-duallistbox.min.css">
	<!-- Custom css -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>/dist/static/css/main.min.css">
	<!-- Sweetalert2 css -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css">
	<!-- Table Export css -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>/plugins/table-export/dist/css/tableexport.min.css">
	<!-- overlayScrollbars -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>/plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
	<!-- summernote -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>/plugins/summernote/summernote-bs4.css">
	<!-- Toastr -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>/plugins/toastr/toastr.min.css">
	<!-- NProgress -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>/plugins/nprogress/nprogress.css">
	<script src="<?php echo base_url();?>/plugins/nprogress/nprogress.js"></script>
	<!-- Theme style -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>/dist/css/adminlte.min.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/custom.css">
	<!-- Google Font: Source Sans Pro -->
	<link href="<?php echo base_url(); ?>/assets/css/google-font.css" rel="stylesheet">
	<!-- jQuery -->
	<script src="<?php echo base_url(); ?>/plugins/jquery/jquery.min.js"></script>
	<!-- <link rel="stylesheet" href="<?php echo base_url();?>/assets/css/trumbowyg.min.css"/>
	<link rel="stylesheet" href="<?php echo base_url();?>/assets/css/bootstrap.min.css"  />
	<link rel="stylesheet" href="<?php echo base_url();?>/assets/css/bootstrap-theme.min.css" />
	<link rel="stylesheet" href="<?php echo base_url();?>/assets/css/style.css" type="text/css"  />
	<link rel="stylesheet" href="<?php echo base_url();?>/assets/css/jquery-ui.css">
	<link rel="stylesheet" href="<?php echo base_url();?>/assets/css/font-awesome.min.css">
	<link rel="stylesheet" href="<?php echo base_url();?>/assets/css/tableexport.min.css">
	<link rel="stylesheet" href="<?php echo base_url();?>/assets/css/jquery.datetimepicker.min.css"> -->
	

	<!-- Latest compiled and minified JavaScript -->
	<!-- <script src="<?php echo base_url();?>/assets/js/jquery-3.2.1.min.js" ></script>
	<script src="<?php echo base_url();?>/assets/js/bootstrap.min.js" ></script>
	<script src="<?php echo base_url();?>/assets/js/jquery-ui.min.js"></script>
	<script src="<?php echo base_url();?>/assets/js/jQuery.print.min.js"></script>
	<script src="<?php echo base_url();?>/assets/js/jquery.datetimepicker.full.min.js"></script> -->

	<!-- trumbowyg Plugin -->
 	
 	<!-- <script src="<?php echo base_url();?>/assets/js/trumbowyg.min.js"></script>
 	<script src="<?php echo base_url();?>/assets/js/id.min.js"></script> -->


	<!-- export xls plugin -->
	<!-- <script src="<?php echo base_url();?>/assets/js/xlsx.core.min.js"></script>
	<script src="<?php echo base_url();?>/assets/js/FileSaver.min.js"></script>
	<script src="<?php echo base_url();?>/assets/js/tableexport.min.js"></script> -->

	<!-- TABLE PLUGIN -->
	<!-- <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>/assets/css/datatables.min.css"/>
 	<script type="text/javascript" src="<?php echo base_url();?>/assets/js/datatables.min.js"></script> -->

 	<!-- CONTEXT MENU PLUGIN -->
 	<!-- <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>/assets/css/jquery.contextMenu.min.css"/>
 	<script type="text/javascript" src="<?php echo base_url();?>/assets/js/jquery.ui.position.min.js"></script>
 	<script type="text/javascript" src="<?php echo base_url();?>/assets/js/jquery.contextMenu.min.js"></script> -->


 	
	
<!-- 	export pdf
<script  src="<?php echo base_url();?>/assets/js/tableExport.js"></script>
<script src="<?php echo base_url();?>/assets/js/jquery.base64.js"></script>
<script src="<?php echo base_url();?>/assets/js/jspdf/libs/sprintf.js"></script>
<script src="<?php echo base_url();?>/assets/js/jspdf/jspdf.js"></script>
<script src="<?php echo base_url();?>/assets/js/jspdf/libs/base64.js"></script>
 -->

	<?php
    if (isset($output)) {
        if ($output->css_files!==''&&$output->js_files!=='') {
            foreach ($output->css_files as $file): ?>
	<link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
	<?php endforeach; ?>
	<?php foreach ($output->js_files as $file): ?>
	<script src="<?php echo $file; ?>"></script>
	<?php endforeach;
        }
    }
?>


</head>
<body class="hold-transition sidebar-mini layout-fixed">
	<div class="wrapper">
	
		<?php
      echo $header;
      echo $sidebar;
    ?>

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- <?php echo $broadcumb; ?> -->

		<!-- <marquee behavior="scroll" direction="left" bgcolor="yellow" style="font-family: impact; font-size:24px; color: #cc0000;">" Loket => Tolong ISIKAN NO HP dan NO KTP untuk pendataan VCLAIM"</marquee> -->
							
			<!-- div id="container_master_bg">
				<div id="container" style="min-height:300px"> -->
					<?php
										/*
										* Variabel $contentnya diambil dari libraries template.php
										* (application/libraries/template.php)
										* */
						echo $content; 
					?>
				<!-- </div>
			</div> -->
			<!-- end loader-wrapper -->	

			<!-- </footer > -->
		</div>
						<!-- <footer class="site-footer" > -->
							<?php
						/*
						* Variabel $footernya diambil dari libraries template.php
						* (application/libraries/template.php)
						* */
						echo $footer; ?>
		<!-- /.content-wrapper -->
	</div>

<!-- jQuery -->
<!-- <script src="<?php echo base_url(); ?>/plugins/jquery/jquery.min.js"></script> -->
<!-- jQuery UI 1.11.4 -->
<script src="<?php echo base_url(); ?>/plugins/jQueryUI/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src="<?php echo base_url(); ?>/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- Morris.js charts -->
<script src="<?php echo base_url(); ?>/plugins/morris/raphael-min.js"></script>
<script src="<?php echo base_url(); ?>/plugins/morris/morris.min.js"></script>
<!-- Sparkline -->
<script src="<?php echo base_url(); ?>/plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="<?php echo base_url(); ?>/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="<?php echo base_url(); ?>/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="<?php echo base_url(); ?>/plugins/knob/jquery.knob.js"></script>
<!-- daterangepicker -->
<script src="<?php echo base_url(); ?>/plugins/daterangepicker/moment.min.js"></script>
<script src="<?php echo base_url(); ?>/plugins/daterangepicker/daterangepicker.min.js"></script>
<!-- datepicker -->
<script src="<?php echo base_url(); ?>/plugins/datepicker/bootstrap-datepicker.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="<?php echo base_url(); ?>/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<!-- <script src="<?php echo base_url(); ?>/plugins/slimScroll/jquery.slimscroll.min.js"></script> -->
<!-- overlayScrollbars -->
<script src="<?php echo base_url(); ?>/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<!-- FastClick -->
<!-- <script src="<?php echo base_url(); ?>/plugins/fastclick/fastclick.js"></script> -->
<!-- DataTables -->
<script src="<?php echo base_url(); ?>/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>/plugins/datatables/dataTables.bootstrap4.js"></script>
<script src="<?php echo base_url(); ?>/plugins/datatables/dataTables.rowReorder.min.js"></script>
<script src="<?php echo base_url(); ?>/plugins/datatables/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url(); ?>/plugins/datatables/responsive.bootstrap4.min.js"></script>
<script src="<?php echo base_url(); ?>/plugins/datatables/dataTables.buttons.min.js"></script>
<script src="<?php echo base_url(); ?>/plugins/datatables/buttons.bootstrap4.min.js"></script>
<!-- <script src="<?php echo base_url(); ?>/plugins/datatables/buttons.flash.min.js"></script> -->
<script src="<?php echo base_url(); ?>/plugins/datatables/jszip.min.js"></script>
<script src="<?php echo base_url(); ?>/plugins/datatables/pdfmake.min.js"></script>
<script src="<?php echo base_url(); ?>/plugins/datatables/vfs_fonts.js"></script>
<script src="<?php echo base_url(); ?>/plugins/datatables/buttons.html5.min.js"></script>
<script src="<?php echo base_url(); ?>/plugins/datatables/buttons.print.min.js"></script>
<script src="<?php echo base_url(); ?>/plugins/datatables/buttons.colVis.min.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url(); ?>/dist/js/adminlte.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<!-- <script src="<?php echo base_url(); ?>/dist/js/pages/dashboard.js"></script> -->
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url(); ?>/dist/js/demo.js"></script>
<!-- Select2 -->
<script src="<?php echo base_url(); ?>/plugins/select2/js/select2.full.min.js"></script>
<!-- Sweetalert2 -->
<script src="<?php echo base_url(); ?>/plugins/sweetalert2/sweetalert2.all.js"></script>
<!-- InputMask -->
<script src="<?php echo base_url(); ?>/plugins/input-mask/jquery.inputmask.js"></script>
<script src="<?php echo base_url(); ?>/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="<?php echo base_url(); ?>/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- bootstrap color picker -->
<script src="<?php echo base_url(); ?>/plugins/colorpicker/bootstrap-colorpicker.min.js"></script>
<!-- bootstrap time picker -->
<script src="<?php echo base_url(); ?>/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- iCheck 1.0.1 -->
<script src="<?php echo base_url(); ?>/plugins/iCheck/icheck.min.js"></script>
<!-- <script src="<?php echo base_url();?>/assets/js/jQuery.print.min.js"></script> -->
<!-- Bootstrap Switch -->
<script src="<?php echo base_url(); ?>/plugins/bootstrap-switch/js/bootstrap-switch.min.js"></script>
<script src="<?php echo base_url();?>/assets/js/jQuery.print.min.js"></script>
<!-- Bootstrap4 Duallistbox -->
<script src="<?php echo base_url();?>/plugins/bootstrap4-duallistbox/jquery.bootstrap-duallistbox.min.js"></script>
<!-- Table Export -->
<script src="<?php echo base_url();?>/plugins/js-xlsx/dist/xlsx.core.min.js"></script>
<script src="<?php echo base_url();?>/plugins/filesaver/FileSaver.min.js"></script>
<script src="<?php echo base_url();?>/plugins/table-export/dist/js/tableexport.min.js"></script>
<!-- Toastr -->
<script src="<?php echo base_url();?>/plugins/toastr/toastr.min.js"></script>
<!-- Setting JS -->
<!-- <script src="<?php echo base_url();?>/assets/js/setting.js"></script> -->
<!-- page script -->
<!-- <script src="<?php echo base_url(); ?>/dist/static/js/main.min.js"></script> -->

<script src="<?php echo base_url();?>assets/js/popper.min.js" ></script>
<script src="<?php echo base_url();?>assets/js/slick.min.js" ></script>


<script src="<?php echo base_url();?>assets/js/main.js" ></script>
<script src="<?php echo base_url();?>assets/js/form.js" ></script>

</body>
</html>