<!-- Main Sidebar Container -->
<aside class="main-sidebar elevation-4 sidebar-light-warning">
  <!-- Brand Logo -->
  <a href="<?php echo base_url();?>" class="brand-link navbar-warning">
    <img src="<?php echo base_url(); ?>/assets/img/basoeni_logo.png" alt="RSUD RA Basoeni" class="brand-image img-circle elevation-3"
          style="opacity: .8">
    <span class="brand-text font-weight-medium text-dark" >SINDAFON</span>
  </a>

  <!-- Sidebar -->
  <div class="sidebar">
    <!-- Sidebar user panel (optional) -->
    

    <!-- Sidebar Menu -->
    <nav class="mt-2">
      <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="true">
        <li class="nav-item <?php if ( $this->session->userdata('menu') == 'dashboard' ) echo 'menu-open';?>">
          <a href="<?php echo base_url();?>Pendaftaran/form_reg" class="nav-link <?php if ( $this->session->userdata('menu') == 'daftar') echo 'active';?>">
            <i class="nav-icon fas fa-sign-in-alt text-dark"></i>
            <p>
              Daftar
            </p>
          </a>
        </li>
        <li class="nav-item <?php if ( $this->session->userdata('menu') == 'jadwal_poli' ) echo 'menu-open';?>">
          <a href="<?php echo base_url().'Pendaftaran/jadwal_poli';?>" class="nav-link <?php if ( $this->session->userdata('menu') == 'jadwal_poli') echo 'active';?>">
            <i class="nav-icon fas fa-calendar-alt text-dark"></i>
            <p>
              Jadwal Poli
            </p>
          </a>
        </li>
        <li class="nav-item <?php if ( $this->session->userdata('menu') == 'check_pendaftaran' ) echo 'menu-open';?>">
          <a href="<?php echo base_url().'pendaftaran/check_pendaftaran';?>" class="nav-link <?php if ( $this->session->userdata('menu') == 'dashboard') echo 'active';?>">
            <i class="nav-icon fas fa-check-circle text-dark"></i>
            <p>
              Cek Pendaftaran
            </p>
          </a>
        </li>
        <li class="nav-item <?php if ( $this->session->userdata('menu') == 'term_condition' ) echo 'menu-open';?>">
          <a href="<?php echo base_url().'Pendaftaran/term_condition';?>" class="nav-link <?php if ( $this->session->userdata('menu') == 'term_condition') echo 'active';?>">
            <i class="nav-icon fas fa-question-circle text-dark"></i>
            <p>
              Syarat & Ketentuan
            </p>
          </a>
        </li>
      </ul>
    </nav>
    <!-- /.sidebar-menu -->
  </div>
  <!-- /.sidebar -->
</aside>