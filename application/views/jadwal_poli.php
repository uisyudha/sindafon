<section class="content mt-2">
  <div class="container-fluid">
    <div class="row">
      <div class="col-12">
        <div class="card card-warning card-outline">
          <div class="card-header">
            <h3 class="card-title">Jadwal Poli</h3>
            <div class="card-tools">
              <button type="button" id="daftar-pasien" class="btn btn-tool" data-card-widget="collapse"><i class="fa fa-minus"></i>
              </button>
              <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fa fa-times"></i>
              </button>
            </div>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <div class="row">
              <div class="col-12">
                <table id="table-pasien" class="table table-sm table-bordered dt-responsive display nowrap" style="width:100%">
                  <thead>
                  <tr>
                    <th>No</th>
                    <th>Poliklinik</th>
                    <th>Nama Dokter</th>
                    <th>Hari</th>
                    <th>Jam</th>
                  </tr>
                  </thead>
                  <tbody id="table-pasien-data"> 
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </div>
</section>
<!-- /.content -->

<script>
$(document).ready(function(){
  // Initialize Select2 Elements in modal
  // $("select").select2().on("select2:open", function(e) {
  // 	//quick focus fight fix
  // 	$(".select2-dropdown").parent(".select2-container").on("focusin", function(e) {
  // 		e.stopPropagation();
  // 	});
  // });
  //Datemask dd-mm-yyyy
  $(".select2").select2();
  $('#datemask').inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' })
  $('[data-mask]').inputmask()

  //Ajax Datatable for Layanan
  var table_pasien = $("#table-pasien").DataTable({
    ajax: {
      url: "<?php echo base_url();?>/Pendaftaran/jadwal_dokter_data_server_side",
      type: "POST"
    },
    
    processing: true,
    serverSide: true,
    paging: true,
    lengthChange: true,
    searching: true,
    language: {
      searchPlaceholder: "Cari"
    },
    ordering: true,
    info: true,
    autoWidth: true,
    // rowReorder: {
    // 	selector: "td:nth-child(2)"
    // },
    responsive: true,
    "columns": [
      { "data": "no" },
      { "data": "nama" },
      { "data": "nama_dokter" },
      { "data": "keterangan_hari" },
      { "data": "keterangan_jam" },
    ],
    "columnDefs": [
      {
        "targets": [-1], 
        "orderable": false, //set not orderable
      },
      // { responsivePriority: 1, targets: 0 },
      // { responsivePriority: 1, targets: -1 }
    ],	 
  });

  //table_kamar.ajax.reload();

  // End of Ajax Datatable Layanan

})
</script>