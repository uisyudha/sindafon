<section class="content mt-2">
  <div class="container-fluid">
    <div class="row">
      <div class="col-12">
        <div class="card card-warning card-outline">
          <div class="card-header">
            <h3 class="card-title">Check Pendaftaran</h3>
            <div class="card-tools">
              <button type="button" id="daftar-pasien" class="btn btn-tool" data-card-widget="collapse"><i class="fa fa-minus"></i>
              </button>
              <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fa fa-times"></i>
              </button>
            </div>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <div class="row">
              <div class="col-12">
                <form>
                  <div class="form-group">
                    <label class="control-label" for="nomr">No RM</label>
                    <input id="nomr" name="nomr" autocomplete="off" placeholder="012345" class="form-control required" type="text" >   
                    <small class="form-text text-muted">*nomor yang ada di kartu pasien</small> 
                  </div>
                  <div class="form-group">
                    <label class="  control-label" for="textinput">Tanggal Berkunjung</label>
                    <input id="tgl_kunjungan" autocomplete="off" name="tgl_kunjungan" placeholder="20-12-2000" class="form-control required" type="text" value="" required>
                  </div>
                  <div class="form-wizard-buttons">
                    <button type="button" class="btn btn-warning btn-nomr">Submit</button>
                  </div>
                </form>
              </div>
            </div>
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </div>
</section>
<!-- /.content -->

<script>
$(document).ready(function(){
  $( ".btn-nomr" ).on('click',function() {
		
		var mr=$('input[name=nomr]').val();

		if(mr.length!=6){
			toastr.error('No RM harus 6 digit');
			// throw new Error("No RM harus 6 digit");
		}
		else if(isNaN(mr)){
			toastr.error('No RM harus angka');
			// throw new Error("No RM harus angka");
		}
		else{
			NProgress.start();
			var nomr = $('#nomr').val();
			var tgl_kunjungan = $('#tgl_kunjungan').val().split("-");
      $.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>" + "Pendaftaran/check_pendaftaran_pasien",
        dataType: "JSON",
        data: {
          nomr: nomr,
          tgl_kunjungan: tgl_kunjungan[2]+'-'+tgl_kunjungan[1]+'-'+tgl_kunjungan[0]
        },
        success: function(data) {
          NProgress.done();
          if(data.success){
            window.location.href = "<?php echo base_url();?>"+"Pendaftaran/berhasil/"+data.response.kode;
          }else{
            toastr.error("Data Tidak Ditemukan") 
          }
          // toastr.success('Data Ditambahkan')
        }
      });
		}
	});

  $( "#tgl_kunjungan" ).datepicker({ format: 'dd-mm-yyyy',
    changeMonth: true,
    changeYear: true,
    yearRange:"-100:+0"
  });  
})
</script>