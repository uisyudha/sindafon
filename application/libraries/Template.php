<?php
class Template
{
  protected $_ci;

  public function __construct()
  {
    $this->_ci = &get_instance();
  }

  public function load($content, $data = null)
  {
    /*
     * $data['headernya'] , $data['contentnya'] , $data['footernya']
     * variabel diatas ^ nantinya akan dikirim ke file views/template/index.php
     * */
    $data['header'] = $this->_ci->load->view('template/header', $data, true);
    $data['sidebar'] = $this->_ci->load->view('template/sidebar', $data, true);
    // $data['broadcumb'] = $this->_ci->load->view('template/broadcumb', $data, true);
    $data['content'] = $this->_ci->load->view($content, $data, true);
    $data['footer'] = $this->_ci->load->view('template/footer', $data, true);

    $this->_ci->load->view('template/index', $data);
  }

}
