<?php

defined('BASEPATH') or exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/JWT.php';
require APPPATH . 'libraries/ExpiredException.php';
use \Firebase\JWT\JWT;
// use \Firebase\JWT\ExpiredException;

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Jadwal_operasi extends REST_Controller
{
  const USERNAME = 'admin';
  const PASSWORD = 'kTUq3aWq';
  const KEY = "ZB9jUm0KB1";

  public function __construct($config = 'rest_jadwal_operasi')
  {
    // Construct the parent class
    parent::__construct($config);
    $this->load->model('m_jadwal_operasi');
  }
  private function verify(){
    $headers=array();
    foreach (getallheaders() as $name => $value) {
        $headers[$name] = $value;
    }

    try {
      $decoded = JWT::decode($headers['x-token'], self::KEY, array('HS256'));
      return true;
    } catch (\Exception $e) {
      $result = array(
        "metadata" => array(
          "message" => "Invalid or expired token",
          "code" => 401
        )
      );
      $this->set_response($result, REST_Controller::HTTP_UNAUTHORIZED);
    }
  }

  public function token_post()
  {
    if($this->post('username') != self::USERNAME || $this->post('password') != self::PASSWORD){
      $result = array(
        "response" => "Invalid Authentication",
        "metadata" => array(
          "message" => "Ok",
          "code" => 401
        )
      );

      $this->set_response($result, REST_Controller::HTTP_UNAUTHORIZED);
    }
    else if($this->post('username') == self::USERNAME && $this->post('password') == self::PASSWORD){ 
      $payload = array(
        "iss" => "rsudrabasoeni",
        "aud" => "bpjs",
        "iat" => time(),
        "exp" => time() + 60*60,
      );
      $jwt = JWT::encode($payload, self::KEY);
      // $decoded = JWT::decode($jwt, self::KEY, array('HS256'));
      
      $result = array(
        "response" => array(
          "token" => $jwt
        ),
        "metadata" => array(
          "message" => "Ok",
          "code" => 200
        )
      );
      $this->set_response($result, REST_Controller::HTTP_OK);
    }
  }

  public function insert_post(){
    if($this->verify()){
      $data['kodebooking'] = $this->post('kodebooking');
      $data['tanggaloperasi'] = $this->post('tanggaloperasi');
      $data['jenistindakan'] = $this->post('jenistindakan');
      $data['kodepoli'] = $this->post('kodepoli');
      $data['namapoli'] = $this->post('namapoli');
      $data['terlaksana'] = $this->post('terlaksana');
      $data['nopeserta'] = $this->post('nopeserta');

      $id = $this->m_jadwal_operasi->insert($data);

      if($id){
        $result = array(
          "response" => $data,
          "metadata" => array(
            "message" => "Ok",
            "code" => 200
          )
        );
        $this->set_response($result, REST_Controller::HTTP_OK);
      }
      else{
        $result = array(
          "metadata" => array(
            "message" => "Insert Failed",
            "code" => 201
          )
        );
        $this->set_response($result, REST_Controller::HTTP_CREATED);
      }      
    }
  }
  
  public function update_put(){
    if($this->verify()){
      $condition['kodebooking'] = $this->put('kodebooking');
      $data['tanggaloperasi'] = $this->put('tanggaloperasi');
      $data['jenistindakan'] = $this->put('jenistindakan');
      $data['kodepoli'] = $this->put('kodepoli');
      $data['namapoli'] = $this->put('namapoli');
      $data['terlaksana'] = $this->put('terlaksana');
      $data['nopeserta'] = $this->put('nopeserta');

      $id = $this->m_jadwal_operasi->update($condition, $data);

      if($id){
        $result = array(
          "response" => $data,
          "metadata" => array(
            "message" => "Ok",
            "code" => 200
          )
        );
        $this->set_response($result, REST_Controller::HTTP_OK);
      }
      else{
        $result = array(
          "metadata" => array(
            "message" => "Update Failed",
            "code" => 201
          )
        );
        $this->set_response($result, REST_Controller::HTTP_CREATED);
      }      
    }
  }

  public function delete_delete(){
    if($this->verify()){
      $body = json_decode(@file_get_contents('php://input'));
      $condition['kodebooking'] = $body->kodebooking;
      // $condition['kodebooking'] = $this->delete('kodebooking');
      $id = $this->m_jadwal_operasi->delete($condition);

      if($id){
        $result = array(
          "metadata" => array(
            "message" => "Ok",
            "code" => 200
          )
        );
        $this->set_response($result, REST_Controller::HTTP_OK);
      }
      else{
        $result = array(
          "metadata" => array(
            "message" => "Delete Failed",
            "code" => 201
          )
        );
        $this->set_response($result, REST_Controller::HTTP_CREATED);
      }      
    }
  }
  
  public function list_kode_booking_post(){
    if($this->verify()){
      $data['nopeserta'] = $this->post('nopeserta');
      $data['terlaksana'] = 0;
      $dt = $this->m_jadwal_operasi->get($data);
      $list = array();

      foreach($dt as $d){
        $list[] = array(
          "kodebooking" => $d->kodebooking,
          "tanggaloperasi" => $d->tanggaloperasi,
          "jenistindakan" => $d->jenistindakan,
          "kodepoli" => $d->kodepoli,
          "namapoli" => $d->namapoli,
          "terlaksana" => $d->terlaksana
        );
      }

      if($list){
        $result = array(
          "response" => array(
            "list" => $list
          ),
          "metadata" => array(
            "message" => "Ok",
            "code" => 200
          )
        );
        $this->set_response($result, REST_Controller::HTTP_OK);
      }
      else{
        $result = array(
          "metadata" => array(
            "message" => "No Data",
            "code" => 201
          )
        );
        $this->set_response($result, REST_Controller::HTTP_CREATED);
      }      
    }
  }

  public function list_jadwal_harian_post(){
    if($this->verify()){
      $condition['date(tanggaloperasi) >='] = $this->post('tanggalawal');
      $condition['date(tanggaloperasi) <='] = $this->post('tanggalakhir');
      $dt = $this->m_jadwal_operasi->get($condition);
      $list = array();

      foreach($dt as $d){
        $list[] = array(
          "kodebooking" => $d->kodebooking,
          "tanggaloperasi" => $d->tanggaloperasi,
          "jenistindakan" => $d->jenistindakan,
          "kodepoli" => $d->kodepoli,
          "namapoli" => $d->namapoli,
          "terlaksana" => $d->terlaksana,
          "nopeserta" => $d->nopeserta,
          "lastupdate" => strtotime($d->lastupdate)
        );
      }

      if($list){
        $result = array(
          "response" => array(
            "list" => $list
          ),
          "metadata" => array(
            "message" => "Ok",
            "code" => 200
          )
        );
        $this->set_response($result, REST_Controller::HTTP_OK);
      }
      else{
        $result = array(
          "metadata" => array(
            "message" => "No Data",
            "code" => 201
          )
        );
        $this->set_response($result, REST_Controller::HTTP_CREATED);
      }      
    }
  }
}
