<?php

defined('BASEPATH') or exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . 'libraries/REST_Controller.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Mainapi extends REST_Controller
{

  public function __construct()
  {

    // Construct the parent class
    parent::__construct();
    $this->load->model('mapi');
    $this->load->model('mkunjungan');
    // Configure limits on our controller methods
    // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
    $this->methods['users_get']['limit'] = 500; // 500 requests per hour per user/key
    $this->methods['users_post']['limit'] = 100; // 100 requests per hour per user/key
    $this->methods['users_delete']['limit'] = 50; // 50 requests per hour per user/key
    $this->methods['pasien_put']['limit'] = 50; // 50 requests per hour per user/key
    $this->methods['pasien_put']['level'] = 1; // 50 requests per hour per user/key

  }
  public function apicheck_get()
  {

    $this->set_response([
      'status' => true,
      'message' => 'Request API Berhasil Dilakukan',

    ], REST_Controller::HTTP_OK); // NO_CONTENT (200) being the HTTP response code

  }
  public function pasien_put()
  {
    /*  $nama=$this->input->post('nama');
    $tempat_lahir=$this->input->post('tempat_lahir');
    $tanggal_lahir=$this->input->post('tanggal_lahir');
    $tanggal_lahir=>$this->input->post('alamat');
    $jenis_kelamin=>$this->input->post('jenis_kelamin');
    $alamat=>$this->input->post('alamat');
    $id_provinsi=>$this->input->post('id_provinsi');
    $id_kota=>$this->input->post('id_kota');
    $id_kecamatan=>$this->input->post('id_kecamatan');
    $id_kelurahan=>$this->input->post('id_kelurahan');
    $no_telp=>$this->input->post('no_telp');
    $no_ktp=>$this->input->post('no_ktp');
    $alamat_ktp=>$this->input->post('alamat_ktp');
    $nama_ortu=>$this->input->post('nama_ortu');
    $pekerjaan=>$this->input->post('pekerjaan');
    $marital=>$this->input->post('marital');
    $pendidikan=>$this->input->post('pendidikan');
    $nama_penanggung=>$this->input->post('nama_penanggung');
    $alamat_penanggung=>$this->input->post('alamat_penanggung');
    $no_penanggung=>$this->input->post('no_penanggung');
    $hubungan_penanggung=>$this->input->post('nama_penanggung');
    $tgl_daftar=>$this->input->post('tgl_daftar');

    if($nama!=null){
    $pasien=array();
    // $this->some_model->insert( ... );
    if($nama!=null)
    $pasien['nama']=$nama;
    if($tempat_lahir!=null)
    $pasien['tempat_lahir']=$tempat_lahir;
    if($tanggal_lahir!=null)
    $pasien['tanggal_lahir']=$tanggal_lahir;
    if($jenis_kelamin!=null)
    $pasien['jenis_kelamin']=$jenis_kelamin;
    if($alamat!=null)
    $pasien['alamat']=$alamat;
    if($id_provinsi!=null)
    $pasien['id_provinsi']=$id_provinsi;
    if($id_kota!=null)
    $pasien['id_kota']=$id_kota;
    if($id_kecamatan!=null)
    $pasien['id_kecamatan']=$id_kecamatan;
    if($id_kelurahan!=null)
    $pasien['id_kelurahan']=$id_kelurahan;
    if($no_telp!=null)
    $pasien['no_telp']=$no_telp;
    if($no_ktp!=null)
    $pasien['noKTP']=$noKTP;
    if($alamat_ktp!=null)
    $pasien['alamat_ktp']=$alamat_ktp;
    if($nama_ortu!=null)
    $pasien['nama_ortu']=$nama_ortu;
    if($pekerjaan!=null)
    $pasien['pekerjaan']=$pekerjaan;
    if($marital!=null)
    $pasien['marital']=$marital;
    if($pendidikan!=null)
    $pasien['pendidikan']=$pendidikan;
    if($nama_penanggung!=null)
    $pasien['nama_penanggung']=$nama_penanggung;
    if($alamat_penanggung!=null)
    $pasien['alamat_penanggung']=$alamat_penanggung;
    if($no_penanggung!=null)
    $pasien['no_penanggung']=$no_penanggung;
    if($hubungan_penanggung!=null)
    $pasien['hubungan_penanggung']=$hubungan_penanggung;
    if($tgl_daftar!=null)
    $pasien['tgl_daftar']=$tgl_daftar;

    $id=$this->rs_model->insert_pasien($pasien);*/
    $id = 1;
    $pasien['id'] = $id;
    if ($id != null) {
      $this->set_response([
        'status' => true,
        'message' => 'Pasien Baru Telah Dibuat',
        'pasien' => $pasien,
      ], REST_Controller::HTTP_OK); // NO_CONTENT (200) being the HTTP response code

    } else {
      $this->response([
        'status' => false,
        'message' => 'Parameter Salah'], REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code

    }

  }

  public function pasien_get()
  {

    $id = $this->get('id');

    // If the id parameter doesn't exist return all the users

    if ($id === null) {
      // Users from a data store e.g. database
      $pasien = $this->pasien_model->get_pasien();

      // Check if the users data store contains users (in case the database result returns NULL)
      if ($pasien) {
        // Set the response and exit
        $this->response([
          'status' => true,
          'message' => 'Pasien ditemukan',
          'pasien' => $pasien,
        ], REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
      } else {
        // Set the response and exit
        $this->response([
          'status' => false,
          'message' => 'Pasien Tidak ditemukan',
        ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
      }
    }

    // Find and return a single record for a particular user.

    $id = (int) $id;

    // Validate the id.
    if ($id <= 0) {
      // Invalid id, set the response and exit.
      $this->response(null, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
    }

    //QUERY USER
    $pasien = $this->pasien_model->get_pasien(array('id' => $id));

    if (!empty($pasien)) {
      $this->set_response([
        'status' => true,
        'message' => 'Pasien Ditemukan',
        'pasien' => $pasien,
      ], REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
    } else {
      $this->set_response([
        'status' => false,
        'message' => 'Pasien Tidak ditemukan',
      ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
    }
  }

  public function pasien_post()
  {

    $id = $this->post('id');
    $nama = $this->post('nama');
    $alamat = $this->post('alamat');
    $noKTP = $this->post('noKTP');
    if ($id === null) {
      // Invalid id, set the response and exit.
      $this->response([
        'status' => false,
        'message' => 'ID Tidak Ditemukan',
      ], REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
    } else {
      $pasien = array();
      $cond['id'] = $this->post('id');
      // $this->some_model->update_user( ... );
      if ($nama != null) {
        $pasien['nama'] = $nama;
      }

      if ($alamat != null) {
        $pasien['alamat'] = $alamat;
      }

      if ($noKTP != null) {
        $pasien['noKTP'] = $noKTP;
      }

      $status = $this->pasien_model->update_pasien($pasien, $cond);
      $pasien['id'] = $id;
      if ($status) {
        $this->set_response([
          'status' => true,
          'message' => 'Update Pasien Berhasil',
          'pasien' => $pasien,
        ], REST_Controller::HTTP_CREATED);
      }
      // CREATED (201) being the HTTP response code
      else {
        $this->set_response([
          'status' => false,
          'message' => 'Update Pasien Gagal',
          'pasien' => $pasien,
        ], REST_Controller::HTTP_NOT_FOUND);
      }
      // NOT_FOUND (404) being the HTTP response code
    }
  }

  public function pasien_delete()
  {

    $id = (int) $this->delete('id');

    // Validate the id.
    if ($id <= 0) {

      // Set the response and exit
      $this->response([
        'id' => $id,
        'status' => false,
        'message' => 'HAPUS GAGAL',
      ], REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
    }

    $cond['id'] = $id;
    // $this->some_model->delete_something($id);
    $status = $this->pasien_model->delete_pasien($cond);
    if ($status) {
      $message = [
        'id' => $id,
        'status' => true,
        'message' => 'pasien Berhasil Dihapus',
      ];
    } else {
      $message = [
        'id' => $id,
        'status' => false,
        'message' => 'pasien Gagal Dihapus',
      ];
    }

    $this->set_response($message, REST_Controller::HTTP_OK); // NO_CONTENT (200) being the HTTP response code
  }

  public function kunjungan_get()
  {
    $tgl = $this->get('tanggal_kunjungan');
    $id = $this->get('id');

    // If the id parameter doesn't exist return all the users

    if ($tgl === null && $id == null) {
      // Kunjungan from a data store e.g. database
      $kunjungan = $this->mapi->get_kunjungan();

      //GET PASIEN BARU
      foreach ($kunjungan as $k) {
        if ($k->status_kunjungan == 1 && $k->pasien_id != 0) {
          $c['id'] = $k->pasien_id;
          $k->data_pasien = $this->mapi->get_pasien($c)[0];
          $time = strtotime($k->tgl_daftar);
          $k->kode_pendaftaran = 'M' . date('ymd', $time) . sprintf("%03d", $k->id);
        } else {
          $time = strtotime($k->tgl_daftar);
          $k->kode_pendaftaran = 'R' . date('ymd', $time) . sprintf("%03d", $k->id);
        }
      }

      // Check if the users data store contains users (in case the database result returns NULL)
      if ($kunjungan) {
        // Set the response and exit
        $this->response([
          'status' => true,
          'message' => 'List kunjungan ditemukan',
          'kunjungan' => $kunjungan,
        ], REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
      } else {
        // Set the response and exit
        $this->response([
          'status' => false,
          'message' => 'Kunjungan Tidak ditemukan',
        ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
      }
    }

    if ($id != null && $tgl === null) {
      if (strlen($id) != 10) {
        $this->response([
          'status' => true,
          'message' => 'Format ID salah 1',
        ], REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
      }
      // $y = substr($id, 1, 2);
      // $m = substr($id, 3, 2);
      // $d = substr($id, 5, 2);
      // if (!(is_numeric($y) && is_numeric($m) && is_numeric($d))) {
      //   $this->response([
      //     'status' => true,
      //     'message' => 'format ID salah',
      //   ], REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
      // }

      // // Kunjungan from a data store e.g. database
      // $id_kunjungan = (int) substr($id, -3);
      $a['kode'] = $id;
      // $a['date(tgl_daftar)'] = '20' . $y . '-' . $m . '-' . $d;

      $kunjungan = $this->mapi->get_kunjungan($a);

      //GET PASIEN BARU
      // foreach ($kunjungan as $k) {
      //   if ($k->status_kunjungan == 1 && $k->pasien_id != 0) {
      //     $c['id'] = $k->pasien_id;
      //     $k->data_pasien = $this->mapi->get_pasien($c)[0];
      //     $time = strtotime($k->tgl_daftar);
      //     $k->kode_pendaftaran = 'R' . date('ymd', $time) . sprintf("%03d", substr($k->id, -3));
      //   } else {
      //     $time = strtotime($k->tgl_daftar);
      //     $k->kode_pendaftaran = 'M' . date('ymd', $time) . sprintf("%03d", substr($k->id, -3));
      //   }
      // }

      // Check if the users data store contains users (in case the database result returns NULL)
      if ($kunjungan) {
        // Set the response and exit
        $this->response([
          'status' => true,
          'message' => 'List kunjungan ditemukan',
          'kunjungan' => $kunjungan,
        ], REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
      } else {
        // Set the response and exit
        $this->response([
          'status' => false,
          'message' => 'Kunjungan Tidak ditemukan',
        ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
      }
    }
    if ($tgl != null && $id == null) {

      // Kunjungan from a data store e.g. database
      $id_kunjungan = (int) substr($id, -3);
      $a['date(tgl_kunjungan)'] = $tgl;

      $kunjungan = $this->mapi->get_kunjungan($a);

      //GET PASIEN BARU
      foreach ($kunjungan as $k) {
        if ($k->status_kunjungan == 1 && $k->pasien_id != 0) {
          $c['id'] = $k->pasien_id;
          $k->data_pasien = $this->mapi->get_pasien($c)[0];
          $time = strtotime($k->tgl_daftar);
          $k->kode_pendaftaran = 'R' . date('ymd', $time) . sprintf("%03d", $k->id);
        } else {
          $time = strtotime($k->tgl_daftar);
          $k->kode_pendaftaran = 'M' . date('ymd', $time) . sprintf("%03d", $k->id);
        }
      }

      // Check if the users data store contains users (in case the database result returns NULL)
      if ($kunjungan) {
        // Set the response and exit
        $this->response([
          'status' => true,
          'message' => 'List kunjungan ditemukan',
          'kunjungan' => $kunjungan,
        ], REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
      } else {
        // Set the response and exit
        $this->response([
          'status' => false,
          'message' => 'Kunjungan Tidak ditemukan',
        ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
      }

    }
    if ($tgl != null && $id != null) {

      if (strlen($id) != 10) {
        $this->response([
          'status' => true,
          'message' => 'format ID salah 2',
        ], REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
      }
      $y = substr($id, 1, 2);
      $m = substr($id, 3, 2);
      $d = substr($id, 5, 2);
      if (!(is_numeric($y) && is_numeric($m) && is_numeric($d))) {
        $this->response([
          'status' => true,
					'message' => 'format ID salah 3',
        ], REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
      }

      // Kunjungan from a data store e.g. database
      $id_kunjungan = (int) substr($id, -3);
      $a['date(tgl_kunjungan)'] = $tgl;
      $a['right(id,3)'] = $id_kunjungan;
      $kunjungan = $this->mapi->get_kunjungan($a);

      //GET PASIEN BARU
      foreach ($kunjungan as $k) {
        if ($k->status_kunjungan == 1 && $k->pasien_id != 0) {
          $c['id'] = $k->pasien_id;
          $k->data_pasien = $this->mapi->get_pasien($c)[0];
          $time = strtotime($k->tgl_daftar);
          $k->kode_pendaftaran = 'R' . date('ymd', $time) . sprintf("%03d", $k->id);
        } else {
          $time = strtotime($k->tgl_daftar);
          $k->kode_pendaftaran = 'M' . date('ymd', $time) . sprintf("%03d", $k->id);
        }
      }

      // Check if the users data store contains users (in case the database result returns NULL)
      if ($kunjungan) {
        // Set the response and exit
        $this->response([
          'status' => true,
          'message' => 'List kunjungan ditemukan',
          'kunjungan' => $kunjungan,
        ], REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
      } else {
        // Set the response and exit
        $this->response([
          'status' => false,
          'message' => 'Kunjungan Tidak ditemukan',
        ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
      }

    }
    // Find and return a single record for a particular user.

  }

  public function kunjunganid_get()
  {

    // Kunjungan from a data store e.g. database
    $kunjungan = $this->mapi->get_kunjungan();

    //GET PASIEN BARU
    foreach ($kunjungan as $k) {
      if ($k->status_kunjungan == 1 && $k->pasien_id != 0) {
        $c['id'] = $k->pasien_id;
        $k->data_pasien = $this->mapi->get_pasien($c)[0];
        $time = strtotime($k->tgl_daftar);
        $k->kode_pendaftaran = 'M' . date('ymd', $time) . sprintf("%03d", $k->id);
      } else {
        $time = strtotime($k->tgl_daftar);
        $k->kode_pendaftaran = 'R' . date('ymd', $time) . sprintf("%03d", $k->id);
      }
    }
    $result = array();
    foreach ($kunjungan as $k) {
      $result[] = $k->kode_pendaftaran;
    }

    // Check if the users data store contains users (in case the database result returns NULL)
    if ($result) {
      // Set the response and exit
      $this->response([
        'status' => true,
        'message' => 'List kunjungan ditemukan',
        'list_kode' => $result,
      ], REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
    } else {
      // Set the response and exit
      $this->response([
        'status' => false,
        'message' => 'Kunjungan Tidak ditemukan',
      ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
    }

  }

  public function kunjungantotal_get()
  {

    // Kunjungan from a data store e.g. database
    $kunjungan = $this->mapi->get_kunjungan();

    $result = count($kunjungan);

    // Check if the users data store contains users (in case the database result returns NULL)
    if ($result) {
      // Set the response and exit
      $this->response([
        'status' => true,
        'message' => 'kunjungan ditemukan',
        'total' => $result,
      ], REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
    } else {
      // Set the response and exit
      $this->response([
        'status' => false,
        'message' => 'Kunjungan Tidak ditemukan',
      ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
    }

  }

  public function totalpasien_get()
  {

    // Kunjungan from a data store e.g. database
    $result = $this->mapi->get_total_pasien();

    
    // Check if the users data store contains users (in case the database result returns NULL)
    if ($result) {
      // Set the response and exit
      $this->response([
        'status' => true,
        'message' => 'Kunjungan ditemukan',
        'total' => $result,
      ], REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
    } else {
      // Set the response and exit
      $this->response([
        'status' => false,
        'message' => 'Kunjungan Tidak ditemukan',
      ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
    }

  }

  public function sync_data_pasien_post()
  {

    $params = $this->input->post(NULL, TRUE);

    if($params['aksi'] == 1){
      //INSERT
      $dt_insert['no_rm'] = $params['no_rm'];
      $dt_insert['title'] = $params['title'];
      $dt_insert['nama'] = $params['nama'];
      $dt_insert['tempat_lahir'] = $params['tempat_lahir'];
      $dt_insert['tanggal_lahir'] = $params['tanggal_lahir'];
      $dt_insert['jenis_kelamin'] = $params['jenis_kelamin'];
      $dt_insert['alamat'] = $params['alamat'];
      $dt_insert['id_provinsi'] = $params['id_provinsi'];
      $dt_insert['id_kota'] = $params['id_kota'];
      $dt_insert['id_kecamatan'] = $params['id_kecamatan'];
      $dt_insert['id_kelurahan'] = $params['id_kelurahan'];
      $dt_insert['no_telp'] = $params['no_telp'];
      $dt_insert['alamat_ktp'] = $params['alamat_ktp'];
      $dt_insert['nama_ortu'] = $params['nama_ortu'];
      $dt_insert['pekerjaan'] = $params['pekerjaan'];
      $dt_insert['pendidikan'] = $params['pendidikan'];
      $dt_insert['nama_penanggung'] = $params['nama_penanggung'];
      $dt_insert['alamat_penanggung'] = $params['alamat_penanggung'];
      $dt_insert['no_penanggung'] = $params['no_penanggung'];
      $dt_insert['hubungan_penanggung'] = $params['hubungan_penanggung'];
      $dt_insert['tgl_daftar'] = $params['tgl_daftar'];

      $result = $this->mapi->insert_pasien($dt_insert);
    }
    else if($params['aksi'] == 2){
      //Check pasien
      $c['no_rm'] = $params['no_rm'];
      $p = $this->mapi->get_pasien($c);
      if($p){
        //UPDATE
        $c_update['no_rm'] = $params['no_rm'];
        $dt_update['no_rm'] = $params['no_rm'];
        $dt_update['title'] = $params['title'];
        $dt_update['nama'] = $params['nama'];
        $dt_update['tempat_lahir'] = $params['tempat_lahir'];
        $dt_update['tanggal_lahir'] = $params['tanggal_lahir'];
        $dt_update['jenis_kelamin'] = $params['jenis_kelamin'];
        $dt_update['alamat'] = $params['alamat'];
        $dt_update['id_provinsi'] = $params['id_provinsi'];
        $dt_update['id_kota'] = $params['id_kota'];
        $dt_update['id_kecamatan'] = $params['id_kecamatan'];
        $dt_update['id_kelurahan'] = $params['id_kelurahan'];
        $dt_update['no_telp'] = $params['no_telp'];
        $dt_update['alamat_ktp'] = $params['alamat_ktp'];
        $dt_update['nama_ortu'] = $params['nama_ortu'];
        $dt_update['pekerjaan'] = $params['pekerjaan'];
        $dt_update['pendidikan'] = $params['pendidikan'];
        $dt_update['nama_penanggung'] = $params['nama_penanggung'];
        $dt_update['alamat_penanggung'] = $params['alamat_penanggung'];
        $dt_update['no_penanggung'] = $params['no_penanggung'];
        $dt_update['hubungan_penanggung'] = $params['hubungan_penanggung'];
        $dt_update['tgl_daftar'] = $params['tgl_daftar'];
        
        $result = $this->mapi->update_pasien($c_update, $dt_update);
      }
      else{
        //INSERT
        $dt_insert['no_rm'] = $params['no_rm'];
        $dt_insert['title'] = $params['title'];
        $dt_insert['nama'] = $params['nama'];
        $dt_insert['tempat_lahir'] = $params['tempat_lahir'];
        $dt_insert['tanggal_lahir'] = $params['tanggal_lahir'];
        $dt_insert['jenis_kelamin'] = $params['jenis_kelamin'];
        $dt_insert['alamat'] = $params['alamat'];
        $dt_insert['id_provinsi'] = $params['id_provinsi'];
        $dt_insert['id_kota'] = $params['id_kota'];
        $dt_insert['id_kecamatan'] = $params['id_kecamatan'];
        $dt_insert['id_kelurahan'] = $params['id_kelurahan'];
        $dt_insert['no_telp'] = $params['no_telp'];
        $dt_insert['alamat_ktp'] = $params['alamat_ktp'];
        $dt_insert['nama_ortu'] = $params['nama_ortu'];
        $dt_insert['pekerjaan'] = $params['pekerjaan'];
        $dt_insert['pendidikan'] = $params['pendidikan'];
        $dt_insert['nama_penanggung'] = $params['nama_penanggung'];
        $dt_insert['alamat_penanggung'] = $params['alamat_penanggung'];
        $dt_insert['no_penanggung'] = $params['no_penanggung'];
        $dt_insert['hubungan_penanggung'] = $params['hubungan_penanggung'];
        $dt_insert['tgl_daftar'] = $params['tgl_daftar'];

        $result = $this->mapi->insert_pasien($dt_insert);
      }
    }
    else if($params['aksi'] == 3){
      //DELETE
      $c_delete['no_rm'] = $params['no_rm'];
      $r = $this->mapi->delete_pasien($c_delete);
    }
    // Check if the users data store contains users (in case the database result returns NULL)
    if ($result) {
      // Set the response and exit
      $this->response([
        'status' => true,
        'message' => 'Berhasil Sinkron',
      ], REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
    } else {
      // Set the response and exit
      $this->response([
        'status' => false,
        'message' => 'Gagal Sinkron',
      ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
    }
  }

  public function data_kunjungan_post()
  {
    // print_r($this->input->post('start'));
    $c['date(tgl_kunjungan)'] = date('Y-m-d');
    $list = $this->mkunjungan->get_datatables($c);
    $data = array();
    $no = $_POST['start'];
    foreach ($list as $k) {
      $cara_bayar_s = '';
      switch($k->cara_bayar){
        case 1:
          $cara_bayar_s = 'BPJS';
          break;
        case 2:
          $cara_bayar_s = 'SPM';
          break;
        case 3:
          $cara_bayar_s = 'BPJS TK';
          break;
        case 4:
          $cara_bayar_s = 'RBH';
          break;
        case 5:
          $cara_bayar_s = 'Jasa Raharja';
          break;
        case 6:
          $cara_bayar_s = 'Jampersal';
          break;
        case 99:
          $cara_bayar_s = 'Umum';
          break;
      }
      $no++;
      // print_r($k);
      $row = array();
      $row['no'] = $no;
      $row['kode'] = $k->kode;
      $row['no_rm'] = $k->no_rm;
      $row['nama'] = $k->nama;
      $row['tgl_kunjungan'] = $k->tgl_kunjungan;
      $row['nama_layanan'] = $k->nama_layanan;
      $row['cara_bayar'] = $cara_bayar_s;
      $row['nomor_anggota'] = $k->nomor_anggota;
      $row['no_rujukan'] = $k->no_rujukan;
      $row['no_surat'] = $k->no_surat;
      $row['kd_layanan'] = $k->layanan;
      $row['kd_cara_bayar'] = $k->cara_bayar;

      $data[] = $row;
    }

    $output = array(
      "draw" => $_POST['draw'],
      "recordsTotal" => $this->mkunjungan->count_all($c),
      "recordsFiltered" => $this->mkunjungan->count_filtered($c),
      "data" => $data,
    );
    //output to json format
    echo json_encode($output);
    
  }
}
