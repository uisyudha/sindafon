var Toast = Swal.mixin({
	toast: true,
	position: "top",
	showConfirmButton: false,
	timer: 3000
});

//iCheck for checkbox and radio inputs
$('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
  checkboxClass: 'icheckbox_minimal-blue',
  radioClass   : 'iradio_minimal-blue'
})

function pad(str, max) {
	str = str.toString();
	return str.length < max ? pad("0" + str, max) : str;
}

function getsatuanbarang(satuan, id_barang) {
  $.ajax({
    type: "POST",
    url: base_url + "/ajax/get_satuan",
    data: "id_barang=" + id_barang,
    success: function (data) {
      
      //$("#kota").html(data);
      satuan.html(data);
      satuan.trigger("change");
      return data;
    }
  });
}

function gethargaterakhir(satuan, id_barang) {
  $.ajax({
    type: "POST",
    url: base_url + "/ajax/get_harga_terakhir",
    data: "id_barang=" + id_barang,
    success: function (data) {
      //$("#kota").html(data);
      satuan[0].value = data;
      return data;
    }
  });
}

function export_table_harian() {
	var title = $("#table-harian-kasir").attr("title");
	$("#table-harian-kasir").tableExport({ filename: title });
}

function add_item_form_permintaan() {
  var newrow = $("<tr>")
    .append(
      $('<td class="id_obat">').append(
        '<select class="input-sm form-control barang input_nama_barang_gudang" name="id_brg[]" style="width:100%" required><option></option></select>'
      )
    )
    .append($('<td class="satuan-obat">').append(""))
    .append(
      $("<td>").append(
        '<input type="text"  class="input-sm form-control form-control-sm" name="qty[]" >'
      )
    )
    .append(
      $('<td class="item-harga">').append(
        '<textarea class="input-sm form-control form-control-sm" name="keterangan[]"> </textarea>'
      )
    )
    .append(
      $("<td>").append(
        '<a href="javascript:void(0)" class="btn btn-danger btn-sm " onclick="$(this).closest(\'tr\').remove();"><i class="fa fa-times-circle" aria-hidden="true"></i></a>'
      )
    );

  newrow.find(".input_nama_barang_gudang").select2({
    'placeholder': 'Nama Obat',
    ajax: {
      url: base_url + "/ajax/get_barang_gudang",
      dataType: 'json',
      delay: 250,
      processResults: function (data) {
        // Tranforms the top-level key of the response object from 'items' to 'results'
        var option = [];
        data.forEach(element => {
          option.push({ id: element.value, text: element.label })
          // $('#icd').val(element.value);
          // var satuan = $(this)
          //   .closest("tr")
          //   .find("td.satuan-obat");
          // var harga_terakhir = $(this).closest("tr").find("input[name^=harga_terakhir]");
          // console.log(satuan);
          // console.log(harga_terakhir)
          // getsatuanbarang(satuan, element.value);
          // gethargaterakhir(harga_terakhir, element.value);
        });
        return {
          results: option
        };
      }
    },
    minimumInputLength: 2,
  });

  newrow.find(".input_nama_barang_gudang").on('select2:select', function (e) {
    var satuan = $(this)
      .closest("tr")
      .find("td.satuan-obat");
  
    getsatuanbarang(satuan, e.params.data.id);
  });

  $("#table-item")
    .find("tbody")
    .append(newrow);
}

function add_item_form_pengeluaran() {
  var newrow = $("<tr>")
    .append(
      $('<td class="id_obat">').append(
        '<select class="input-sm form-control barang input_nama_barang_keluar" id="id_brg" name="id_brg[]" style="width:100%" required><option></option></select>'
      )
    )
    .append($('<td class="satuan-obat">').append(""))
    .append(
      $("<td>").append(
        '<input type="text"  class="input-sm form-control form-control-sm" name="qty[]" >'
      )
    )
    .append(
      $('<td class="item-harga">').append(
        '<textarea class="input-sm form-control form-control-sm" name="keterangan[]"> </textarea>'
      )
    )
    .append(
      $("<td>").append(
        '<a href="javascript:void(0)" class="btn btn-danger btn-sm " onclick="$(this).closest(\'tr\').remove();"><i class="fa fa-times-circle" aria-hidden="true"></i></a>'
      )
    );

  newrow.find(".input_nama_barang_keluar").select2({
    'placeholder': 'Nama Obat',
    ajax: {
      url: base_url + "/ajax/get_barang_poli",
      dataType: 'json',
      delay: 250,
      processResults: function (data) {
        // Tranforms the top-level key of the response object from 'items' to 'results'
        var option = [];
        data.forEach(element => {
          option.push({ id: element.value, text: element.label })
          // $('#icd').val(element.value);
          // var satuan = $(this)
          //   .closest("tr")
          //   .find("td.satuan-obat");
          // var harga_terakhir = $(this).closest("tr").find("input[name^=harga_terakhir]");
          // console.log(satuan);
          // console.log(harga_terakhir)
          // getsatuanbarang(satuan, element.value);
          // gethargaterakhir(harga_terakhir, element.value);
        });
        return {
          results: option
        };
      }
    },
    minimumInputLength: 2,
  });

  newrow.find(".input_nama_barang_keluar").on('select2:select', function (e) {
    var satuan = $(this)
      .closest("tr")
      .find("td.satuan-obat");
  
    getsatuanbarang(satuan, e.params.data.id);
  });

  $("#table-item")
    .find("tbody")
    .append(newrow);
}

$('#btn-menu-sidebar').on('click', function(){
	setTimeout(function () {
		$.fn.dataTable.tables({
		visible: true,
		api: true
		}).columns.adjust();
		}, 350);
})

$('input[name="tanggal_permintaan"]').daterangepicker({
  singleDatePicker: true,
  autoUpdateInput: false,
  showDropdowns: true,
  minYear: 2018,
  maxYear: parseInt(moment().add('1', 'year').format('YYYY'), 10),
  locale: {
    format: 'DD-MM-YYYY'
  },
  drops: 'down'
});

$('input[name="tgl_keluar"]').on('apply.daterangepicker', function (ev, picker) {
  $(this).val(picker.startDate.format('DD-MM-YYYY'));
});

$('input[name="tgl_keluar"]').daterangepicker({
  singleDatePicker: true,
  autoUpdateInput: false,
  showDropdowns: true,
  minYear: 2018,
  maxYear: parseInt(moment().add('1', 'year').format('YYYY'), 10),
  locale: {
    format: 'DD-MM-YYYY'
  },
  drops: 'down'
});

$('input[name="tanggal_permintaan"]').on('apply.daterangepicker', function (ev, picker) {
  $(this).val(picker.startDate.format('DD-MM-YYYY'));
});

$(".input_nama_barang_gudang").select2({
  'placeholder': 'Nama Obat',
  ajax: {
    url: base_url + "/ajax/get_barang_gudang",
    dataType: 'json',
    delay: 250,
    processResults: function (data) {
      // Tranforms the top-level key of the response object from 'items' to 'results'
      var option = [];
      data.forEach(element => {
        option.push({ id: element.value, text: element.label })
        // $('#icd').val(element.value);
        // var satuan = $(this)
        //   .closest("tr")
        //   .find("td.satuan-obat");
        // var harga_terakhir = $(this).closest("tr").find("input[name^=harga_terakhir]");
        // console.log(satuan);
        // console.log(harga_terakhir)
        // getsatuanbarang(satuan, element.value);
        // gethargaterakhir(harga_terakhir, element.value);
      });
      return {
        results: option
      };
    }
  },
  minimumInputLength: 2,
});

$(".input_nama_barang_gudang").on('select2:select', function (e) {
  var satuan = $(this)
    .closest("tr")
    .find("td.satuan-obat");

  getsatuanbarang(satuan, e.params.data.id);
});

$(".input_nama_barang_keluar").select2({
  'placeholder': 'Nama Obat',
  ajax: {
    url: base_url + "/ajax/get_barang_poli",
    dataType: 'json',
    delay: 250,
    processResults: function (data) {
      // Tranforms the top-level key of the response object from 'items' to 'results'
      var option = [];
      data.forEach(element => {
        option.push({ id: element.value, text: element.label })
        // $('#icd').val(element.value);
        // var satuan = $(this)
        //   .closest("tr")
        //   .find("td.satuan-obat");
        // var harga_terakhir = $(this).closest("tr").find("input[name^=harga_terakhir]");
        // console.log(satuan);
        // console.log(harga_terakhir)
        // getsatuanbarang(satuan, element.value);
        // gethargaterakhir(harga_terakhir, element.value);
      });
      return {
        results: option
      };
    }
  },
  minimumInputLength: 2,
});

$(".input_nama_barang_keluar").on('select2:select', function (e) {
  var satuan = $(this)
    .closest("tr")
    .find("td.satuan-obat");

  getsatuanbarang(satuan, e.params.data.id);
});

$('#form-poli-pengeluaran-barang').on('submit', function(e){
	e.preventDefault();
	var form = this;
	Swal.fire({
		title: 'Apakah Data Sudah Benar?',
		text: "",
		type: 'warning',
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		confirmButtonText: 'OK'
	}).then((result) =>{
		if(result.value){
			form.submit();
		}
	})
})
function updateTime(){
  var hari= ["Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jum&#39;at", "Sabtu"];
  var bulan = ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"];
  var tanggal = new Date().getDate(); 
  var hari = hari[new Date().getDay()];
  var bulan = bulan[new Date().getMonth()];
  var tahun = new Date().getFullYear();
  var jam = new Date().getHours();
  var min = new Date().getMinutes();
  var sec = new Date().getSeconds();
  if (jam < 10)
    jam = "0"+jam;
  if (min < 10)
    min = "0"+min;
  if (sec < 10)
    sec = "0"+sec;

  var date = 'Hari: ' + hari + ', ' + tanggal + ' ' + bulan + ' ' + tahun + ' ' + jam + ':' + min + ":" + sec +' WIB';
    document.getElementById("tanggal").innerHTML = date;
}

$(document).ready(function(){
  setInterval(() => {
    updateTime()
  }, 1000);

  /* $("[data-widget='collapse']").click(function() {
    //Find the box parent  
    var test =       $("#daftar-layanan").find("i.fa-minus");
    console.log(test);
    //.removeClass("fa-minus").addClass("fa-plus");
}); */
})