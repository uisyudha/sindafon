var url='https://daftar.slwebid.com/';



function getState(val) {

  $.ajax({

  type: "POST",

  url: url+"pendaftaran/get_city",

  data:'country_id='+val,

  success: function(data){



    //console.log(data);

    $("#kota").html(data);

    

  }

  });

}



function getDistrict(val) {

  $.ajax({

  type: "POST",

  url: url+"pendaftaran/get_district",

  data:'city_id='+val,

  success: function(data){



    //console.log(data);

    $("#kec").html(data);



  }

  });

}

function getVillage(val) {

  $.ajax({

  type: "POST",

  url: url+"pendaftaran/get_village",

  data:'district_id='+val,

  success: function(data){



    //console.log(data);

    $("#kel").html(data);

    

  }

  });

}

$( function() {



    /*CUSTOM COMBOBOX*/

    $.widget( "custom.combobox", {

      _create: function() {

        this.wrapper = $( "<span>" )

          .addClass( "custom-combobox" )

          .insertAfter( this.element );

 

        this.element.hide();

        this._createAutocomplete();

        this._createShowAllButton();

      },

 

      _createAutocomplete: function() {

        var selected = this.element.children( ":selected" ),

          value = selected.val() ? selected.text() : "";

 

        this.input = $( "<input>" )

          .appendTo( this.wrapper )

          .val( value )

          .attr( "title", "" )

          .addClass( "custom-combobox-input ui-widget ui-widget-content ui-state-default ui-corner-left" )

          .autocomplete({

            delay: 0,

            minLength: 0,

            source: $.proxy( this, "_source" )

          })

          .tooltip({

            classes: {

              "ui-tooltip": "ui-state-highlight"

            }

          });

 

        this._on( this.input, {

          autocompleteselect: function( event, ui ) {

            ui.item.option.selected = true;

            this._trigger( "select", event, {

              item: ui.item.option

            });

          },

 

          autocompletechange: "_removeIfInvalid"

            



        },

        





        );

      },

 

      _createShowAllButton: function() {

        var input = this.input,

          wasOpen = false;

 

        $( "<a>" )

          .attr( "tabIndex", -1 )

          .attr( "title", "Show All Items" )

          .tooltip()

          .appendTo( this.wrapper )

          .button({

            icons: {

              primary: "ui-icon-triangle-1-s"

            },

            text: false

          })

          .removeClass( "ui-corner-all" )

          .addClass( "custom-combobox-toggle ui-corner-right" )

          .on( "mousedown", function() {

            wasOpen = input.autocomplete( "widget" ).is( ":visible" );

          })

          .on( "click", function() {

            input.trigger( "focus" );

 

            // Close if already visible

            if ( wasOpen ) {

              return;

            }

 

            // Pass empty string as value to search for, displaying all results

            input.autocomplete( "search", "" );

          });

      },

 

      _source: function( request, response ) {

        var matcher = new RegExp( $.ui.autocomplete.escapeRegex(request.term), "i" );

        response( this.element.children( "option" ).map(function() {

          var text = $( this ).text();

          if ( this.value && ( !request.term || matcher.test(text) ) )

            return {

              label: text,

              value: text,

              option: this

            };

        }) );

      },

 

      _removeIfInvalid: function( event, ui ) {

 

        // Selected an item, nothing to do

        if ( ui.item ) {

          return;

        }

 

        // Search for a match (case-insensitive)

        var value = this.input.val(),

          valueLowerCase = value.toLowerCase(),

          valid = false;

        this.element.children( "option" ).each(function() {

          if ( $( this ).text().toLowerCase() === valueLowerCase ) {

            this.selected = valid = true;

            return false;

          }

        });

 

        // Found a match, nothing to do

        if ( valid ) {



          return;

        }

 

        // Remove invalid value

        this.input

          .val( "" )

          .attr( "title", value + " didn't match any item" )

          .tooltip( "open" );

        this.element.val( "" );

        this._delay(function() {

          this.input.tooltip( "close" ).attr( "title", "" );

        }, 2500 );

        this.input.autocomplete( "instance" ).term = "";

      },

 

      _destroy: function() {

        this.wrapper.remove();

        this.element.show();

      } 

    });

 

    $( "#provinsi" ).combobox({ 

        select: function (event, ui) { 

             getState(this.value);

            console.log('get list kota');

        } 

    }); 

     $( "#kota" ).combobox({ 

        select: function (event, ui) { 

             getDistrict(this.value);

            console.log('get list kecamatan');

        } 

    });

     $( "#kec" ).combobox({ 

        select: function (event, ui) { 

             getVillage(this.value);

            console.log('get list Keluarahan');

        } 

    });

     $( "#kel" ).combobox();

     

   

    /*EO CUSTOM COMBOBOX*/



    //DECLARE RADIO INPUT

    $( ".radio" ).checkboxradio();

    $( "#tgl_lahir" ).datepicker({ format: 'dd-mm-yyyy',

      changeMonth: true,

      changeYear: true,

      yearRange:"-100:+0"

    });  

    // $( "#tgl_daftar" ).datepicker({ format: 'dd-mm-yy',

    //   minDate: 0,

    //   maxDate: "+3d",

    //   beforeShowDay: function(date) {

    //     var day = date.getDay();

    //     return [(day != 0), ''];

    //   }



    // }); 



    /*konfirmasi data*/

    



 $( "#btn-nonreg-submit" ).click(function() {

          var title=$('select[name=title]').val();

          var nama=$('input[name=nama]').val();

          var nama_ayah=$('input[name=nama_ayah]').val();

          var kelamin=$('input[name=kelamin]').val();

          var status=$('select[name=status_kawin]').val();

          var pendidikan=$('select[name=pendidikan]').val();

          var agama=$('select[name=agama]').val();

          var tempat_lahir=$('input[name=tempat_lahir]').val();

          var tgl_lahir=$('input[name=tgl_lahir]').val();

          var alamat=$('input[name=alamat]').val();

          var provinsi=$('select[name=provinsi]').val();

          var kota=$('select[name=kota] ').val();

          var kec=$('select[name=kec] ').val();

          var kel=$('select[name=kel] ').val();

          var no_ktp=$('input[name=no_ktp]').val();

          var no_telp=$('input[name=no_telp]').val();

          var pekerjaan=$('input[name=pekerjaan]').val();

          var penanggung_jawab=$('input[name=penanggung_jawab]').val();

          var telp_penanggung=$('input[name=telp_penanggung]').val();

          var poli=$('select[name=poli]').val();

          var tgl_kunjungan=$('input[name=tgl_daftar]').val();

          var jenispasien=$('select[name=jenispasien]').val();

          var no_anggota=$('input[name=no_anggota]').val();

          var email=$('input[name=email]').val();





        console.log(nama);

        console.log(nama_ayah);

        console.log(kelamin);

        console.log(status);

        console.log(pendidikan);

        console.log(agama);

        console.log(tempat_lahir);

        console.log(tgl_lahir);

        console.log(alamat);

        console.log(provinsi);

        console.log(kota);

        console.log(kec);

        console.log(kel);

        console.log(no_ktp);

        console.log(no_telp);

        console.log(pekerjaan);

        console.log(penanggung_jawab);

        console.log(telp_penanggung);

        console.log(poli);

        console.log(tgl_kunjungan);

        console.log(jenispasien);

        console.log(no_anggota);

        console.log(email);



       if(email==''){

        if(!confirm('Email kosong, Anda tidak Akan Mendapatkan email konfirmasi ke Email?')){

          return ;

        }

       }



            $(".se-pre-con").fadeIn("slow");

       



              $.ajax({

                type: "POST",

                url: url+"Pendaftaran/proses_nonreg",

                data:'nama='+nama+

                    '&title='+title+

                    '&nama_ayah='+nama_ayah+

                    '&kelamin='+kelamin+

                    '&status='+status+

                    '&pendidikan='+pendidikan+

                    '&agama='+agama+

                    '&tempat_lahir='+tempat_lahir+

                    '&tgl_lahir='+tgl_lahir+

                    '&alamat='+alamat+

                    '&provinsi='+provinsi+

                    '&kota='+kota+

                    '&kec='+kec+

                    '&kel='+kel+

                    '&no_ktp='+no_ktp+

                    '&no_telp='+no_telp+

                    '&pekerjaan='+pekerjaan+

                    '&penanggung_jawab='+penanggung_jawab+

                    '&telp_penanggung='+telp_penanggung+

                    '&poli='+poli+

                    '&tgl_kunjungan='+tgl_kunjungan+

                    '&jenispasien='+jenispasien+

                    '&no_anggota='+no_anggota+

                    '&email='+email,

                success: function(data){

                  

                  setTimeout(function() {

                       var result=JSON.parse(data);

                        $(".se-pre-con").fadeOut("slow");

                         

                        

                        if(result.status==false){

                            alert('Maaf Sedang Ada Masalah Pada Sistem, Pendaftaran Anda Gagal!');

                        }else{

                          window.location.replace(url+"pendaftaran/berhasil/"+result.kode);

                        }

                         

                   }, 1000);



                 

                 



                  



                }

                });



     });









   





    /*konfirmasi data*/



    







});











 

function scroll_to_class(element_class, removed_height) {

	var scroll_to = $(element_class).offset().top - removed_height;

	if($(window).scrollTop() != scroll_to) {

		$('.form-wizard').stop().animate({scrollTop: scroll_to}, 0);

	}

}



function bar_progress(progress_line_object, direction) {

	var number_of_steps = progress_line_object.data('number-of-steps');

	var now_value = progress_line_object.data('now-value');

	var new_value = 0;

	if(direction == 'right') {

		new_value = now_value + ( 100 / number_of_steps );

	}

	else if(direction == 'left') {

		new_value = now_value - ( 100 / number_of_steps );

	}

	progress_line_object.attr('style', 'width: ' + new_value + '%;').data('now-value', new_value);

}



$(document).ready(function() {

    

    /*

        Form

    */

    $('.form-wizard fieldset:first').fadeIn('slow');

    

    $('.form-wizard .required').on('focus', function() {

    	$(this).removeClass('input-error');

    });

    

    // next step

    $('.form-wizard .btn-next').on('click', function() {

      var parent_fieldset = $(this).parents('fieldset');
      
    	var next_step = true;

    	// navigation steps / progress steps

    	var current_active_step = $(this).parents('.form-wizard').find('.form-wizard-step.active');

    	var progress_line = $(this).parents('.form-wizard').find('.form-wizard-progress-line');

    	

    	// fields validation

    	parent_fieldset.find('.required').each(function() {

    		if( $(this).val() == "" ) {

    			$(this).addClass('input-error');

    			 next_step = false;

    		}

    		else {

    			$(this).removeClass('input-error');

    		}

    	});

    	// fields validation

    	

    	if( next_step ) {

    		parent_fieldset.fadeOut(400, function() {

    			// change icons

    			current_active_step.removeClass('active').addClass('activated').next().addClass('active');

    			// progress bar

    			bar_progress(progress_line, 'right');

    			// show next step

	    		$(this).next().fadeIn();

	    		// scroll window to beginning of the form

    			scroll_to_class( $('.form-wizard'), 20 );

	    	});

    	}

    	

    });

    

    // previous step

    $('.form-wizard .btn-previous').on('click', function() {

    	// navigation steps / progress steps

    	var current_active_step = $(this).parents('.form-wizard').find('.form-wizard-step.active');

    	var progress_line = $(this).parents('.form-wizard').find('.form-wizard-progress-line');

    	

    	$(this).parents('fieldset').fadeOut(400, function() {

    		// change icons

    		current_active_step.removeClass('active').prev().removeClass('activated').addClass('active');

    		// progress bar

    		bar_progress(progress_line, 'left');

    		// show previous step

    		$(this).prev().fadeIn();

    		// scroll window to beginning of the form

			scroll_to_class( $('.form-wizard'), 20 );

    	});

    });

    

    // submit

    $('.form-wizard').on('submit', function(e) {

    	

    	// fields validation

    	$(this).find('.required').each(function() {

    		if( $(this).val() == "" ) {

    			e.preventDefault();

    			$(this).addClass('input-error');

    		}

    		else {

    			$(this).removeClass('input-error');

    		}

    	});

    	// fields validation

    	

    });

    

    

});











// image uploader scripts 



var $dropzone = $('.image_picker'),

    $droptarget = $('.drop_target'),

    $dropinput = $('#inputFile'),

    $dropimg = $('.image_preview'),

    $remover = $('[data-action="remove_current_image"]');



$dropzone.on('dragover', function() {

  $droptarget.addClass('dropping');

  return false;

});



$dropzone.on('dragend dragleave', function() {

  $droptarget.removeClass('dropping');

  return false;

});



$dropzone.on('drop', function(e) {

  $droptarget.removeClass('dropping');

  $droptarget.addClass('dropped');

  $remover.removeClass('disabled');

  e.preventDefault();

  

  var file = e.originalEvent.dataTransfer.files[0],

      reader = new FileReader();



  reader.onload = function(event) {

    $dropimg.css('background-image', 'url(' + event.target.result + ')');

  };

  

  console.log(file);

  reader.readAsDataURL(file);



  return false;

});



$dropinput.change(function(e) {

  $droptarget.addClass('dropped');

  $remover.removeClass('disabled');

  $('.image_title input').val('');

  

  var file = $dropinput.get(0).files[0],

      reader = new FileReader();

  

  reader.onload = function(event) {

    $dropimg.css('background-image', 'url(' + event.target.result + ')');

  }

  

  reader.readAsDataURL(file);

});



$remover.on('click', function() {

  $dropimg.css('background-image', '');

  $droptarget.removeClass('dropped');

  $remover.addClass('disabled');

  $('.image_title input').val('');

});



$('.image_title input').blur(function() {

  if ($(this).val() != '') {

    $droptarget.removeClass('dropped');

  }

});



// image uploader scripts









/*GET KOTA DESA */



//var url='http://192.168.207.1/gos/';















