function updateTime(){
  var hari= ["Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jum&#39;at", "Sabtu"];
  var bulan = ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"];
  var tanggal = new Date().getDate(); 
  var hari = hari[new Date().getDay()];
  var bulan = bulan[new Date().getMonth()];
  var tahun = new Date().getFullYear();
  var jam = new Date().getHours();
  var min = new Date().getMinutes();
  var sec = new Date().getSeconds();
  if (jam < 10)
    jam = "0"+jam;
  if (min < 10)
    min = "0"+min;
  if (sec < 10)
    sec = "0"+sec;

  var date = 'Hari: ' + hari + ', ' + tanggal + ' ' + bulan + ' ' + tahun + ' ' + jam + ':' + min + ":" + sec +' WIB';
    document.getElementById("tanggal").innerHTML = date;
}

$(document).ready(function(){
  setInterval(() => {
    updateTime()
  }, 1000);

  /* $("[data-widget='collapse']").click(function() {
    //Find the box parent  
    var test =       $("#daftar-layanan").find("i.fa-minus");
    console.log(test);
    //.removeClass("fa-minus").addClass("fa-plus");
}); */
})