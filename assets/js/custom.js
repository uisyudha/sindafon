
function getState(val) {
	$.ajax({
	type: "POST",
	url: url+"main/get_city",
	data:'country_id='+val,
	success: function(data){

		//console.log(data);
		$("#kota").html(data);
		
	}
	});
}

function getDistrict(val) {
	$.ajax({
	type: "POST",
	url: url+"main/get_district",
	data:'city_id='+val,
	success: function(data){

		//console.log(data);
		$("#kec").html(data);

	}
	});
}
function getVillage(val) {
	$.ajax({
	type: "POST",
	url: url+"main/get_village",
	data:'district_id='+val,
	success: function(data){

		//console.log(data);
		$("#kel").html(data);
		
	}
	});
}

function checkPeserta(val) {
	if(val=="99"){
       document.getElementById("no_peserta").style.display="none";
       document.getElementById("no_peserta").value="";
       document.getElementById("no_peserta").required=false;

    }else{
      document.getElementById("no_peserta").style.display="inline";
      document.getElementById("no_peserta").required=true;

    }
}

function setdropdown(val1,val2,val3){
				getState(val1);
				getDistrict(val2);
				getVillage(val3);
				return true;
}
function check_user_rm(val) {

	if(val.length>4){
				
		$.ajax({
		type: "POST",
		url: url+"layanan_daftar/get_user_rm",
		data:'no_rm='+val,
		success: function(data){
			if(data==''){
				console.log('no data');
			}
			else{
				document.getElementById("valid_rm").value="1";
				document.getElementById("no_rm").readOnly = true;

				//console.log(data);
				var obj=JSON.parse(data);
				/*console.log(obj);
				console.log(obj['no_rm']);*/
				setdropdown(obj['id_provinsi'],obj['id_kota'],obj['id_kecamatan']);
				
				$('#nama_pasien').val(obj['nama']);
				$('#title').val(obj['title']);
				$('#nama_ayah').val(obj['nama_ortu']);
				$("input[type=radio][name=kelamin][value='"+obj['jenis_kelamin']+"']").prop("checked",true);
				$("#status_kawin").val(obj['marital']);
				$("#pendidikan").val(obj['pendidikan']);
				$("#agama").val(obj['agama']);
				$("#tempat_lahir").val(obj['tempat_lahir']);
				var tgl_lahir=obj['tanggal_lahir'].split("-");

				$("#tgl_lahir").val(tgl_lahir[2]+'-'+tgl_lahir[1]+'-'+tgl_lahir[0]);
				$("#alamat_sekarang").val(obj['alamat']);
				$("#alamat_ktp").val(obj['alamat_ktp']);
				$("#provinsi").val(obj['id_provinsi']);
				$("#no_ktp").val(obj['no_ktp']);
				$("#no_telp").val(obj['no_telp']);
				$("#pekerjaan").val(obj['pekerjaan']);
				$("#penanggung_jawab").val(obj['nama_penanggung']);
				$("#alamat_penanggung").val(obj['alamat_penanggung']);
				$("#hubungan_pasien").val(obj['hubungan_penanggung']);
				$("#telp_penanggung").val(obj['no_penanggung']);
				setTimeout(function() {
					$("#kota").val(obj['id_kota']);
					$("#kec").val(obj['id_kecamatan']);
					$("#kel").val(obj['id_kelurahan']);
					console.log(obj['id_kelurahan']);
				}, 1000);
				
				


				
				
				
				
				
			}
			
			//$("#kel").html(data);
		}
		});
	}
	
}

function cetaknota(val){
	
	var no_invoice=pad(val,6);
	$.get( url+"layanan_kasir/get_invoice/"+val, function( data ) {
  		$( "#printarea" ).html( data );
  		$("#printarea").show();
  		
 		 $("#printarea").print({
  			mediaPrint: true,
  			title: 'Nota Bill '+no_invoice
   		});

		$("#printarea").html('');
   	$("#printarea").hide();


  		//alert( "Load was performed." );
});
}

function cetaktagihanall(val){
  
     $("#page-print").print({
        mediaPrint: true,
        title: 'Nota No  '+val
      });

  
}
function print_obat(val){
	
	var no_invoice=pad(val,6);
	$.get( url+"layanan_apotik/get_detail_obat/"+val, function( data ) {
  		$( "#printarea" ).html( data );
  		$("#printarea").show();
  		
 	$("#printarea").print({
  			mediaPrint: true,
  			title: 'Nota Bill '+no_invoice
  		});

		$("#printarea").html('');
  	$("#printarea").hide();


  		//alert( "Load was performed." );
});
}
function cetaknotaranap(val){
	
	var no_invoice=pad(val,6);
	$.get( url+"layanan_kasir/get_invoice_ranap/"+val, function( data ) {
  		$("#printarea").html( data );
  		$("#printarea").show();


 	$("#printarea").print({
  			mediaPrint: true,
  			title: 'Nota Bill '+no_invoice
  		});

		$("#printarea").html('');
  		$("#printarea").hide();


  		//alert( "Load was performed." );
});
}
function cetaknotaapotikrajal(val){
	
	var no_invoice=pad(val,6);
	$.get( url+"layanan_kasir/get_invoice_apotik_rajal/"+val, function( data ) {
  		$( "#printarea" ).html( data );
  		$("#printarea").show();
  		
 	$("#printarea").print({
  			mediaPrint: true,
  			title: 'Nota Bill '+no_invoice
  		});

		$("#printarea").html('');
  		$("#printarea").hide();


  		//alert( "Load was performed." );
});
}
function cetaknotaapotikrajalalkes(val){
  
  var no_invoice=pad(val,6);
  $.get( url+"layanan_kasir/get_invoice_apotik_rajal_alkes/"+val, function( data ) {
      $( "#printarea" ).html( data );
      $("#printarea").show();
      
  $("#printarea").print({
        mediaPrint: true,
        title: 'Nota Bill '+no_invoice
      });

    $("#printarea").html('');
      $("#printarea").hide();


      //alert( "Load was performed." );
});
}
function cetaknotaapotiklangsung(val){
	
	var no_invoice=pad(val,6);
	$.get( url+"layanan_kasir/get_invoice_apotik_langsung/"+val, function( data ) {
  		$( "#printarea" ).html( data );
  		$("#printarea").show();
 		
 	$("#printarea").print({
  			mediaPrint: true,
  			title: 'Nota Bill '+no_invoice
  		});

		$("#printarea").html('');
  		$("#printarea").hide();


  		//alert( "Load was performed." );
});
}

function cetaknotaapotikranap(val){
	
	var no_invoice=pad(val,6);
	$.get( url+"layanan_kasir/get_invoice_apotik_ranap/"+val, function( data ) {
  		$( "#printarea" ).html( data );
  		$("#printarea").show();
  		
 	$("#printarea").print({
  			mediaPrint: true,
  			title: 'Nota Bill '+no_invoice
  		});

		$("#printarea").html('');
  		$("#printarea").hide();


  		//alert( "Load was performed." );
});
}
function cetaknotaapotikranapalkes(val){
  
  var no_invoice=pad(val,6);
  $.get( url+"layanan_kasir/get_invoice_apotik_ranap_alkes/"+val, function( data ) {
      $( "#printarea" ).html( data );
      $("#printarea").show();
      
  $("#printarea").print({
        mediaPrint: true,
        title: 'Nota Bill '+no_invoice
      });

    $("#printarea").html('');
      $("#printarea").hide();


      //alert( "Load was performed." );
});
}
function export_excel(val){
	
	var no_invoice=pad(val,6);
	$.get( url+"layanan_kasir/get_invoice/"+val, function( data ) {
  		$( "#printarea" ).html( data );
  		$("#printarea").show();
  		
  		$('#printarea').tableExport({type:'pdf',escape:'false'});

		/*	w=window.open();
			//w.document.write(jQuery('#show_print').html());
			w.document.write($("#printarea").html());
			w.print();*/
  		$("#printarea").html('');
  		$("#printarea").hide();


  		//alert( "Load was performed." );
});
}
function export_table_harian(){
  var title=$('#table-harian-kasir').attr('title');
		$('#table-harian-kasir').tableExport(
      {filename:title}
    );   
}
function export_table_bulanan(){
  var title=$('#table-stok-bulanan').attr('title');
		$('#table-stok-bulanan').tableExport(
			{filename:title}
			);   
}

function editresep(){
  var a=confirm('Apakah Data Sudah Benar?');

  if(a){
    
  }
}


 $(function() {
            $( "#kodeicd" ).autocomplete({ 
            	source: url+"layanan_poli/get_icd",  
           		minLength:2, 
           		select: function( event, ui ) {
            		

            		
            		//console.log(selectedObj.value);
            		
            		
            		$('#kodeicd').val(ui.item.label);
					$("#icd").val(ui.item.value);
            		
				
							
					 return false;
        		},
             
            });   

             $( "#icd" ).autocomplete({ 
            	source: url+"layanan_poli/get_icd_by_name",  
           		minLength:3, 
           		select: function( event, ui ) {
            		var selectedObj = ui.item;  
            			$('#kodeicd').val(ui.item.value);
					$("#icd").val(ui.item.label);
            		 return false;
        		},
             
            }); 


             $( "#input-nama-tindakan" ).autocomplete({ 


            	source: url+"layanan_poli/get_tindakan_poli",  
           		minLength:1, 
           		select: function( event, ui ) {
            		var selectedObj = ui.item;  
            		//console.log(selectedObj.value);
            		$('#input-nama-tindakan').val(selectedObj.value);

            			$("#input-id-tindakan").val(ui.item.value);
            			$("#input-nama-tindakan").val(ui.item.label);
            			$("#input-jenis-tindakan").val(ui.item.desc);
				
						 return false;	
        		},
             
            }); 
             $( "#input-nama-tindakan-ranap" ).autocomplete({ 


            	source: url+"layanan_ranap/get_tindakan_ranap",  
           		minLength:1, 
           		select: function( event, ui ) {
            		var selectedObj = ui.item;  
            		//console.log(selectedObj.value);
            		$('#input-nama-tindakan').val(selectedObj.value);

            			$("#input-id-tindakan").val(ui.item.value);
            			$("#input-nama-tindakan-ranap").val(ui.item.label);
            			$("#input-jenis-tindakan").val(ui.item.desc);
				
						 return false;	
        		},
             
            }); 
             $( "#input-nama-tindakan-lain" ).autocomplete({ 


            	source: url+"layanan_poli/get_tindakan_poli_lain",  
           		minLength:1, 
           		select: function( event, ui ) {
            		var selectedObj = ui.item;  
            		//console.log(selectedObj.value);
            		$('#input-nama-tindakan').val(selectedObj.value);

            			$("#input-id-tindakan").val(ui.item.value);
            			$("#input-nama-tindakan-lain").val(ui.item.label);
            			$("#input-jenis-tindakan").val(ui.item.desc);
				
						 return false;	
        		},
             
            });  
             $( "#input-nama-tindakan-penunjang" ).autocomplete({ 


            	source: url+"layanan_penunjang/get_tindakan_penunjang",  
           		minLength:1, 
           		select: function( event, ui ) {
            		var selectedObj = ui.item;  
            		//console.log(selectedObj.value);
            		$('#input-nama-tindakan').val(selectedObj.value);

            			$("#input-id-tindakan").val(ui.item.value);
            			$("#input-nama-tindakan-penunjang").val(ui.item.label);
            			$("#input-jenis-tindakan").val(ui.item.desc);
				
						 return false;	
        		},
             
            });

             $( "#nama_tindakan_manual" ).autocomplete({ 


            	source: url+"layanan_kasir/get_daftar_layanan",  
           		minLength:2, 
           		select: function( event, ui ) {
            		var selectedObj = ui.item;  
            		//console.log(selectedObj.value);
            			$('#nama_tindakan_manual').val(selectedObj.label);
            			$("#id_tindakan").val(selectedObj.value);
            			
				
						 return false;	
        		},
             
            });
             // $( "#input-nama-tindakan" ).autocomplete( "option", "appendTo", ".eventInsForm" );
      });     


 $( function() {
  
    $( "#tgl_lahir" ).datepicker({ dateFormat: 'dd-mm-yy',
      changeMonth: true,
      changeYear: true,
      yearRange:"-100:+0"
    }); 
     $( ".input_tanggal_lahir" ).datepicker({ dateFormat: 'dd-mm-yy',
      changeMonth: true,
      changeYear: true,
      yearRange:"-100:+0"
    }); 
    $( ".input_tanggal" ).datepicker({ 
      changeMonth: true,
      changeYear: true
    });
     $( ".input_tanggal_penerimaan" ).datepicker({ 
     dateFormat: 'dd-mm-yy',
      changeMonth: true,
      changeYear: true
    });
    $( ".input_tanggal_ranap" ).datepicker({ dateFormat: 'yy-mm-dd',
      changeMonth: true,
      changeYear: true
    });
    $( "#tgl_start" ).datepicker({
      changeMonth: true,
      changeYear: true
    });
    $( "#tgl_end" ).datepicker({
      changeMonth: true,
      changeYear: true
    });
    $( "#tgl_pengadaan" ).datepicker({
      dateFormat: 'dd-mm-yy',
      changeMonth: true,
      changeYear: true
    });
 	$('#text-diagnosa').trumbowyg({
    lang: 'id',
    btns: [
      /*  ['viewHTML'],*/
        ['formatting'],
        'btnGrp-semantic',
        /*['superscript', 'subscript'],*/
       /* ['link'],
        ['insertImage'],*/
        'btnGrp-justify',
        'btnGrp-lists',
        ['horizontalRule'],
       /* ['removeformat'],*/
        ['fullscreen']
    ]
	});
	$('#text-terapi').trumbowyg({
    lang: 'id',
    btns: [
      /*  ['viewHTML'],*/
        ['formatting'],
        'btnGrp-semantic',
        /*['superscript', 'subscript'],*/
       /* ['link'],
        ['insertImage'],*/
        'btnGrp-justify',
        'btnGrp-lists',
        ['horizontalRule'],
       /* ['removeformat'],*/
        ['fullscreen']
    ]
	});

   
   
  } );



function pad (str, max) {
  str = str.toString();
  return str.length < max ? pad("0" + str, max) : str;
}




setInterval(function() {
var d = new Date();
var time = d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
  
  $('.time-input').val(time);
}, 1000);


$(function(){
	  dialog = $( "#data-pembayaran" ).dialog({
      autoOpen: false,
      height: 800,
      width: 1100,
      modal: true
    });
 dialog2 = $( "#data-tindakan" ).dialog({
      autoOpen: false,
      height: 800,
      width: 900,
      modal: true
    });


	
	$( "#btn-status-pembayaran" ).on( "click", function() {
		var data=$( "#btn-status-pembayaran" ).val();
		$.get( url+"layanan_poli/get_data_pembayaran/"+data, function( data ) {
  		
		$('#data-pembayaran').html('  '+data);
		dialog.dialog( "open" );
		});
      
    });
    $( "#btn-status-pembayaran-ranap" ).on( "click", function() {
		var data=$( "#btn-status-pembayaran-ranap" ).val();
		$.get( url+"layanan_ranap/get_data_pembayaran_ranap/"+data, function( data ) {
  		
		$('#data-pembayaran').html('  '+data);
		dialog.dialog( "open" );
		});
      
    });
	$( "#btn-riwayat-pasien" ).button().on( "click", function() {
		var data=$( "#btn-riwayat-pasien" ).val();
		var data2=$( "#btn-status-pembayaran" ).val();
		$.get( url+"layanan_poli/data_riwayat_pasien/"+data+"/"+data2, function( data ) {
  		
		$('#data-pembayaran').html('  '+data);
		dialog.dialog( "open" );
		});
      
    });

    $( "#btn-tindakan" ).on( "click", function() {
		
		var data2=$( "#btn-tindakan" ).val();
		$('#input-id-tindakan').val('');
		$('#input-nama-tindakan').val('');
		$('#input-nama-tindakan-lain').hide();
		$('#input-nama-tindakan').show();
		$('#add-tindakan').show();
		$('#add-tindakan-lain').hide();
		$.get( url+"layanan_poli/data_tindakan_poli_pasien/"+data2, function( data ) {
  		
		$('#tabel-tindakan').html('  '+data);
		

		dialog2.dialog( "open" );
		});
      
    }); 
    $( "#btn-tindakan-lain" ).on( "click", function() {
		
		var data2=$( "#btn-status-pembayaran" ).val();
		$('#input-id-tindakan').val('');
		$('#input-nama-tindakan').val('');
		$('#input-nama-tindakan-lain').show();
		$('#input-nama-tindakan').hide();
		$('#add-tindakan').hide();
		$('#add-tindakan-lain').show();

		$.get( url+"layanan_poli/data_tindakan_lain_pasien/"+data2, function( data ) {
  		
		$('#tabel-tindakan').html('  '+data);
		dialog2.dialog( "open" );
		});
      
    }); 
    $( "#btn-tindakan-penunjang" ).button().on( "click", function() {
		
		var data2=$( "#btn-tindakan-penunjang" ).val();

		var data3=$( "#input-asal-poli" ).val();
		var data4=$( "#id_penunjang" ).val();
		

		$.get( url+"layanan_penunjang/data_tindakan_penunjang/"+data2+"/"+data3+"/"+data4, function( data ) {
  		
		$('#tabel-tindakan').html('  '+data);
		dialog2.dialog( "open" );
		});
      
    });  
   

    $( "#btn-tindakan-ranap" ).on( "click", function() {
		
		var data2=$( "#btn-tindakan-ranap" ).val();
		$('#input-id-tindakan').val('');
		$('#input-nama-tindakan').val('');
		$('#input-nama-tindakan-lain').hide();
		$('#input-nama-tindakan').show();
		$('#add-tindakan').show();
		$('#add-tindakan-lain').hide();
		$.get( url+"layanan_ranap/data_tindakan_ranap_pasien/"+data2, function( data ) {
  		
		$('#tabel-tindakan').html('  '+data);
		

		dialog2.dialog( "open" );
		});
      
    }); 
    $( "#btn-konsultasi-gizi" ).button().on( "click", function() {
		
		var kunjungan=$( "#btn-status-pembayaran" ).val();
		var status=confirm('Konsultasi gizi?');

		if(status)
			alert('konsultasi gizi dilakukan');
		else
			alert('konsultasi gizi tidak dilakukan');
      
    }); 
 	$( "#add-tindakan" ).on( "click", function() {
		
		var id_tindakan=$( "#input-id-tindakan" ).val();

		var kunjungan=$( "#btn-tindakan" ).val();
		var qty=$( "#input-qty-tindakan" ).val();

		
		
  			$.ajax({
			type: "POST",
			url: url+"layanan_poli/add_tindakan_poli",
			data:'kunjungan='+kunjungan+
					'&id_tindakan='+id_tindakan+
					'&qty='+qty,
			success: function(data){

				if(data=='1'){
					 dialog2.dialog( "close" );
					 $( "#btn-tindakan" ).click()
					console.log('ok');
				}else{
					$( "#btn-tindakan" ).click()
					console.log('no');
				}

			}
			});
		
    });	
    $( "#add-tindakan-ranap" ).on( "click", function() {
		
		var id_tindakan=$( "#input-id-tindakan" ).val();
		var qty=$( "#input-qty-tindakan" ).val();

		var kunjungan=$( "#btn-tindakan-ranap" ).val();
		var dokter=$( "#input-nama-dokter" ).val();
		
		
  			$.ajax({
			type: "POST",
			url: url+"layanan_ranap/add_tindakan_ranap",
			data:'kunjungan='+kunjungan+
					'&id_tindakan='+id_tindakan+
					'&qty='+qty+
					'&dokter='+dokter,
			success: function(data){

				if(data=='1'){
					 dialog2.dialog( "close" );
					 $( "#btn-tindakan-ranap" ).click()
					console.log('ok');
				}else{
					$( "#btn-tindakan-ranap" ).click()
					console.log('no');
				}

			}
			});
		
    });	
    $( "#add-tindakan-lain" ).button().on( "click", function() {
		
		var id_tindakan=$( "#input-id-tindakan" ).val();
		var kunjungan=$( "#btn-status-pembayaran" ).val();
		
		
  			$.ajax({
			type: "POST",
			url: url+"layanan_poli/add_tindakan_poli",
			data:'kunjungan='+kunjungan+
					'&id_tindakan='+id_tindakan,
			success: function(data){

				if(data=='1'){
					 dialog2.dialog( "close" );
					 $( "#btn-tindakan-lain" ).click()
					console.log('ok');
				}else{
					$( "#btn-tindakan-lain" ).click()
					console.log('no');
				}

			}
			});
		
    });

     $( "#add-tindakan-penunjang" ).on( "click", function() {
		
		var id_tindakan=$( "#input-id-tindakan" ).val();
		var kunjungan=$( "#id_kunjungan" ).val();
		var asal_poli=$( "#input-asal-poli" ).val();
		
		
  			$.ajax({
			type: "POST",
			url: url+"layanan_penunjang/add_tindakan_penunjang",
			data:'kunjungan='+kunjungan+
					'&id_tindakan='+id_tindakan+
					'&asal_poli='+asal_poli,
			success: function(data){

				if(data=='1'){
					 dialog2.dialog( "close" );
					 $( "#btn-tindakan-penunjang" ).click()
					console.log('ok');
				}else{
					$( "#btn-tindakan-penunjang" ).click()
					console.log('no');
				}

			}
			});
		
    });


      dialog3 = $( "#data-kamar" ).dialog({
      autoOpen: false,
      height: 800,
      width: 900,
      modal: true
    	});
    
 $( "#btn-bed" ).button().on( "click", function() {
		
		dialog3.dialog( "open" );
	
      
    });

 	 dialog4 = $( "#data_pasien" ).dialog({
      autoOpen: false,
      height: 400,
      width: 400,
      modal: true
    	});

 	 dialog5 = $( "#tindakan-manual" ).dialog({
      autoOpen: false,
      height: 600,
      width: 800,
      modal: true
    	}); 

 	 dialog6 = $( "#tindakan-non-tarif" ).dialog({
      autoOpen: false,
      height: 400,
      width: 800,
      modal: true
    	});
   
 $( "#btn-tindakan-manual" ).on( "click", function() {
		
		dialog5.dialog( "open" );
	
      
    });

 $( "#btn-tindakan-non-tarif" ).on( "click", function() {
		
		dialog6.dialog( "open" );
	
      
    });


dialog7 = $( "#form-Transaksi" ).dialog({
      autoOpen: false,
      height: 350,
      width: 700,
      modal: true
    	});
	
	$( "#btn-transaksi-baru" ).on( "click", function() {
		
		dialog7.dialog( "open" );
	
      
    });
 dialog8 = $( "#form-tambah-obat" ).dialog({
      autoOpen: false,
      height: 350,
      width: 800,
      modal: true
    	});
	
	$( "#btn-tambah-item" ).on( "click", function() {
		
		dialog8.dialog( "open" );
	
      
    });



  


	//form obat racik
	dialog9 = $( "#form-obat-racik" ).dialog({
      autoOpen: false,
      height: 350,
      width: 600,
      modal: true
    	});
	
	
	//form obat 
	dialog10 = $( "#form-obat" ).dialog({
      autoOpen: false,
      height: 350,
      width: 600,
      modal: true
    	});
	
	//form detail obat racik
  dialog11 = $( "#form-detail-racik" ).dialog({
      autoOpen: false,
      height: 350,
      width: 600,
      modal: true
      });


  dialog12 = $( "#tindakan-manual-ranap" ).dialog({
      autoOpen: false,
      height: 600,
      width: 800,
      modal: true
      }); 

   dialog13 = $( "#tindakan-non-tarif-ranap" ).dialog({
      autoOpen: false,
      height: 400,
      width: 800,
      modal: true
      });

   dialog14 = $( "#resep-ranap" ).dialog({
      autoOpen: false,
      height: 400,
      width: 800,
      modal: true
      });

   //FORM OBAT RACIK (APOTIK)
    dialog20 = $( "#form-tambah-obat-racik" ).dialog({
      autoOpen: false,
      height: 350,
      width: 800,
      modal: true
      });
     //FORM OBAT RACIK detail (APOTIK)
    dialog21 = $( "#form-tambah-obat-racik-detail" ).dialog({
      autoOpen: false,
      height: 350,
      width: 800,
      modal: true
      });
    $( "#btn-tambah-item-racik" ).on( "click", function() {
    
    dialog20.dialog( "open" );
  
      
    });
   
 $( "#btn-tindakan-manual-ranap" ).on( "click", function() {
    
    dialog12.dialog( "open" );
  
      
    });

 $( "#btn-tindakan-non-tarif-ranap" ).on( "click", function() {
    
    dialog13.dialog( "open" );
  
      
    });

  $( "#btn-resep-baru-ranap" ).on( "click", function() {
    
    dialog14.dialog( "open" );
  
      
    });


})

  function tambah_obat_racik(a){
    $('#obat_racik_id').val(a);
    dialog21.dialog( "open" );
  }

 function add_obat_racik(a){
 	$('#id_resep_racik').val(a);
		dialog9.dialog( "open" );
	}
function add_detail_obat_racik(a){
  $('#id_racik').val(a);
    dialog11.dialog( "open" );
  }
 function add_obat_resep(a){
 	$('#id_resep').val(a);
		dialog10.dialog( "open" );
	}

function set_manual_tarif(id,val){
	$('#id_tindakan').val(id);
	$('#nama_tindakan_manual').val(val);
	$('#tindakan-manual').scrollTop(0);

}

function set_manual_tarif_penunjang(id,val){
	$('#input-id-tindakan').val(id);
	$('#input-nama-tindakan-penunjang').val(val);
	$('#input-jenis-tindakan').val(2);
	$('#data-tindakan').scrollTop(0);
	

}
function set_manual_tarif_poli(id,val){
	$('#input-id-tindakan').val(id);
	$('#input-nama-tindakan').val(val);
	$('#input-jenis-tindakan').val(2);
	$('#data-tindakan').scrollTop(0);
	

}
function set_manual_tarif_ranap(id,val){
	$('#input-id-tindakan').val(id);
	$('#input-nama-tindakan-ranap').val(val);
	$('#input-jenis-tindakan').val(2);
	$('#data-tindakan').scrollTop(0);
	

}
function setbed(val){

$.ajax({
	type: "GET",
	url: url+"layanan_ranap/get_bed_detail/"+val,
	
	success: function(data){

	//	console.log(data);
		
		$("#tempat_tidur").val(data.no_kamar);
		$("#no_bed").val(data.no_bed);
		$("#id_bed").val(val);
		dialog3.dialog( "close" );
	}
	});
}
function get_detail_kamar(val){
	dialog4.dialog( "open" );
$.ajax({
	type: "GET",
	url: url+"layanan_ranap/get_pasien_by_bed/"+val,
	
	success: function(data){

		console.log(data);
		
		$("#no_rm").html(data.kunjungan.no_rm);
		$("#nama_pasien").html(data.kunjungan.nama);
		$("#tanggal_masuk").html(data.kunjungan.masuk);
		
	}
	});
}
function edit_history_obat(id,tgl,kategori,nama_obat,waktu,keterangan){
	
		
		$("#jenis_obat").val(kategori);
		$("#nama_obat").val(nama_obat);
		$("#id_pemberian").val(id);
		$("#tgl_start").val(tgl);
		$("#keterangan_obat").val(keterangan);
		
	
}

function print_obat_ranap(no_rm){
	$("#table_obat_ranap").print({
  			mediaPrint: true,
  			title: 'Daftar Pemberian Obat Harian pasien no_rm '+no_rm
  		});
}

function print_tracer(){
	$("#tracer").print({
			globalStyles: false,
  			mediaPrint: true,
  			stylesheet: url+'assets/css/print.css',
  		});
}

function add_item_form(){
	
	var newrow=$('<tr>')
    	.append($('<td>')
    		.append('<input type="hidden"  class="input-sm form-control barang input_id_barang" name="id_brg[]">'+
    			 '<input type="text" class="input-sm form-control barang input_nama_barang ui-autocomplete-input" name="brg[]">')           
        )
        .append($('<td>')
    		.append($('#select-satuan').html())           
        )
        .append($('<td>')
        	.append('<input type="text" class="input-sm form-control" name="qty[]">')           
            
        ) 
        .append($('<td>')
        	.append('<input type="text" class="input-sm form-control" name="harga[]">')           
            
        ) .append($('<td>')
        	.append('<input type="text" class="form-control input-md input_tanggal" name="kadaluarsa[]">')           
            
        ).append($('<td>')
        	.append('<a href="javascript:void(0)" class="btn btn-danger btn-sm " onclick="$(this).closest(\'tr\').remove();"><i class="fa fa-times-circle" aria-hidden="true"></i></a>')           
            
        );

	newrow.find('.input_nama_barang').autocomplete({ 
            	source: url+"master_data/get_barang",  
           		minLength:2, 
           		select: function( event, ui ) {
            	console.log($(this));
            	console.log($(this).prev('.input_id_barang'));

            		$(this).val(ui.item.label);
            		$(this).prev('.input_id_barang').val(ui.item.value);
            		
            		
				
							
					 return false;
        		},
             
            });  

	newrow.find('.input_tanggal').datepicker({ 
			      changeMonth: true,
			      changeYear: true
			    }); 

	

	$("#table-item").find('tbody')
    .append(newrow
    );

    
}


function add_item_form_pengadaan(){
	
	var newrow=$('<tr>')
    	.append($('<td class="id_obat">')
    		.append('<input type="hidden"  class="input-sm form-control barang input_id_barang_pengadaan" name="id_brg[]">'+
    			 '<input type="text" class="input-sm form-control barang input_nama_barang_pengadaan ui-autocomplete-input" name="brg[]">')           
        )
        .append($('<td class="satuan-obat">')
    		.append('')           
        )
        .append($('<td>')
        	.append('<input type="text" class="input-sm form-control" name="qty[]" oninput="check_last_price(this)"> ')           
            
        ) 
        .append($('<td class="item-harga">')
        	.append('<input type="text" class="input-sm form-control" name="harga[]">')           
            
        ).append($('<td>')
        	.append('<a href="javascript:void(0)" class="btn btn-danger btn-sm " onclick="$(this).closest(\'tr\').remove();"><i class="fa fa-times-circle" aria-hidden="true"></i></a>')           
            
        );

	newrow.find('.input_nama_barang_pengadaan').autocomplete({ 
            	source: url+"master_data/get_barang_pengadaan",  
           		minLength:2, 
           		select: function( event, ui ) {
            	
            		$(this).val(ui.item.label);
            		$(this).prev('.input_id_barang_pengadaan').val(ui.item.value);
            		
           			 var satuan=$(this).closest('tr').find('td.satuan-obat');
           			 console.log(satuan);
					       getsatuanbarang(satuan,ui.item.value)	;
					
							
					 return false;
        		},
             
            });  

	

	

	$("#table-item").find('tbody')
    .append(newrow
    );

    
}

function add_item_form_permintaan(){
	
	var newrow=$('<tr>')
    	.append($('<td class="id_obat">')
    		.append('<input type="hidden"  class="input-sm form-control barang input_id_barang" name="id_brg[]">'+
    			 '<input type="text" class="input-sm form-control barang input_nama_barang_gudang ui-autocomplete-input" name="brg[]">')           
        )
        .append($('<td class="satuan-obat">')
    		.append('')           
        )
        .append($('<td>')
        	.append('<input type="text" class="input-sm form-control" name="qty[]"  > ')           
            
        ) 
        .append($('<td class="item-harga">')
        	.append('<textarea class="input-sm form-control" name="keterangan[]">')           
            
        ).append($('<td>')
        	.append('<a href="javascript:void(0)" class="btn btn-danger btn-sm " onclick="$(this).closest(\'tr\').remove();"><i class="fa fa-times-circle" aria-hidden="true"></i></a>')           
            
        );

	newrow.find('.input_nama_barang_gudang').autocomplete({ 
            	source: url+"master_data/get_barang_farmasi",  
           		minLength:2, 
           		select: function( event, ui ) {
            	
            		$(this).val(ui.item.label);
            		$(this).prev('.input_id_barang').val(ui.item.value);
            		
           			 var satuan=$(this).closest('tr').find('td.satuan-obat');
           			 console.log(satuan);
					getsatuanbarang(satuan,ui.item.value)	;
					
							
					 return false;
        		},
             
            });  

	

	

	$("#table-item").find('tbody')
    .append(newrow
    );

    
}
function add_item_form_pengeluaran(){
  
  var newrow=$('<tr>')
      .append($('<td class="id_obat">')
        .append('<input type="hidden"  class="input-sm form-control barang input_id_barang" name="id_brg[]">'+
           '<input type="text" class="input-sm form-control barang input_nama_barang_keluar ui-autocomplete-input" name="brg[]">')           
        )
        .append($('<td class="satuan-obat">')
        .append('')           
        )
        .append($('<td>')
          .append('<input type="text" class="input-sm form-control" name="qty[]"  > ')           
            
        ) 
        .append($('<td class="item-harga">')
          .append('<textarea class="input-sm form-control" name="keterangan[]">')           
            
        ).append($('<td>')
          .append('<a href="javascript:void(0)" class="btn btn-danger btn-sm " onclick="$(this).closest(\'tr\').remove();"><i class="fa fa-times-circle" aria-hidden="true"></i></a>')           
            
        );

  newrow.find('.input_nama_barang_keluar').autocomplete({ 
              source: url+"master_data/get_barang_poli",  
              minLength:2, 
              select: function( event, ui ) {
              
                $(this).val(ui.item.label);
                $(this).prev('.input_id_barang').val(ui.item.value);
                
                 var satuan=$(this).closest('tr').find('td.satuan-obat');
                 console.log(satuan);
          getsatuanbarang(satuan,ui.item.value) ;
          
              
           return false;
            },
             
            });  

  

  

  $("#table-item").find('tbody')
    .append(newrow
    );

    
}
function add_item_form_resep(){
	
	var newrow=$('<tr>')
    	.append($('<td class="item_barang">')
    		.append('<input type="hidden"  class="input-sm form-control barang input_id_barang" name="id_brg[]">'+
    			 '<input type="text" class="input-sm form-control barang input_nama_barang_poli ui-autocomplete-input" name="brg[]">')           
        )
        .append($('<td class="satuan" >')
    		.append($('#select-satuan').html())           
        )
        .append($('<td>')
        	.append('<input type="text" class="input-sm form-control" name="qty[]">')           
            
        ) 
        .append($('<td>')
        	.append('<textarea rows="3" class="input-sm form-control" name="keterangan[]"></textarea>')           
            
        ) .append($('<td class="note text-center">')
        	
            
        ).append($('<td>')
        	.append('<a href="javascript:void(0)" class="btn btn-danger btn-sm " onclick="$(this).closest(\'tr\').remove();"><i class="fa fa-times-circle" aria-hidden="true"></i></a>')           
            
        );

	newrow.find('.input_nama_barang_poli').autocomplete({ 
            	source: url+"master_data/get_barang",  
           		minLength:2, 
           		select: function( event, ui ) {
            	//console.log($(this));
            	//console.log($(this).prev('.input_id_barang'));

            		$(this).val(ui.item.label);
            		$(this).prev('.input_id_barang').val(ui.item.value);
            		
            		

            		//proses form 
					var satuan=$(this).closest('tr').find('td.satuan').find('select.select-satuan-poli');
					getsatuanbarang(satuan,ui.item.value)		
					 return false;
        		},
             
            });  
	newrow.find('.input_tanggal').datepicker({ 
			      changeMonth: true,
			      changeYear: true
			    }); 

	newrow.find( ".select-satuan-poli" ).change(function(){
            		var tr=$(this).closest('tr');
            		var id=$(this).closest('tr').find('td.item_barang').find('input.input_id_barang').val();
            		var satuan =$(this).val();
            		

            		$.ajax({
							type: "POST",
							url: url+"master_data/get_stok_apotik",
							data:'id_brg='+id+
									'&satuan='+satuan,
									
							success: function(data){
								value=jQuery.parseJSON(data);
								
								tr.find('td.note').html(value.stok);

							}
					});

            })
	

	$("#table-item").find('tbody')
    .append(newrow
    );

    
}

function updatetotalharga(a){
	$(a).closest('tr').find('td.total_harga').value='1';
	console.log($(a).closest('tr').find('td.total_harga'));

}

function getdetailpo(id_pengadaan){
	console.log('halooo '+id_pengadaan);
	$.ajax({
	type: "GET",
	url: url+"layanan_gudang/get_detail_po/"+id_pengadaan,
	
	success: function(data){
		var html='';
    var total=0;
    var totaldis=0;
    var totalppn=0;
		for(a= 0; a< data.length ;a++){
			html+='<tr><td>'+(a+1)+'</td><td>'+data[a].nama_obat+'</td>'+
				     
				     '<td class="item-qty"><input type="hidden" name="id_barang[]" value="'+data[a].obat_id+'">'+

				     	 
				     	 '<input type="text" class="form-control input-sm" name="qty[]" value="'+data[a].qty+'" oninput="updatetotal(this)"></td>'+
				     '<td class="item-harga"><input type="text" class="form-control input-sm" name="harga[]" value="'+data[a].harga+'" oninput="updatetotal(this)"></td>'+
				     '<td><input type="text" class="form-control input-sm input_tanggal" name="exp[]" required></td>'+
				     '<td class="item-diskon_persen"><input type="text" class="form-control input-sm " name="diskon_persen[]" value="0" oninput="diskon_persen_update(this)"></td>'+
				     '<td class="item-diskon"><input type="text" class="form-control input-sm " name="diskon_real[]" value="0" oninput="updatetotal(this)"></td>'+
				     '<td class="dpp text-right">'+(data[a].qty*data[a].harga).toLocaleString('id') +'</td>'+
				     '<td class="dpp_diskon text-right">'+(data[a].qty*data[a].harga).toLocaleString('id') +'</td>'+
				     '<td class="dpp_ppn text-right">'+((data[a].qty*data[a].harga)*1.1).toLocaleString('id')+'</td></tr>';
             total+=(data[a].qty*data[a].harga);
             totaldis+=(data[a].qty*data[a].harga);
             totalppn+=((data[a].qty*data[a].harga)*1.1);
		}
    html+='<tr class="text-info"><td colspan="7"><strong>Jumlah Total </strong></td>'+
              '<td class="total text-right"><strong>'+total.toLocaleString('id')+'</strong></td>'+
              '<td class="totaldis text-right"><strong>'+totaldis.toLocaleString('id')+'</strong></td>'+
              '<td class="totalppn text-right"><strong>'+totalppn.toLocaleString('id')+'</strong></td></tr>';

	
	console.log(data[0].no_pengadaan);
		
		$('.data-penerimaan').html(html);
		$('.data-penerimaan').find('.input_tanggal').datepicker({ dateFormat: 'dd-mm-yy',
      changeMonth: true,
      changeYear: true 
    }); 


	
            		
	$('#btn-submit-penerimaan').prop('disabled', false);
	}
	});

	
}

function check_last_price(a){
	console.log('check last price');
	var tr=a.closest('tr');
	var id_obat=$(tr).find('td.id_obat').find('input[name="id_brg[]"]').val();
	$.ajax({
	type: "GET",
	url: url+"layanan_gudang/get_last_price/"+id_obat,
	
	success: function(data){
		console.log(data);
		var id_obat=$(tr).find('td.item-harga').find('input[name="harga[]"]').val(data[0].harga);
    }
	}); 
}
function diskon_persen_update(a){
	var tr=a.closest('tr');
		var qty=$(tr).find('td.item-qty').find('input[name="qty[]"]').val();
		$(tr).find('td.item-qty').find('input[name="update_status[]"]').val('1');
		var harga=$(tr).find('td.item-harga').find('input[name="harga[]"]').val();
		var diskon_persen=$(tr).find('td.item-diskon_persen').find('input[name="diskon_persen[]"]').val();


		
		
		//update total
		var total=qty*harga;
		var diskonvalue=total*diskon_persen/100;
		$(tr).find('td.item-diskon').find('input[name="diskon_real[]"]').val(diskonvalue.toFixed(2));

		updatetotal(a);

}
function updatetotal(a){

	/*var tr=a.closest('tr');
		var qty=$(tr).find('td.item-qty').find('input[name="qty[]"]').val();
		$(tr).find('td.item-qty').find('input[name="update_status[]"]').val('1');
		var harga=$(tr).find('td.item-harga').find('input[name="harga[]"]').val();
		var diskon=$(tr).find('td.item-diskon').find('input[name="diskon_real[]"]').val();
		
		
		//update total
		var total=qty*harga;
		var persen_diskon=(diskon/total)*100;
		var diskon_persen=$(tr).find('td.item-diskon_persen').find('input[name="diskon_persen[]"]').val(persen_diskon.toFixed(2));

		var total_diskon=(qty*harga)-diskon;
		var total_ppn=total_diskon*(1+0.1);
		$(tr).find('td.dpp').html(total.toLocaleString());
		$(tr).find('td.dpp_diskon').html(total_diskon.toLocaleString());
		$(tr).find('td.dpp_ppn').html(total_ppn.toLocaleString());

 */
  
  
   var table = document.getElementsByClassName("data-penerimaan")[0];
   var allrows = table.getElementsByTagName("tr");
   
 
 
     var totalall = 0;
     var totaldis = 0;
     var totalppn = 0;
     for (i=0; i < (allrows.length-1); i++) {
        //row_cells = allrows[i].getElementsByTagName("input");
        //console.log(allrows[i].getElementsByTagName("input").length); //the number of inputs in the row
      var total=0;

        var qty=allrows[i].getElementsByTagName('input')[1].value;
        var harga=allrows[i].getElementsByTagName('input')[2].value;
        var diskon=allrows[i].getElementsByTagName('input')[5].value;
        total=qty*harga;
        totalall+=total;
        totaldis+=total-diskon;
         totalppn+=((total-diskon)*1.1);
        //UPDATE TOTAl
        allrows[i].getElementsByClassName('dpp')[0].innerHTML =total.toLocaleString('id');
        allrows[i].getElementsByClassName('dpp_diskon')[0].innerHTML =(total-diskon).toLocaleString('id');
        allrows[i].getElementsByClassName('dpp_ppn')[0].innerHTML =(total-diskon).toLocaleString('id');

      
       
     }
     allrows[allrows.length-1].getElementsByClassName('total')[0].innerHTML ='<strong>'+totalall.toLocaleString('id')+'</strong>';
     allrows[allrows.length-1].getElementsByClassName('totaldis')[0].innerHTML ='<strong>'+totaldis.toLocaleString('id')+'</strong>';
     allrows[allrows.length-1].getElementsByClassName('totalppn')[0].innerHTML ='<strong>'+totalppn.toLocaleString('id')+'</strong>';
}

function updatestatusinput(a){
	var tr=a.closest('tr');
		
		$(tr).find('td.item-qty').find('input[name="update_status[]"]').val('1');
}


function getsatuanbarang(satuan,id_barang){
	$.ajax({
	type: "POST",
	url: url+"master_data/get_satuan",
	data:'id_barang='+id_barang,
	success: function(data){

		//console.log(data);
		//$("#kota").html(data);
		satuan.html(data);
		satuan.trigger('change');
		return data;
	}
	});

}
function add_item_form_transaksi_obat(){
	
	var newrow=$('<tr>')
    	.append($('<td class="item_barang">')
    		.append('<input type="hidden"  class="input-sm form-control barang input_id_barang" name="id_brg[]">'+
    			 '<input type="text" class="input-sm form-control barang input_nama_barang_transaksi ui-autocomplete-input" name="brg[]">')           
        )
        .append($('<td class="satuan  select-satuan-transaksi" >')
    		.append($('#select-satuan').html())           
        )
        .append($('<td>')
        	.append('<input type="text" class="input-sm form-control" name="qty[]">')           
            
        ) 
        .append($('<td>')
        	.append('<p class="harga-item-satuan">Rp. 0,00</p>')           
         	
        ).append($('<td>')
        		.append('<p class="harga-item-total">Rp. 0,00</p>')
           
        ).append($('<td>')
        	.append('<p class="stok">0</p>')
        	
        ).append($('<td>')
        	.append('<a href="javascript:void(0)" class="btn btn-danger btn-sm " onclick="$(this).closest(\'tr\').remove();"><i class="fa fa-times-circle" aria-hidden="true"></i></a>')           
            
        );

	newrow.find('.input_nama_barang_transaksi').autocomplete({ 
            	source: url+"master_data/get_barang",  
           		minLength:2, 
           		select: function( event, ui ) {
            	//console.log($(this));
            	//console.log($(this).prev('.input_id_barang'));

            		$(this).val(ui.item.label);
            		$(this).prev('.input_id_barang').val(ui.item.value);
            		$(this).closest('tr').find('td.satuan').find('select.select-satuan-transaksi').trigger("change");
							
					 return false;
        		},
             
            });  
	newrow.find('.input_tanggal').datepicker({ 
			      changeMonth: true,
			      changeYear: true
			    }); 

	/*newrow.find( ".select-satuan-transaksi" ).change(function(){
            		var tr=$(this).closest('tr');
            		var id=$(this).closest('tr').find('td.item_barang').find('input.input_id_barang').val();
            		var satuan =$(this).val();
            		

            		$.ajax({
							type: "POST",
							url: url+"master_data/get_stok_apotik",
							data:'id_brg='+id+
									'&satuan='+satuan,
									
							success: function(data){
								value=jQuery.parseJSON(data);
								
								tr.find('td.note').html(value.stok);

							}
					});

            })
	

	$("#table-item").find('tbody')
    .append(newrow
    );
*/
    
}

function get_histori_pasien(a){
	var jenis_pasien=$('.jenis_pasien').val();
	var no_rm=a;
	if(jenis_pasien==null){
		alert('pilih dahulu jenis pasien');
	}
	console.log('jenis pasien '+jenis_pasien+' , no_rm'+no_rm);
if(a.length!=6)
  return false;

 
	if(jenis_pasien==1){
		//get pasien rajal
			$.ajax({
	
							url: url+"layanan_apotik/get_data_pasien_rajal/"+no_rm,
							success: function(data){
								

							
								
							if(data.length>0){
								$('.tombol-ok').prop("disabled",false);
								$('.id_kunjungan').val(data[0]['kid']);
								$('.nama_pasien').val(data[0]['nama']+' ,'+data[0]['title']);
								$('.asal_poli').val(data[0]['layanan']);
								$('.poli').val(data[0]['nama_layanan']);
								$('.tanggal_kunjungan').val(data[0]['tgl_kunjungan']);

								
							}

							
							
							
								

							}
					});

	}else if(jenis_pasien==2){

		//get_pasien_ranap	
	$.ajax({
	
							url: url+"layanan_apotik/get_data_pasien_ranap/"+no_rm,
							success: function(data){
								

							
								
							if(data.length>0){
								$('.tombol-ok').prop("disabled",false);
								$('.id_ranap').val(data[0]['ranap_id']);
								$('.nama_pasien').val(data[0]['nama']+' ,'+data[0]['title']);
								$('.asal_poli').val(data[0]['penanggung_jawab']);
                $('.poli').val(data[0]['nama_layanan']);
								$('.tanggal_kunjungan').val(data[0]['tgl_kunjungan']);

								
							}

							
							
							
								

							}
					});


	}
}




$(function() {

			$( ".nama_obat" ).autocomplete({ 
            	source: url+"master_data/get_barang",  
           		minLength:2, 
           		select: function( event, ui ) {
            	

            		$(this).val(ui.item.label);
            		$('.id_obat').val(ui.item.value);
            		
                var id=ui.item.value;
                  	$.ajax({
                    type: "POST",
                    url: url+"master_data/get_stok_apotik",
                    data:'id_brg='+id,
                        
                    success: function(data){
                      value=jQuery.parseJSON(data);
                      
                      $('.stok_obat').html(value.stok);
                      

                        }
                    });
            		
				
							
					 return false;
        		},
             
            });    
			


            $( ".input_nama_barang" ).autocomplete({ 
              source: url+"master_data/get_barang",  
              minLength:2, 
              select: function( event, ui ) {
              console.log($(this));
              console.log($(this).prev('.input_id_barang'));

                $(this).val(ui.item.label);
                $(this).prev('.input_id_barang').val(ui.item.value);
                
                 var satuan=$(this).closest('tr').find('td.satuan-obat');
                 console.log(satuan);
          getsatuanbarang(satuan,ui.item.value) ;
              return false;
            },
             
            }); 
                 
          $( ".input_nama_barang_gudang" ).autocomplete({ 
            	source: url+"master_data/get_barang_farmasi",  
           		minLength:2, 
           		select: function( event, ui ) {
            	console.log($(this));
            	console.log($(this).prev('.input_id_barang'));

            		$(this).val(ui.item.label);
            		$(this).prev('.input_id_barang').val(ui.item.value);
            		
            		 var satuan=$(this).closest('tr').find('td.satuan-obat');
           			 console.log(satuan);
					getsatuanbarang(satuan,ui.item.value)	;
            	return false;
        		},
             
            }); 
          $( ".input_nama_barang_pengadaan" ).autocomplete({ 
              source: url+"master_data/get_barang_pengadaan",  
              minLength:2, 
              select: function( event, ui ) {
              console.log($(this));
              console.log($(this).prev('.input_id_barang_pengadaan'));

                $(this).val(ui.item.label);
                $(this).prev('.input_id_barang_pengadaan').val(ui.item.value);
                
                 var satuan=$(this).closest('tr').find('td.satuan-obat');
                 console.log(satuan);
                 getsatuanbarang(satuan,ui.item.value) ;
              return false;
            },
             
            }); 
          $( ".input_nama_barang_keluar" ).autocomplete({ 
              source: url+"master_data/get_barang_poli",  
              minLength:2, 
              select: function( event, ui ) {
              console.log($(this));
              console.log($(this).prev('.input_id_barang'));

                $(this).val(ui.item.label);
                $(this).prev('.input_id_barang').val(ui.item.value);
                
                 var satuan=$(this).closest('tr').find('td.satuan-obat');
                 console.log(satuan);
          getsatuanbarang(satuan,ui.item.value) ;
              return false;
            },
             
            }); 


            $( ".input_nama_barang_poli" ).autocomplete({ 
            	source: url+"master_data/get_barang",  
           		minLength:2, 
           		select: function( event, ui ) {
            	//console.log($(this));
            	//console.log($(this).prev('.input_id_barang'));

            		$(this).val(ui.item.label);
            		$(this).prev('.input_id_barang').val(ui.item.value);
            		
            		
            		var satuan=$(this).closest('tr').find('td.satuan').find('select.select-satuan-poli');
					getsatuanbarang(satuan,ui.item.value);
					var drop=$('#satuan-barang-input');
					getsatuanbarang(drop,ui.item.value);
							
					 return false;
        		},
             
            });  
              $( "#nama_obat_transaksi" ).autocomplete({ 
              	source: url+"master_data/get_barang",  
             		minLength:2, 
             		select: function( event, ui ) {
                  	//console.log($(this));
                  	//console.log($(this).prev('.input_id_barang'));

                  		$(this).val(ui.item.label);
                  		$('#id_obat').val(ui.item.value);
                  		$('.stok').html(ui.item.stok);
                       $('#stok_obat').val(ui.item.stok);
                  		id=ui.item.value;
      				/*	
              	$.ajax({
        							type: "POST",
        							url: url+"master_data/get_stok_apotik",
        							data:'id_brg='+id,
        									
        							success: function(data){
        								value=jQuery.parseJSON(data);
        								
        								$('.stok').html(value.stok);
        								$('#stok_obat').val(value.stok);

        							}
      					     });
                     */
      							
      					  return false;
              		},
             
              }); 
               $( "#nama_obat_transaksi_racik" ).autocomplete({ 
                source: url+"master_data/get_barang",  
                minLength:2, 
                select: function( event, ui ) {
                    //console.log($(this));
                    //console.log($(this).prev('.input_id_barang'));

                      $(this).val(ui.item.label);
                      $('#id_obat_racik').val(ui.item.value);
                      
                      id=ui.item.value;
                  $.ajax({
                      type: "POST",
                      url: url+"master_data/get_stok_apotik",
                      data:'id_brg='+id,
                          
                      success: function(data){
                        value=jQuery.parseJSON(data);
                        
                        $('.stok_racik').html(value.stok);
                        $('#stok_obat_racik').val(value.stok);

                      }
                     });
                    
                  return false;
                  },
             
              });     
              $( ".select-satuan-transaksi" ).change(function(){
            		
            		var id=$('#id_obat').val();
            		var satuan =$(this).val();
            		

            		$.ajax({
							type: "POST",
							url: url+"master_data/get_stok_apotik",
							data:'id_brg='+id+
									'&satuan='+satuan,
									
							success: function(data){
								value=jQuery.parseJSON(data);
								
								$('.stok').html(value.stok);
								$('#stok_obat').val(value.stok);

							}
					});

            })
         /*    $( ".input_nama_barang_transaksi" ).autocomplete({ 
            	source: url+"master_data/get_barang",  
           		minLength:2, 
           		select: function( event, ui ) {
            	//console.log($(this));
            	//console.log($(this).prev('.input_id_barang'));

            		$(this).val(ui.item.label);
            		$(this).prev('.input_id_barang').val(ui.item.value);
            		
            		$(this).closest('tr').find('td.satuan').find('select.select-satuan-transaksi').trigger("change");
					
							
					 return false;
        		},
             
            });   
             $( ".qty-item-transaksi" ).on("change paste keyup", function(){
            		var tr=$(this).closest('tr');
            		var id=$(this).closest('tr').find('td.item_barang').find('input.input_id_barang').val();
            		var satuan=$(this).closest('tr').find('td.satuan').find('select.select-satuan-transaksi').val();
            		var cara_bayar=$('#cara_bayar').val();
            		var qty =$(this).val();
            		var total =parseInt($('#total-semua-item').html());

					console.log(total) ;

            		$.ajax({
							type: "POST",
							url: url+"master_data/get_harga_barang",
							data:'id_brg='+id+
									'&satuan='+satuan+
									'&cara_bayar='+cara_bayar,
									
							success: function(data){
								value=jQuery.parseJSON(data);
								harga=value.harga;
								tr.find('p.harga-item-satuan').html('Rp. '+harga);
								tr.find('p.harga-item-total').html('Rp. '+harga*qty);
								total+=(harga*qty)
								$('#total-semua-item').html(total);

							}
					});

            });*/
            $( ".input_nama_barang_kasir" ).autocomplete({ 
            	source: url+"master_data/get_barang",  
           		minLength:2, 
           		select: function( event, ui ) {
            	//console.log($(this));
            	//console.log($(this).prev('.input_id_barang'));

            		$(this).val(ui.item.label);
            		$(this).prev('.input_id_barang').val(ui.item.value);
            		
							
					 return false;
        		},
             
            });   

            $( ".select-satuan-poli" ).change(function(){
            		var tr=$(this).closest('tr');
            		var id=$(this).closest('tr').find('td.item_barang').find('input.input_id_barang').val();
            		var satuan =$(this).val();
            		

            		$.ajax({
							type: "POST",
							url: url+"master_data/get_stok_apotik",
							data:'id_brg='+id+
									'&satuan='+satuan,
									
							success: function(data){
								value=jQuery.parseJSON(data);
								
								tr.find('td.note').html(value.stok);

							}
					});

            });

            $( ".select-satuan-transaksi" ).change(function(){
            		var tr=$(this).closest('tr');
            		var id=$(this).closest('tr').find('td.item_barang').find('input.input_id_barang').val();
            		var satuan =$(this).val();
            		

            		$.ajax({
							type: "POST",
							url: url+"master_data/get_stok_apotik",
							data:'id_brg='+id+
									'&satuan='+satuan,
									
							success: function(data){
								value=jQuery.parseJSON(data);
									tr.find('td.stok').html(value.stok);

							}
					});

            });



            //update status keluar
            $('#select-status-keluar').change(function(){
              console.log($(this).val());
              $('#poli-lain').show();
              if($(this).val()==='9'){
                $('#poli-lain').prop('required',true);
                $('#poli-lain').show();

              }else{
                $('#poli-lain').prop('required',false);
                $('#poli-lain').hide();
              }
            });




            /*disable enter for submit*/

            $('#form-resep').keydown(function (e) {
              if (e.keyCode == 13) {
                  var inputs = $(this).parents("form").eq(0).find(":input");
                  if (inputs[inputs.index(this) + 1] != null) {                    
                      inputs[inputs.index(this) + 1].focus();
                  }
                  e.preventDefault();
                  return false;
              }
            });


});



/*NEW METHOD UPDATE*/

$(function(){

      dialogsep = $( "#form-sep" ).dialog({
      autoOpen: false,
      height: 550,
      width: 600,
      modal: true
      });
    
    
     $( "#tglSEP" ).datepicker({ dateFormat: 'yy-mm-dd',
      changeMonth: true,
      changeYear: true,
      yearRange:"-100:+0"
    }); 

  
});


//DISBLE ENABLE GENERATE SEP BUTTON
$(document).ready(function() {
    $('#no_peserta').keyup(function() {

        var status = false;
        $('#no_peserta').each(function() {
            if ($(this).val().length == 13) {
                status = true;
            }
        });

        if (status) {
            $('#cr-sep').removeAttr('disabled');
            
           
        } else {
           $('#cr-sep').attr('disabled', 'disabled');
           
        }
    });
    $('#no_peserta').on('paste',function() {

        var status = false;
        $('#no_peserta').each(function() {
            if ($(this).val().length == 13) {
                status = true;
            }
        });

        if (status) {
            $('#cr-sep').removeAttr('disabled');
            
           
        } else {
           $('#cr-sep').attr('disabled', 'disabled');
           
        }
    });
});

function opensep(){
       dialogsep.dialog( "open" );
        
     

       var tglsep=$('#sep_tgl').val();
       var nobpjs=$('#no_peserta').val();
       var no_rujukan=$('#no_rujuk').val();
       var cara_bayar=$('#cara_bayar').val();
       var poli=$('#poli').val();
       var politext=$('#poli option:selected').text();
       
       var asal_masuk=0;
       if($('#radi-0').is(':checked'))
          asal_masuk=0; /*DATA SENDIRI*/
        else if($('#radi-1').is(':checked'))
          asal_masuk=1;  /*puspesmas(faskes 1)*/
        else if($('#radi-1').is(':checked'))
           asal_masuk=2;  /*RS(faskes 2)*/
         var catatan='';
         var diagAwal='Z76.9';
       
       var jenis='2' ;/* 1=RI ,2=RJ*/
     
       $('#sep_nokartu').val(nobpjs);
       $('#sep_norujuk').val(0);
       $('#sep_poli').val(politext);
       $('#sep_status').val('');
       $('#sep_nama').val('');
       $('#sep_kelas').val('');
       $('#sep_telp').val('');
       $('#sep_keterangan').val('-');
       $('#panel-sep').hide();
        $('#rujukan_help').html('');
       //GET KOPE POLI 
        $.ajax({
          type: "GET",
          cache: false,
          url: url+"api_vklaim/get_kode_poli/"+poli,
           success: function(data){
             var obj=JSON.parse(data);
               
            if(obj.length=='0'){
                if(poli!=null){
                  $('#sep_poli').val('Poli Belum Tersedia, Hub Admin');   
                }else{
                  $('#sep_poli').val('Silahkan Pilih Poli Dahulu');   
                }
               
            }else{
                $('#sep_poli_value').val(obj[0].kode_poli);
                $('#panel-sep').show();
                 //GET STATUS bpjs
                    $.ajax({
                      type: "GET",
                      cache: false,
                      url: url+"api_vklaim/peserta_no/"+nobpjs+"/"+tglsep,
                      beforeSend:function(){
                        $('#loadingsep').show();
                        $('#sep_btn').hide();
                        $('#sep_rebtn').hide();
                      },
                      complete: function(){
                          $('#loadingsep').hide();
                          if($('#sep_btn').is(':visible')) {
                            // Code
                          }else{
                            $('#sep_rebtn').show();  
                          }
                          

                      },

                      success: function(dataa){
                        $('#loadingsep').hide();
                        $('#sep_btn').show();
                        
                         var obj=JSON.parse(dataa);
                          
                           
                          if (obj.metaData!= null) {
                            if(obj.metaData.code==200){
                              
                               $('#sep_status').val(obj.response.peserta.statusPeserta.keterangan);
                               $('#sep_nama').val(obj.response.peserta.nama);
                               $('#sep_nik').val(obj.response.peserta.nik);
                               $('#sep_kelas').val(obj.response.peserta.hakKelas.keterangan);
                               $('#sep_kelas_value').val(obj.response.peserta.hakKelas.kode);
                               $('#sep_telp').val(obj.response.peserta.mr.noTelepon);
                               $('#sep_norm').val(obj.response.peserta.mr.noMR);
                          
                               var nama_pasien=$('#nama_pasien').val();
                               var lahir=$('#tgl_lahir').val();
                               var nik=$('#no_ktp').val();

                                var status_peserta=$("input[name='s_pasien']:checked").val();
                                var norm=0;

                                if(status_peserta==2){
                                      norm= $('#no_rm').val();
                                  }else{
                                     norm= $('#norm-baru').val();
                                  }

                                console.log('status peserta :'+status_peserta);
                                console.log('nik :'+nik);
                                console.log('norm :'+norm);
                                //cek no nik
                                  console.log('nik  :'+nik.trim()+' - '+obj.response.peserta.nik.trim());
                               if(nik.trim()!=obj.response.peserta.nik.trim()){
                                  $('#sep_btn').prop('disabled', true);
                                  $('#sep_nik').focus();
                                  $('.sep_nik_status').html('<i title="Data Peserta Tidak Sama dengan Data SIMRS" class="fa fa-times" aria-hidden="true" style="font-size:24px;color:red"></i>');
                               }
                               else{
                                  $('#sep_btn').prop('disabled', false);
                                  $('#sep_nik').blur();
                                  $('.sep_nik_status').html('<i title="Data Peserta Sesuai dengan Data SIMRS" class="fa fa-check" aria-hidden="true" style="font-size:24px;color:green"></i>');
                               }
                                 console.log('norm  :'+norm.trim()+' - '+obj.response.peserta.mr.noMR.trim());
                               if(norm.trim()!=obj.response.peserta.mr.noMR.trim()){
                                
                                  $('#sep_norm').focus();
                                  $('.sep_norm_status').html('<i title="Data Peserta Tidak Sama dengan Data SIMRS" class="fa fa-times" aria-hidden="true" style="font-size:24px;color:red"></i>');
                               }
                               else{
                               
                                  $('#sep_norm').blur();
                                  $('.sep_norm_status').html('<i title="Data Peserta Sesuai dengan Data SIMRS" class="fa fa-check" aria-hidden="true" style="font-size:24px;color:green"></i>');
                               }
                                
                               if(obj.response.peserta.statusPeserta.kode=='0'){
                                  $('#sep_status').blur();
                                   if(poli==16){
                                    console.log('ugd ');
                                    
                                      if(status_peserta==2){
                                         

                                      }else{
                                          $('#sep_btn').prop('disabled', false);
                                      }

                                  }else{
                                   $('#sep_norujuk').val('Sedang mencari...');    
                                   cari_rujukan(nobpjs);

                                  }
                               }
                               else{
                                     
                                   /*PESERTA TIDAK DITEMUKAN*/
                                    $('#sep_status').focus();
                                    $('#sep_btn').attr('disabled', 'disabled');
                                   
                               }
                           }else{
                              $('#sep_btn').hide();
                           }
                         
                        }else{
                              $('#sep_btn').hide();
                        }
                      }

                      });
              
            }
            
             
            
          }
          });

      
       
}


function buatSEP(){

    var no_rujukan=$('#sep_norujuk').val();
    var no_kartu=$('#sep_nokartu').val();
    
    var tgl_sep=$('#sep_tgl').val();
    var kelas=$('#sep_kelas_value').val();
    var poli=$('#sep_poli_value').val();
    var telp=$('#sep_telp').val();
    var lakalantas='0';
    var penjamin='0';
    var lokasilaka=' ';
    var user='SIMRS';
    
    /*GET NORM*/
    var status_peserta=$("input[name='s_pasien']:checked").val();
    var norm=0;
        if(status_peserta==2){
            norm= $('#no_rm').val();
         }else{
            norm= $('#norm-baru').val();
         }


      /*PENCARIAN SEP*/
        $.ajax({
          type: "GET",
          cache: false,
           
          url: url+"api_vklaim/monitor_kunjungan/"+tgl_sep+"/"+2,
           success: function(data){
             var obj=JSON.parse(data);
           //   console.log(obj);
              var status = false;
              if (obj.metaData!= null) {
                if(obj.metaData.code==200){
                    if(status==false){

                        /*PENGECEKAN JIKA DITEMUKAN KUNJUNGAN NOMOR YANG SAMA*/
                        for(var a=0;a<obj.response.sep.length;a++){
                          console.log(no_kartu+' '+obj.response.sep[a].noKartu);
                          if(obj.response.sep[a].noKartu==no_kartu){
                            status=true;
                            break;
                          }
                        }
                      }

                  }else{
                    
                  }
              }else{
                alert('Permintaan Gagal, silahkan ulangi');
                return false;
              }

          
              

              if(status){
                alert('Peserta Telah Terdaftar pada Kunjungan hari ini');

                return false;
              } else{

                if(no_rujukan==0){
                    var catatan=' ';
                    var asal_rujuk='2';
                    var diag='Z76.9';
                    
                      /*PEMBUATAN SEP TANPA RUJUKAN*/
                      $.ajax({
                        type: "POST",
                        cache: false,
                        data:{  'nokartu' : no_kartu,
                                'nomr' : norm,
                                'tgl_sep' : tgl_sep,
                                'jenis'  : '2',
                                'kelas'  : kelas,
                                'asal_rujuk'  : asal_rujuk,
                                'tgl_rujuk'  : tgl_sep,
                                'no_rujuk'  : '0',
                                'ppk_rujuk'  : '0',
                                'catatan'  : catatan,
                                'diag'  : diag,
                                'poli'  : poli,
                                'telp'  : telp,
                                'lakalantas'  : lakalantas,
                                'penjamin'  : penjamin,
                                'lokasilaka'  : lokasilaka,
                                'user'  : user,
                                'user'  : user,

                              },
                        url: url+"api_vklaim/sep_insert",
                         success: function(data){
                           var obj=JSON.parse(data);
                            console.log(obj);
                            if (obj.metaData!= null) {
                              if(obj.metaData.code==200){
                                alert('Pembuatan SEP Berhasil');
                                 $( "#form-sep" ).dialog('close');
                                 $( "#cr-sep" ).hide();
                                 $( "#pr-sep" ).show();
                                  $('#no_sep').val(obj.response.sep.noSep);
                                
                              }else{
                                alert('Pembuatan SEP GAGAL SILAHKAN ULANGI PROSES');
                              }
                            }
                            else{
                               alert('Pembuatan SEP GAGAL SILAHKAN ULANGI PROSES');
                            }
                         }
                       });

                  }else{
                    var asal=$('input[name=asal_masuk]:checked').val();
                    var asal_rujuk=0;
                    if(asal==2)
                    asal_rujuk=1;
                    else if(asal==3)
                    asal_rujuk=2;
                    else{
                      alert('pilih asal Rujukan Terlebih dahulu');
                      return false;
                    }              
                    /*Pencarian RUJUKAN*/
                      $.ajax({
                        type: "GET",
                        cache: false,
                        url: url+"api_vklaim/rujukan_nokartu/"+nokartu,
                         success: function(data){
                           var obj=JSON.parse(data);

                            if (obj.metaData!= null) {
                              if(obj.metaData.code==200){
                                       var asal=$('input[name=asal_masuk]:checked').val();

                                      

                                      var catatan=' ';
                                      var diag=obj.response.rujukan.diagnosa.kode;
                                      var tgl_rujuk=obj.response.rujukan.tglKunjungan;
                                      var no_rujuk=obj.response.rujukan.noKunjungan;
                                      var ppk_rujuk=obj.response.rujukan.provPerujuk.kode;
                                      
                                        /*PEMBUATAN SEP TANPA RUJUKAN*/
                                        $.ajax({
                                          type: "POST",
                                          cache: false,
                                          data:{  'nokartu' : no_kartu,
                                                  'nomr' : norm,
                                                  'tgl_sep' : tgl_sep,
                                                  'jenis'  : '2',
                                                  'kelas'  : kelas,
                                                  'asal_rujuk'  : asal_rujuk,
                                                  'tgl_rujuk'  : tgl_rujuk,
                                                  'no_rujuk'  : no_rujuk,
                                                  'ppk_rujuk'  : ppk_rujuk,
                                                  'catatan'  : catatan,
                                                  'diag'  : diag,
                                                  'poli'  : poli,
                                                  'telp'  : telp,
                                                  'lakalantas'  : lakalantas,
                                                  'penjamin'  : penjamin,
                                                  'lokasilaka'  : lokasilaka,
                                                  'user'  : user,
                                                  

                                                },
                                          url: url+"api_vklaim/sep_insert",
                                           success: function(data){
                                             var obj=JSON.parse(data);
                                              console.log(obj);
                                              if (obj.metaData!= null) {
                                                if(obj.metaData.code==200){
                                                   $( "#form-sep" ).dialog('close');
                                                   $( "#cr-sep" ).hide();
                                                   $( "#pr-sep" ).show();
                                                    $('#no_sep').val(obj.response.sep.noSep);
                                                  
                                                }else{
                                                  alert('Pembuatan SEP GAGAL SILAHKAN ULANGI PROSES');
                                                }
                                              }
                                              else{
                                                 alert('Pembuatan SEP GAGAL SILAHKAN ULANGI PROSES');
                                              }
                                           }
                                         });
                                
                              }else{
                                alert('Pembuatan SEP GAGAL SILAHKAN ULANGI PROSES');
                              }
                            }
                            else{
                               alert('Pembuatan SEP GAGAL SILAHKAN ULANGI PROSES');
                            }
                         }
                       });
                  }


  
              }
           }
         });


    
}

function cari_rujukan(nokartu){


    //GET KOPE POLI 
        $.ajax({
          type: "GET",
          cache: false,
          url: url+"api_vklaim/rujukan_nokartu/"+nokartu,
           success: function(data){
             var obj=JSON.parse(data);
             console.log(data);
              if (obj.metaData!= null) {
                if(obj.metaData.code==200){
                   
                   $('#sep_norujuk').val(obj.response.rujukan.noKunjungan);
                  
                }else{
                  $('#sep_norujuk').val(0);    
                   $('#rujukan_help').html('rujukan Tidak ditemukan');
                    $('#sep_btn').hide();
                }
              }
              else{
                $('#sep_norujuk').val(0);    
                 $('#rujukan_help').html('rujukan Tidak ditemukan');
                 $('#sep_btn').hide();
              }
           }
         });
}


function deleteSEP(noSEP){

  var sep=noSEP;

  if(confirm('Data SEP akan dihapus, Anda yakin?')){

    console.log('PROSES HAPUS SEP');
      $.ajax({
          type: "GET",
          cache: false,
          url: url+"api_vklaim/sep_delete/"+sep,
           success: function(data){
             var obj=JSON.parse(data);

              if (obj.metaData!= null) {
                if(obj.metaData.code==200){
                   
                   alert('SEP '+obj.response+', Berhasil Dihapus');
                   window.location.replace(url+'layanan_daftar/vsep');
                  
                }else{
                  alert('SEP '+noSep+', Gagal Dihapus');
                }
              }
              else{
                alert('SEP '+noSep+', Gagal Dihapus');
              }
           }
         });
  }
}



function CariKartu(noKartu,tglSep){

  var nokartu=noKartu;
  var tgl=tglSep;

   

    console.log('PROSES Mencari NoKartu '+nokartu);
      $.ajax({
          type: "GET",
          cache: false,
          url: url+"api_vklaim/peserta_no/"+nokartu+"/"+tgl,
           success: function(data){
             var obj=JSON.parse(data);

              if (obj.metaData!= null) {
                if(obj.metaData.code==200){
                   
                   alert('Data Peserta Ditemukan');
                   console.log(obj);
                   $('#sep_nama').html(obj.response.peserta.nama);
                   $('#sep_nokartu').html(obj.response.peserta.noKartu);
                   $('#sep_nik').html(obj.response.peserta.nik);
                   
                   $('#sep_tgllahir').html(obj.response.peserta.tglLahir);
                   $('#sep_jenispeserta').html(obj.response.peserta.jenisPeserta.keterangan);
                   $('#sep_hakkelas').html(obj.response.peserta.hakKelas.keterangan);
                   $('#sep_status').html('<span class="text-info">'+obj.response.peserta.statusPeserta.keterangan+'</span>');
                   $('#nik').val(obj.response.peserta.nik);
                   $('#norm').val(obj.response.peserta.mr.noMR);
                   $('#telp').val(obj.response.peserta.mr.noTelepon);
                   $('#rawatkelas').val(obj.response.peserta.hakKelas.kode);
                    if(obj.response.peserta.statusPeserta.kode==0){
                      $('#btn-simpan-sep').prop( "disabled", false );
                    }else{
                       $('#btn-simpan-sep').prop( "disabled", true );
                    }
                }else{
                  alert('Data Peserta Tidak Ditemukan');
                }
              }
              else{
                alert('Data Peserta Tidak Ditemukan');
              }
           }
         });
 
}

function CariKartubyNik(noKartu,tglSep){

  var nokartu=noKartu;
  var tgl=tglSep;

   

    console.log('PROSES Mencari NoKartu '+nokartu);
      $.ajax({
          type: "GET",
          cache: false,
          url: url+"api_vklaim/peserta_nik/"+nokartu+"/"+tgl,
           success: function(data){
             var obj=JSON.parse(data);

              if (obj.metaData!= null) {
                if(obj.metaData.code==200){
                   
                   alert('Data Peserta Ditemukan');
                   console.log(obj);
                   $('#sep_nama').html(obj.response.peserta.nama);
                   $('#sep_nokartu').html(obj.response.peserta.noKartu);
                   $('#sep_nik').html(obj.response.peserta.nik);
                   
                   $('#sep_tgllahir').html(obj.response.peserta.tglLahir);
                   $('#sep_jenispeserta').html(obj.response.peserta.jenisPeserta.keterangan);
                   $('#sep_hakkelas').html(obj.response.peserta.hakKelas.keterangan);
                   $('#sep_status').html('<span class="text-info">'+obj.response.peserta.statusPeserta.keterangan+'</span>');
                   $('#nokartu').val(obj.response.peserta.noKartu);
                   $('#norm').val(obj.response.peserta.mr.noMR);
                   $('#telp').val(obj.response.peserta.mr.noTelepon);
                   $('#rawatkelas').val(obj.response.peserta.hakKelas.kode);
                    if(obj.response.peserta.statusPeserta.kode==0){
                      $('#btn-simpan-sep').prop( "disabled", false );
                    }else{
                       $('#btn-simpan-sep').prop( "disabled", true );
                    }
                }else{
                  alert('Data Peserta Tidak Ditemukan');
                }
              }
              else{
                alert('Data Peserta Tidak Ditemukan');
              }
           }
         });
 
}
function CariKartubyRujukan(noRujuk){

  var norujuk=noRujuk;
  

   

    console.log('PROSES Mencari NoRujuk '+norujuk);
      $.ajax({
          type: "GET",
          cache: false,
          url: url+"api_vklaim/rujukan/"+norujuk,
           success: function(data){
             var obj=JSON.parse(data);

              if (obj.metaData!= null) {
                if(obj.metaData.code==200){
                   
                   alert('Data Peserta Ditemukan');
                   console.log(obj);
                   $('#sep_nama').html(obj.response.rujukan.peserta.nama);
                   $('#sep_nokartu').html(obj.response.rujukan.peserta.noKartu);
                   $('#sep_nik').html(obj.response.rujukan.peserta.nik);
                   
                   $('#sep_tgllahir').html(obj.response.rujukan.peserta.tglLahir);
                   $('#sep_jenispeserta').html(obj.response.rujukan.peserta.jenisPeserta.keterangan);
                   $('#sep_hakkelas').html(obj.response.rujukan.peserta.hakKelas.keterangan);
                   $('#sep_status').html('<span class="text-info">'+obj.response.rujukan.peserta.statusPeserta.keterangan+'</span>');
                   $('#nokartu').val(obj.response.rujukan.peserta.noKartu);
                   $('#nik').val(obj.response.rujukan.peserta.nik);
                   $('#norm').val(obj.response.rujukan.peserta.mr.noMR);
                   $('#telp').val(obj.response.rujukan.peserta.mr.noTelepon);
                   $('#rawatkelas').val(obj.response.rujukan.peserta.hakKelas.kode);
                   $('#sep_diag').val(obj.response.rujukan.diagnosa.kode+' - '+obj.response.rujukan.diagnosa.nama);
                   $('#sep_diag_val').val(obj.response.rujukan.diagnosa.kode);
                   $('#ppkrujuk').val(obj.response.rujukan.provPerujuk.nama);
                   $('#ppkrujuk_val').val(obj.response.rujukan.provPerujuk.kode);
                   $('#pelayanan').val(obj.response.rujukan.pelayanan.kode);
                   $('#poli').val(obj.response.rujukan.poliRujukan.kode);
                   if(obj.response.rujukan.keluhan!='')
                   $('#catatan').val('Keluhan Rujukan - '+obj.response.rujukan.keluhan);

                    if(obj.response.rujukan.peserta.statusPeserta.kode==0){
                      $('#btn-simpan-sep').prop( "disabled", false );
                    }else{
                       $('#btn-simpan-sep').prop( "disabled", true );
                    }
                }else{
                  alert('Data Peserta Tidak Ditemukan');
                }
              }
              else{
                alert('Data Peserta Tidak Ditemukan');
              }
           }
         });
 
}
function CariKartubyRujukanpcare(noRujuk){

  var norujuk=noRujuk;
  

   

    console.log('PROSES Mencari NoRujuk '+norujuk);
      $.ajax({
          type: "GET",
          cache: false,
          url: url+"api_vklaim/rujukanpcare/"+norujuk,
           success: function(data){
             var obj=JSON.parse(data);

              if (obj.metaData!= null) {
                if(obj.metaData.code==200){
                   
                   alert('Data Peserta Ditemukan');
                   console.log(obj);
                   $('#sep_nama').html(obj.response.rujukan.peserta.nama);
                   $('#sep_nokartu').html(obj.response.rujukan.peserta.noKartu);
                   $('#sep_nik').html(obj.response.rujukan.peserta.nik);
                   
                   $('#sep_tgllahir').html(obj.response.rujukan.peserta.tglLahir);
                   $('#sep_jenispeserta').html(obj.response.rujukan.peserta.jenisPeserta.keterangan);
                   $('#sep_hakkelas').html(obj.response.rujukan.peserta.hakKelas.keterangan);
                   $('#sep_status').html('<span class="text-info">'+obj.response.rujukan.peserta.statusPeserta.keterangan+'</span>');
                   $('#nokartu').val(obj.response.rujukan.peserta.noKartu);
                   $('#nik').val(obj.response.rujukan.peserta.nik);
                   $('#norm').val(obj.response.rujukan.peserta.mr.noMR);
                   $('#telp').val(obj.response.rujukan.peserta.mr.noTelepon);
                   $('#rawatkelas').val(obj.response.rujukan.peserta.hakKelas.kode);
                   $('#sep_diag').val(obj.response.rujukan.diagnosa.kode+' - '+obj.response.rujukan.diagnosa.nama);
                   $('#sep_diag_val').val(obj.response.rujukan.diagnosa.kode);
                   $('#ppkrujuk').val(obj.response.rujukan.provPerujuk.nama);
                   $('#ppkrujuk_val').val(obj.response.rujukan.provPerujuk.kode);
                   $('#pelayanan').val(obj.response.rujukan.pelayanan.kode);
                   $('#poli').val(obj.response.rujukan.poliRujukan.kode);
                   if(obj.response.rujukan.keluhan!='')
                   $('#catatan').val('Keluhan Rujukan - '+obj.response.rujukan.keluhan);

                    if(obj.response.rujukan.peserta.statusPeserta.kode==0){
                      $('#btn-simpan-sep').prop( "disabled", false );
                    }else{
                       $('#btn-simpan-sep').prop( "disabled", true );
                    }
                }else{
                  alert('Data Peserta Tidak Ditemukan');
                }
              }
              else{
                alert('Data Peserta Tidak Ditemukan');
              }
           }
         });
 
}

$(function() {
            $( "#sep_diag" ).autocomplete({ 
              source: url+"master_data/get_icd",  
              minLength:2, 
              select: function( event, ui ) {
                

                
                //console.log(selectedObj.value);
                
                
                $('#sep_diag').val(ui.item.label);
                $("#sep_diag_val").val(ui.item.value);
                
        
              
           return false;
            },
             
            });   

             $( ".tglsep" ).datepicker({ dateFormat: 'yy-mm-dd',
              changeMonth: true,
              changeYear: true,
              yearRange:"-100:+0"
            }); 



            $( "#ppkrujuk" ).autocomplete({ 
              source: url+"api_vklaim/findfaskes",  
              minLength:3, 
              select: function( event, ui ) {
                

                
                //console.log(selectedObj.value);
                
                
                $('#ppkrujuk').val(ui.item.label);
                $("#ppkrujuk_val").val(ui.item.value);
                
        
              
           return false;
            },
             
            });  
})



function echeck(){
  $.get( url+"api_eklaim/checkapi", function( data ) {
        $('#api-result').html('<strong> API RESULT => </strong>'+data);

    });

}


/*MULTIPLE AUTOCOMPLETE*/
 $(function() {
     function split( val ) {
    return val.split( /,\s*/ );
  }

  function extractLast( term ) {
     return split( term ).pop();
   }

   $( "#search" )
        .autocomplete({
             minLength: 0,
             source: function( request, response ) {
                 response( $.ui.autocomplete.filter(
                     items, extractLast( request.term ) ) );
             },
             focus: function() {
                 return false;
             },
            select: function( event, ui ) {
                var terms = split( this.value );
                terms.pop();
                terms.push( ui.item.value );
                terms.push( "" );
                this.value = terms.join( ", " );
                return false;
            }
        });

 });     


/*KASIR RETURN OBAT */

$(function(){

      dialogreturnrajal = $( "#form-pengembalian" ).dialog({
      autoOpen: false,
      height: 600,
      width: 800,
      modal: true
      });
    
    
     

  $( "#btn-return-obat-rajal" ).on( "click", function() {
      dialogreturnrajal.dialog('open');

  });
  
});

function updatehargaobatreturn(a,harga){
  var jumlah=a.value;
  var note=$(a).closest('tr').find('td.harga-obat').find('small.total-harga-obat');
  total=jumlah*harga;
  note.html(total.toFixed(2));

  //update TOTAL PENGEMBALIAN
  
  var tabel=document.getElementById('tabel-pengembalian');
  var sum=0;
  var item=tabel.getElementsByClassName('total-harga-obat');
  for(var i=0; i<item.length; i++){
        sum += Number(item[i].innerHTML);
       /* console.log(Number(item[i].innerHTML));*/
        }
  $('.total-tagihan-return').html('Rp. '+sum);
  /*console.log('total '+ sum);*/


  //form validation max qty
  var maxqty=$(a).closest('td').find('.qty-max').val();

  var text=$(a).closest('td').find('span.text-warning');

  if(parseInt(jumlah) > parseInt(maxqty)){
    text.html('Jumlah Melebihi Resep');
   text.show();
   $('#btn-submit-return').prop('disabled', true);
  
  }else{
    text.html('');
    text.hide();
    $('#btn-submit-return').prop('disabled', false);
    
  }
}

function validasitransaksiapotik(){
  var norm=$('input[name="no_rm').val();
  if(norm.length!=6){

   alert('NoRM harus 6 digit');
    return false;
  }
  else{
      return confirm('Apakah Data Sudah Benar?');
  }
  
  
}
$(function(){
 dialogcarinama = $( "#fcariNama" ).dialog({
      autoOpen: false,
      height: 300,
      width: 500,
      modal: true
      });
 $( "#btn-cariNama" ).on( "click", function() {
      dialogcarinama.dialog('open');

  });
$( ".btn-ok-nama" ).on( "click", function() {
      dialogcarinama.dialog('close');

  });


   $( ".namapasien" ).autocomplete({ 
              source: url+"layanan_poli/get_pasien",  
              minLength:3, 
              select: function( event, ui ) {
                

                
                //console.log(selectedObj.value);
                
                
                $('.namapasien').val(ui.item.label);
                $(".inputnorm").val(ui.item.value);
                
        
              
           return false;
            },
             
            });   
  
});




function getkunjunganonline(){
  var idkunjungan=$('#no_online').val();
  if(idkunjungan.length==10){
    $.ajax({
    type: "POST",
    url: url+"layanan_daftar/get_kunjungan_online",
    data:'id='+idkunjungan,
    success: function(data){

      var obj=JSON.parse(data)
      console.log(obj);
       if(obj.status){
         $('#online_status').val('0');
          if(obj.kunjungan.status==1){
            alert('Data sudah Diproses');  
          }else{
             alert('Data ditemukan');  
             $('#poli option[value='+obj.kunjungan.layanan+']').prop('selected', true) ;
             $('#cara_bayar option[value='+obj.kunjungan.cara_bayar+']').prop('selected', true) ;
             checkPeserta(obj.kunjungan.cara_bayar);
             $('#no_peserta').val(obj.kunjungan.nomor_anggota);

              if(obj.kunjungan.status_kunjungan==1){
                $('#status_pasien').val('Baru');
                $('#s_pasien').val(1);
                $('#id_pasien').val(obj.kunjungan.data_pasien.id);
                  setdropdown(obj.kunjungan.data_pasien.id_provinsi,obj.kunjungan.data_pasien.id_kota,obj.kunjungan.data_pasien.id_kecamatan);
                setTimeout(function() {
                  $("#provinsi").val(obj.kunjungan.data_pasien.id_provinsi);
                  $("#kota").val(obj.kunjungan.data_pasien.id_kota);
                  $("#kec").val(obj.kunjungan.data_pasien.id_kecamatan);
                  $("#kel").val(obj.kunjungan.data_pasien.id_kelurahan);
                 
                }, 1000);
             
                $('#nama_pasien').val(obj.kunjungan.data_pasien.nama);
                $('#title').val(obj.kunjungan.data_pasien.title);
                $('#nama_ayah').val(obj.kunjungan.data_pasien.nama_ortu);
                $("input[type=radio][name=kelamin][value='"+obj.kunjungan.data_pasien.jenis_kelamin+"']").prop("checked",true);
                $("#status_kawin").val(obj.kunjungan.data_pasien.marital);
                $("#pendidikan").val(obj.kunjungan.data_pasien.pendidikan);
                $("#agama").val(obj.kunjungan.data_pasien.agama);
                $("#tempat_lahir").val(obj.kunjungan.data_pasien.tempat_lahir);
                var tgl_lahir=obj.kunjungan.data_pasien.tanggal_lahir.split("-");

                $("#tgl_lahir").val(tgl_lahir[2]+'-'+tgl_lahir[1]+'-'+tgl_lahir[0]);
                $("#alamat_sekarang").val(obj.kunjungan.data_pasien.alamat);
                $("#alamat_ktp").val(obj.kunjungan.data_pasien.alamat);
                
                $("#no_ktp").val(obj.kunjungan.data_pasien.no_ktp);
                $("#no_telp").val(obj.kunjungan.data_pasien.no_telp);
                $("#pekerjaan").val(obj.kunjungan.data_pasien.pekerjaan);
                $("#penanggung_jawab").val(obj.kunjungan.data_pasien.nama_penanggung);
                $("#alamat_penanggung").val(obj.kunjungan.data_pasien.alamat_penanggung);
                $("#hubungan_pasien").val(obj.kunjungan.data_pasien.hubungan_penanggung);
                $("#telp_penanggung").val(obj.kunjungan.data_pasien.no_penanggung);

              }else{
                $('#status_pasien').val('Lama');
                $('#no_rm').val(obj.kunjungan.no_rm);
                $('#s_pasien').val(2);
                check_user_rm(obj.kunjungan.no_rm);

              }

          }
          
       }else{
         alert('Data Tidak Ditemukan, silahkan coba tombol cari Online');
       }
      
    }
    });
  }else{
    alert('no pendaftaran tidak sesuai');
  }
}

function getkunjunganonlineapi(){

  var idkunjungan=$('#no_online').val();
  if(idkunjungan.length==10){
    $.ajax({
    type: "POST",
    url: url+"layanan_daftar/get_kunjungan_online_api",
    data:'id='+idkunjungan,
    success: function(data){

      var obj=JSON.parse(data)
      console.log(obj);
       if(obj.status){
          alert('Data Ditemukan');
          $('#online_status').val('1');
            $('#poli option[value='+obj.kunjungan.layanan+']').prop('selected', true) ;
           $('#cara_bayar option[value='+obj.kunjungan.cara_bayar+']').prop('selected', true) ;
            checkPeserta(obj.kunjungan.cara_bayar);
           $('#no_peserta').val(obj.kunjungan.nomor_anggota);

          if(obj.kunjungan.status_kunjungan==1){
            $('#status_pasien').val('Baru');
            $('#s_pasien').val(1);
            $('#id_pasien').val(obj.kunjungan.data_pasien.id);


                //console.log(obj.kunjungan.data_pasien.id_provinsi);
                
                /*console.log(obj);
                console.log(obj['no_rm']);*/
               setdropdown(obj.kunjungan.data_pasien.id_provinsi,obj.kunjungan.data_pasien.id_kota,obj.kunjungan.data_pasien.id_kecamatan);
                setTimeout(function() {
                  $("#provinsi").val(obj.kunjungan.data_pasien.id_provinsi);
                  $("#kota").val(obj.kunjungan.data_pasien.id_kota);
                  $("#kec").val(obj.kunjungan.data_pasien.id_kecamatan);
                  $("#kel").val(obj.kunjungan.data_pasien.id_kelurahan);
                 
                }, 1000);
             
                $('#nama_pasien').val(obj.kunjungan.data_pasien.nama);
                $('#title').val(obj.kunjungan.data_pasien.title);
                $('#nama_ayah').val(obj.kunjungan.data_pasien.nama_ortu);
                $("input[type=radio][name=kelamin][value='"+obj.kunjungan.data_pasien.jenis_kelamin+"']").prop("checked",true);
                $("#status_kawin").val(obj.kunjungan.data_pasien.marital);
                $("#pendidikan").val(obj.kunjungan.data_pasien.pendidikan);
                $("#agama").val(obj.kunjungan.data_pasien.agama);
                $("#tempat_lahir").val(obj.kunjungan.data_pasien.tempat_lahir);
                var tgl_lahir=obj.kunjungan.data_pasien.tanggal_lahir.split("-");

                $("#tgl_lahir").val(tgl_lahir[2]+'-'+tgl_lahir[1]+'-'+tgl_lahir[0]);
                $("#alamat_sekarang").val(obj.kunjungan.data_pasien.alamat);
                $("#alamat_ktp").val(obj.kunjungan.data_pasien.alamat);
                
                $("#no_ktp").val(obj.kunjungan.data_pasien.no_ktp);
                $("#no_telp").val(obj.kunjungan.data_pasien.no_telp);
                $("#pekerjaan").val(obj.kunjungan.data_pasien.pekerjaan);
                $("#penanggung_jawab").val(obj.kunjungan.data_pasien.nama_penanggung);
                $("#alamat_penanggung").val(obj.kunjungan.data_pasien.alamat_penanggung);
                $("#hubungan_pasien").val(obj.kunjungan.data_pasien.hubungan_penanggung);
                $("#telp_penanggung").val(obj.kunjungan.data_pasien.no_penanggung);
               

          }else{
            $('#status_pasien').val('Lama');
            $('#no_rm').val(obj.kunjungan.no_rm);
              $('#s_pasien').val(2);
            check_user_rm(obj.kunjungan.no_rm);

          }

         
       }else{
         alert('Data Tidak Ditemukan, silahkan coba tombol cari Online');
       }
      
    }
    });
  }else{
    alert('no pendaftaran tidak sesuai');
  }
}


/*KODE ICD RM*/
$(function() {
            $( "#icd_val" ).autocomplete({ 
              source: url+"master_data/get_icd",  
              minLength:2, 
              select: function( event, ui ) {                
                //console.log(selectedObj.value);                
                $('#icd_val').val(ui.item.label);
                $("#kode_icd").val(ui.item.value);               
              return false;
              },             
            });   

             $( "#icd_val_s" ).autocomplete({ 
              source: url+"master_data/get_icd",  
              minLength:2, 
              select: function( event, ui ) {                
                //console.log(selectedObj.value);    

                $('#icd_val_s').val('');
                var newrow;
                if($('#tabel-icd-s tr').length==0){
                  newrow=$('<tr>').append($('<td class="col-md-6">').append('<input type="text" class="prim-icd form-control input-sm" name="icd_r[]" value="'+ui.item.label+'" readonly>'+
                                                              '<input type="hidden" class=" prim-icd-code form-control input-sm" name="icd_s_kode[]" value="'+ui.item.value+'" >'))
                                    .append($('<td>').append('<button class="btn btn-default" onclick="$(this).closest(\'tr\').remove();"><i class="fa fa-remove" ></i></button>'))
                                    .append($('<td>').append('<span class="text-info">Primer</span>'));
                }
                else{
                   newrow=$('<tr>').append($('<td class="col-md-6">').append('<input type="text" class="context-icd form-control input-sm" name="icd_r[]" value="'+ui.item.label+'" readonly>'+
                                                              '<input type="hidden" class="context-icd-code form-control input-sm" name="icd_s_kode[]" value="'+ui.item.value+'" >'))
                                    .append($('<td>').append('<button class="btn btn-default" onclick="$(this).closest(\'tr\').remove();"><i class="fa fa-remove" ></i></button>'))
                                    .append($('<td>').append('<span class="text-warning">Sekunder</span>'));
                }
                $("#tabel-icd-s").append(newrow);

              return false;
              },             
            });   



             $( "#procedure" ).autocomplete({ 
              source: url+"master_data/get_procedure",  
              minLength:2, 
              select: function( event, ui ) {                
                //console.log(selectedObj.value);    

                $('#procedure').val('');
                
                var pro='<input type="hidden" class="form-control input-sm" name="pro_kode[]" value="'+ui.item.value+'" >';
           
                
                var newrow=$('<tr>').append($('<td class="col-md-6">').append('<input type="text" class="form-control input-sm" name="pro_r[]" value="'+ui.item.label+'" readonly>'+pro))
                                    .append($('<td>').append('<button class="btn btn-default" onclick="$(this).closest(\'tr\').remove();"><i class="fa fa-remove" ></i></button>'));

                $("#tabel-procedure-s").append(newrow);

              return false;
              },             
            });   


             jQuery.datetimepicker.setLocale('id');
             //init datetime picker form rm
             $('.tgl_rm').datetimepicker({
              format:'Y-m-d H:i:s',
              formatTime:'H:i:s',
             });


            $("#btn-save-eklaim").on('click',function(){
               var procedure = $("input[name='pro_kode[]']")
                    .map(function(){
                      return $(this).val();}
                      ).get();
                
                var icd = $("input[name='icd_s_kode[]']")
                      .map(function(){
                        return $(this).val();}
                        ).get();
                var idpoli=$('#id_poli').val();

                //data pasien
                var nokartu=$('#nokartu').val();
                var namapasien=$('#namapasien').val();
                var norm=$('#norm').val();
                var nosep=$('#nosep').val();
                var tlahir=$('#tlahir').val();
                var kelamin=$('#kelamin').val();
                var jenkel=1;
                if(kelamin=='P'){
                  jenkel=2;
                }else{
                  jenkel=1;
                }

                //data klaim
                var tmasuk=$('#masuk').val();
                var tpulang=$('#pulang').val();
                var jenis_rawat=2;
                var diagnosa=icd;
                 
                var proc=procedure;
                var t1=$('#t1').val();
                var t2=$('#t2').val();
                var t3=$('#t3').val();
                var t4=$('#t4').val();
                var t5=$('#t5').val();
                var t6=$('#t6').val();
                var t7=$('#t7').val();
                var t8=$('#t8').val();
                var t9=$('#t9').val();
                var t10=$('#t10').val();
                var t11=$('#t11').val();
                var t12=$('#t12').val();
                var t13=$('#t13').val();
                var t14=$('#t14').val();
                var t15=$('#t15').val();
                var t16=$('#t16').val();
                var nama_dokter=$('#dpjp').val();
                var cara_pulang=$('#cara_pulang').val(); //2pulang 6Ke Rumah Sakit Lain 7MENINGGAL


                /*API CARA pulang
                //Cara pulang didefinisikan sebagai berikut:
                1 = Atas persetujuan dokter 2 = Dirujuk 3 = Atas permintaan sendiri 4 = Meninggal 5 = Lain-lain
                  */
                var cpulang=1;
                if(cara_pulang==2){
                  cpulang=1;
                }else if(cara_pulang==6){
                  cpulang=2;
                }else if(cara_pulang==7){
                  cpulang=4;
                }

                 

                  /*UPDATE KUNJUNGAN*/
                   $.ajax({
                      type : "POST",
                      url  : url+"layanan_rm/update_diagnosa_poli",
                      dataType : "JSON",
                      data : {id:idpoli, icd_s:icd,  procedure:procedure},
                      success: function(data){
                          
                           
                          console.log(data.pesan);
                      }
                  });


                    /*UPDATE INSERT EKLAIM*/
                   $.ajax({
                      type : "POST",
                      url  : url+"api_eklaim/newclaim",
                      dataType : "JSON",
                      data : {nokartu:nokartu, nosep:nosep, norm:norm, namapasien:namapasien, tgl_lahir:tlahir, kelamin:jenkel},
                      success: function(data){
                          
                          if(data.metadata.code==400)
                            alert(data.metadata.message);

                           if(data.metadata.code==200){
                                // UPDATE CLAIM
                                $.ajax({
                                  type : "POST",
                                  url  : url+"api_eklaim/updateklaim",
                                  dataType : "JSON",
                                  data : {nosep:nosep, nokartu:nokartu, tmasuk:tmasuk, tpulang:tpulang, 
                                            jenis_rawat:jenis_rawat,
                                           diagnosa:diagnosa,
                                            
                                           proc:proc,
                                           t1:t1,
                                           t2:t2,
                                           t3:t3,
                                           t4:t4,
                                           t5:t5,
                                           t6:t6,
                                           t7:t7,
                                           t8:t8,
                                           t9:t9,
                                           t10:t10,
                                           t11:t11,
                                           t12:t12,
                                           t13:t13,
                                           t14:t14,
                                           t15:t15,
                                           t16:t16,
                                           cara_pulang:cpulang,
                                          nama_dokter:nama_dokter},
                                  success: function(data){
                                      
                                      if(data.metadata.code==200){
                                        alert('EKLAIM BERHASIL DIUPDATE');
                                        location.reload();
                                      }else{
                                         alert(data.metadata.message);
                                      }
                                  }
                              });
                           }
                      }
                  });
                  console.log(procedure);    
                  console.log(icd);    
                  console.log(icd_s);    
                  console.log(idpoli);    

            });

            $("#btn-update-eklaim").on('click',function(){
               var procedure = $("input[name='pro_kode[]']")
                    .map(function(){
                      return $(this).val();}
                      ).get();
                
                var icd = $("input[name='icd_s_kode[]']")
                      .map(function(){
                        return $(this).val();}
                        ).get();
                var idpoli=$('#id_poli').val();

                //data pasien
                var nokartu=$('#nokartu').val();
                var namapasien=$('#namapasien').val();
                var norm=$('#norm').val();
                var nosep=$('#nosep').val();
                var tlahir=$('#tlahir').val();
                var kelamin=$('#kelamin').val();
                var jenkel=1;
                if(kelamin=='P'){
                  jenkel=2;
                }else{
                  jenkel=1;
                }

                //data klaim
                var tmasuk=$('#masuk').val();
                var tpulang=$('#pulang').val();
                var jenis_rawat=2;
                var diagnosa=icd;
                
                var proc=procedure;
                var t1=$('#t1').val();
                var t2=$('#t2').val();
                var t3=$('#t3').val();
                var t4=$('#t4').val();
                var t5=$('#t5').val();
                var t6=$('#t6').val();
                var t7=$('#t7').val();
                var t8=$('#t8').val();
                var t9=$('#t9').val();
                var t10=$('#t10').val();
                var t11=$('#t11').val();
                var t12=$('#t12').val();
                var t13=$('#t13').val();
                var t14=$('#t14').val();
                var t15=$('#t15').val();
                var t16=$('#t16').val();
                var nama_dokter=$('#dpjp').val();
                var cara_pulang=$('#cara_pulang').val(); //2pulang 6Ke Rumah Sakit Lain 7MENINGGAL


                /*API CARA pulang
                //Cara pulang didefinisikan sebagai berikut:
                1 = Atas persetujuan dokter 2 = Dirujuk 3 = Atas permintaan sendiri 4 = Meninggal 5 = Lain-lain
                  */
                var cpulang=1;
                if(cara_pulang==2){
                  cpulang=1;
                }else if(cara_pulang==6){
                  cpulang=2;
                }else if(cara_pulang==7){
                  cpulang=4;
                }

                 

                  /*UPDATE KUNJUNGAN*/
                   $.ajax({
                      type : "POST",
                      url  : url+"layanan_rm/update_diagnosa_poli",
                      dataType : "JSON",
                      data : {id:idpoli, icd_s:icd, procedure:procedure},
                      success: function(data){
                          
                          
                          console.log(data);
                      }
                  });


                    
                   // UPDATE CLAIM
                                $.ajax({
                                  type : "POST",
                                  url  : url+"api_eklaim/updateklaim",
                                  dataType : "JSON",
                                  data : {nosep:nosep, nokartu:nokartu, tmasuk:tmasuk, tpulang:tpulang, 
                                            jenis_rawat:jenis_rawat,
                                           diagnosa:diagnosa,
                                            
                                           proc:proc,
                                           t1:t1,
                                           t2:t2,
                                           t3:t3,
                                           t4:t4,
                                           t5:t5,
                                           t6:t6,
                                           t7:t7,
                                           t8:t8,
                                           t9:t9,
                                           t10:t10,
                                           t11:t11,
                                           t12:t12,
                                           t13:t13,
                                           t14:t14,
                                           t15:t15,
                                           t16:t16,
                                           cara_pulang:cpulang,
                                          nama_dokter:nama_dokter},
                                  success: function(data){
                                      
                                      console.log(data);
                                      if(data.metadata.code==200){
                                      
                                        alert('EKLAIM BERHASIL DIUPDATE');

                                      }else{
                                         alert(data.metadata.message);
                                      }
                                  }
                              });
                  console.log(procedure);    
                  console.log(icd);    
                  
                  console.log(idpoli);    

            });

          $("#btn-hapus-eklaim").on('click',function(){
            var nosep=$('#nosep').val();
             //klaim final
             $.ajax({
                                  type : "GET",
                                  url  : url+"api_eklaim/delete_claim",
                                  dataType : "JSON",
                                  data : {sep:nosep},
                                  success: function(data){
                                      
                                      console.log(data);
                                      if(data.metadata.code==200){
                                        alert('Klaim Dihapus '+data.metadata.message);
                                        location.reload();
                                      }else{
                                        alert('Error '+data.metadata.message);
                                      }
                                  }
                              });
          });
          $("#btn-final-eklaim").on('click',function(){
            var nosep=$('#nosep').val();
            //klaim final
             $.ajax({
                                  type : "POST",
                                  url  : url+"api_eklaim/claimfinal",
                                  dataType : "JSON",
                                  data : {nosep:nosep},
                                  success: function(data){
                                      
                                      console.log(data);
                                      if(data.metadata.code==200){
                                        alert('FINAL EKLAIM BERHASIL '+data.metadata.message);
                                      }else{
                                        alert('Error '+data.metadata.message);
                                      }
                                  }
                              });


          });

           $("#btn-save-eklaim-ri").on('click',function(){
               var procedure = $("input[name='pro_kode[]']")
                    .map(function(){
                      return $(this).val();}
                      ).get();
                
                var icd = $("input[name='icd_s_kode[]']")
                      .map(function(){
                        return $(this).val();}
                        ).get();
                var idpoli=$('#id_poli').val();

                //data pasien
                var nokartu=$('#nokartu').val();
                var namapasien=$('#namapasien').val();
                var norm=$('#norm').val();
                var nosep=$('#nosep').val();
                var tlahir=$('#tlahir').val();
                var kelamin=$('#kelamin').val();
                var jenkel=1;
                if(kelamin=='P'){
                  jenkel=2;
                }else{
                  jenkel=1;
                }

                //data klaim
                var tmasuk=$('#masuk').val();
                var tpulang=$('#pulang').val();
                var jenis_rawat=1;
                var diagnosa=icd;
                var kelas_rawat=$('#kelas-rawat').val();
                var proc=procedure;
                var t1=$('#t1').val();
                var t2=$('#t2').val();
                var t3=$('#t3').val();
                var t4=$('#t4').val();
                var t5=$('#t5').val();
                var t6=$('#t6').val();
                var t7=$('#t7').val();
                var t8=$('#t8').val();
                var t9=$('#t9').val();
                var t10=$('#t10').val();
                var t11=$('#t11').val();
                var t12=$('#t12').val();
                var t13=$('#t13').val();
                var t14=$('#t14').val();
                var t15=$('#t15').val();
                var t16=$('#t16').val();
                var nama_dokter=$('#dpjp').val();
                var cara_pulang=$('#cara_pulang').val(); //2pulang 6Ke Rumah Sakit Lain 7MENINGGAL


                /*API CARA pulang
                //Cara pulang didefinisikan sebagai berikut:
                1 = Atas persetujuan dokter 2 = Dirujuk 3 = Atas permintaan sendiri 4 = Meninggal 5 = Lain-lain
                  */
                var cpulang=1;
                if(cara_pulang==2){
                  cpulang=1;
                }else if(cara_pulang==6){
                  cpulang=2;
                }else if(cara_pulang==7){
                  cpulang=4;
                }

                 

                  /*UPDATE KUNJUNGAN*/
                   $.ajax({
                      type : "POST",
                      url  : url+"layanan_rm/update_diagnosa_ranap",
                      dataType : "JSON",
                      data : {id:idpoli, icd_s:icd,  procedure:procedure},
                      success: function(data){
                          
                           
                          console.log(data.pesan);
                      }
                  });


                    /*UPDATE INSERT EKLAIM*/
                   $.ajax({
                      type : "POST",
                      url  : url+"api_eklaim/newclaim",
                      dataType : "JSON",
                      data : {nokartu:nokartu, nosep:nosep, norm:norm, namapasien:namapasien, tgl_lahir:tlahir, kelamin:jenkel},
                      success: function(data){
                          
                          if(data.metadata.code==400)
                            alert(data.metadata.message);

                           if(data.metadata.code==200){
                                // UPDATE CLAIM
                                $.ajax({
                                  type : "POST",
                                  url  : url+"api_eklaim/updateklaim",
                                  dataType : "JSON",
                                  data : {nosep:nosep, nokartu:nokartu, tmasuk:tmasuk, tpulang:tpulang, 
                                            jenis_rawat:jenis_rawat,
                                           diagnosa:diagnosa,
                                           kelas_rawat:kelas_rawat,
                                           proc:proc,
                                           t1:t1,
                                           t2:t2,
                                           t3:t3,
                                           t4:t4,
                                           t5:t5,
                                           t6:t6,
                                           t7:t7,
                                           t8:t8,
                                           t9:t9,
                                           t10:t10,
                                           t11:t11,
                                           t12:t12,
                                           t13:t13,
                                           t14:t14,
                                           t15:t15,
                                           t16:t16,
                                           cara_pulang:cpulang,
                                          nama_dokter:nama_dokter},
                                  success: function(data){
                                      
                                      if(data.metadata.code==200){
                                        alert('EKLAIM BERHASIL DIUPDATE');
                                        location.reload();
                                      }else{
                                         alert(data.metadata.message);
                                      }
                                  }
                              });
                           }
                      }
                  });
                  console.log(procedure);    
                  console.log(icd);    
                  console.log(icd_s);    
                  console.log(idpoli);    

            });

          $("#btn-update-eklaim-ri").on('click',function(){
               var procedure = $("input[name='pro_kode[]']")
                    .map(function(){
                      return $(this).val();}
                      ).get();
                
                var icd = $("input[name='icd_s_kode[]']")
                      .map(function(){
                        return $(this).val();}
                        ).get();
                var idpoli=$('#id_poli').val();

                //data pasien
                var nokartu=$('#nokartu').val();
                var namapasien=$('#namapasien').val();
                var norm=$('#norm').val();
                var nosep=$('#nosep').val();
                var tlahir=$('#tlahir').val();
                var kelamin=$('#kelamin').val();
                var jenkel=1;
                if(kelamin=='P'){
                  jenkel=2;
                }else{
                  jenkel=1;
                }

                //data klaim
                var tmasuk=$('#masuk').val();
                var tpulang=$('#pulang').val();
                var jenis_rawat=1;
                var diagnosa=icd;
                
                var proc=procedure;
                var t1=$('#t1').val();
                var t2=$('#t2').val();
                var t3=$('#t3').val();
                var t4=$('#t4').val();
                var t5=$('#t5').val();
                var t6=$('#t6').val();
                var t7=$('#t7').val();
                var t8=$('#t8').val();
                var t9=$('#t9').val();
                var t10=$('#t10').val();
                var t11=$('#t11').val();
                var t12=$('#t12').val();
                var t13=$('#t13').val();
                var t14=$('#t14').val();
                var t15=$('#t15').val();
                var t16=$('#t16').val();
                var nama_dokter=$('#dpjp').val();
                var cara_pulang=$('#cara_pulang').val(); //2pulang 6Ke Rumah Sakit Lain 7MENINGGAL


                /*API CARA pulang
                //Cara pulang didefinisikan sebagai berikut:
                1 = Atas persetujuan dokter 2 = Dirujuk 3 = Atas permintaan sendiri 4 = Meninggal 5 = Lain-lain
                  */
                var cpulang=1;
                if(cara_pulang==2){
                  cpulang=1;
                }else if(cara_pulang==6){
                  cpulang=2;
                }else if(cara_pulang==7){
                  cpulang=4;
                }

                 

                  /*UPDATE KUNJUNGAN*/
                   $.ajax({
                      type : "POST",
                      url  : url+"layanan_rm/update_diagnosa_ranap",
                      dataType : "JSON",
                      data : {id:idpoli, icd_s:icd, procedure:procedure},
                      success: function(data){
                          
                          
                          console.log(data);
                      }
                  });


                    
                   // UPDATE CLAIM
                                $.ajax({
                                  type : "POST",
                                  url  : url+"api_eklaim/updateklaim",
                                  dataType : "JSON",
                                  data : {nosep:nosep, nokartu:nokartu, tmasuk:tmasuk, tpulang:tpulang, 
                                            jenis_rawat:jenis_rawat,
                                           diagnosa:diagnosa,
                                            
                                           proc:proc,
                                           t1:t1,
                                           t2:t2,
                                           t3:t3,
                                           t4:t4,
                                           t5:t5,
                                           t6:t6,
                                           t7:t7,
                                           t8:t8,
                                           t9:t9,
                                           t10:t10,
                                           t11:t11,
                                           t12:t12,
                                           t13:t13,
                                           t14:t14,
                                           t15:t15,
                                           t16:t16,
                                           cara_pulang:cpulang,
                                          nama_dokter:nama_dokter},
                                  success: function(data){
                                      
                                      console.log(data);
                                      if(data.metadata.code==200){
                                      
                                        alert('EKLAIM BERHASIL DIUPDATE');

                                      }else{
                                         alert(data.metadata.message);
                                      }
                                  }
                              });
                  console.log(procedure);    
                  console.log(icd);    
                  
                  console.log(idpoli);    

            });


  });
